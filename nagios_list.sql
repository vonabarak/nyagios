SET NAMES utf8;
INSERT INTO nyagios_nagios (id, zabbix, login, password, fqdn, enabled, description, ctime, mtime, error) VALUES
(1,  0, NULL, NULL, 'nagios2.agava.net',1,'Виртуальный UNIX-хостинг','2014-07-10 15:59:40','2014-11-06 13:55:05',NULL),
(2,  0, NULL, NULL, 'nagios4.agava.net',0,'iFolder, Rotamax, Freehosting','2014-07-10 15:59:47','2014-11-06 13:55:15',NULL),
(3,  0, NULL, NULL, 'nagios5.agava.net',1,'Windows-хостинг и VPS','2014-07-10 15:59:52','2014-11-06 13:55:26',NULL),
(4,  0, NULL, NULL, 'nagios6.agava.net',1,'Linux VPS','2014-07-10 15:59:58','2014-11-06 13:55:37',NULL),
(5,  0, NULL, NULL, 'nagios7.agava.net',1,'Инфраструктурные сервера','2014-07-10 16:00:03','2014-11-06 13:55:51',NULL),
(6,  0, NULL, NULL, 'nagios8.agava.net',0,'filin','2014-07-10 16:00:06','2014-11-06 13:56:04',NULL),
(7,  0, NULL, NULL, 'nagios-mne.agava.net',1,'Хостинг и VPS mne.ru','2014-07-10 16:00:16','2014-11-06 13:56:08',NULL),
(8,  0, NULL, NULL, 'infmon.agava.net',1,'filin','2014-07-10 16:00:32','2014-11-06 13:56:12',NULL),
(9,  0, NULL, NULL, 'nagios.col.agava.net',1,'Dedication и Colocation','2014-07-10 16:00:38','2014-11-06 13:56:32',NULL),
(10, 0, NULL, NULL, 'nagios.domain',1,'Сервера из внутренней сети','2014-07-10 16:00:50','2014-11-06 13:56:38',NULL),
(11, 0, NULL, NULL, 'nagios.cz.domain',1,'Сервера из внутренней сети в Чехии','2014-07-10 16:00:53','2014-11-06 13:56:45',NULL),
(12, 0, NULL, NULL, 'nagios2.renter.ru',1,'Сервера проекта Renter.ru','2014-07-10 16:00:53','2014-11-06 13:56:48',NULL),
(13, 0, NULL, NULL, 'nagios5win.agava.net',1,'Новый мониторинг Win-хостинга и VPS','2014-07-10 16:00:53','2014-11-06 13:56:48',NULL),
(14, 1, 'demo', 'simple-pa$$', 'flows2.agava.net',1,'Zabbix','2014-07-10 16:00:53','2014-11-06 13:56:48',NULL)
ON DUPLICATE KEY UPDATE id=VALUES(id), ctime=NOW(), description=VALUES(description);
