$(function(){
  $(".tablesorter").tablesorter();
 $("#incidents").tablesorter({
        sortList: [[5,0]]
    });
  $("#stat").tablesorter({
        sortList: [[1,1]]
    });
});

$('#delete-incident-button').on('click', function(e) {
    e.preventDefault();
    var id = $(this).data('id');
    $('#delete-incident-modal').data('id', id).modal('show');
    var confirmBtn = document.getElementById('confirm-delete-incident');
    confirmBtn.setAttribute('href', '/rm?id='+id);
});