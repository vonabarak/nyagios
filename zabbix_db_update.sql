
set names utf8;
alter table nyagios_nagios add zabbix boolean default 0;
alter table nyagios_nagios add login varchar(64);
alter table nyagios_nagios add password varchar(64);
insert into nyagios_nagios (fqdn, zabbix, login, password) values
('flows2.agava.net', 1, 'user', 'password');
