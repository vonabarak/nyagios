# encoding: utf-8
from django import template
from django.core.urlresolvers import reverse
import re
import nyagios.russian_numerical
from datetime import datetime
from django.utils.timezone import get_current_timezone
from django.contrib.auth.models import User, Group
from django.utils.datastructures import SortedDict
register = template.Library()


# Возвращает 'active', если текущий URL соответствует заданному в переменной
#
# Пример: {% url 'incidents' as incidents %}
#         {% active request incidents  %} -> 'active'
#
@register.simple_tag
def active(request, pattern):
    if re.search(pattern, request.path):
        return 'active'
    return ''


# Возвращает разницу между двумя временами в минутах.
# Если не указать второй таймстамп, будет считаться разница с текущим временем.
# По дефолту возвращается строка,
# Чтобы получить целочисленное значение, нужно указать True последним аргументом.
#
# Пример: {% timedelta i.mtime False True %} -> 12
#
@register.simple_tag
def timedelta(old, new, integer=False):
    if not new:
        new = get_current_timezone().localize(datetime.now())

    delta = new - old
    delta_min = delta.days * 24 * 60 + delta.seconds / 60
    if integer:
        return int(delta_min)
    else:
        return nyagios.russian_numerical.declension(
            format(delta_min),
            'минута'
        )


# Склоняет переданное слово для соответствия числу
# Слова должны быть зашиты в модуле nyagios.russian_numerical
#
# Пример: {% declen i.hosts 'хост' %} -> '5 хостов'
#
@register.simple_tag
def declen(number, name):
    return nyagios.russian_numerical.declension(number, name.encode('utf-8'))


# Улучшенная версия naturaltime
# Приставку "назад/через" можно отключить, передав аргументом False
#
# Пример: {{n.mtime|time_ago:False}} -> '96 минут'
#
@register.filter
def time_ago(time, ago=True):
    return nyagios.russian_numerical.time_ago(time, ago)


# Применяет к строке произвольную замену по регулярному выражению
#
# Пример: {{ 'ok, this is silly' | replace:'/is/was/i' }} -> 'ok, thwas was silly'
#
@register.filter
def replace(string, args):
    search = args.split(args[0])[1]
    replace = args.split(args[0])[2]

    return re.sub(search, replace, string)


# Возвращает первый параметр в рабоее время и второй - в нерабочее
#
# Пример: {% if_worktime 'работаем', 'отдыхаем' %}
#
@register.simple_tag
def if_worktime(true=True, false=False):
    date = get_current_timezone().localize(datetime.now())

    if 10 <= date.hour < 19:
        return true
    else:
        return false


# Возвращает True, если юзер является членом группы
#
# Пример: {% if request.user|has_group:"admin" %}Будь мужиком, блеать!{% endif %}
#
@register.filter
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return True if group in user.groups.all() else False


@register.filter
def link_highlight(text):
    matches = re.findall(r'(\s|^)(http(s?)://\S+)(\s|$)', text)
    if matches:
        for m in set([m[1] for m in matches]):
            text = text.replace(m, u'<a href="{0}" target="_blank">{0}</a>'.format(m))
    return text


class SetVarNode(template.Node):

    def __init__(self, var_name, var_value):
        self.var_name = var_name
        self.var_value = var_value

    def render(self, context):
        try:
            value = template.Variable(self.var_value).resolve(context)
        except template.VariableDoesNotExist:
            value = ""
        context[self.var_name] = value
        return u""


@register.tag
def setvar(parser, token):
    """
        {% set <var_name>  = <var_value> %}
    """
    parts = token.split_contents()
    if len(parts) < 4:
        raise template.TemplateSyntaxError("'set' tag must be of the form:  {% set <var_name>  = <var_value> %}")
    return SetVarNode(parts[1], parts[3])


# Возвращает число, если оно не равно нулю
#
# Пример:  {% positive incidents.count  %} -> ''
#
@register.simple_tag
def positive(number):
    if number>0:
        return number
    return ''

@register.simple_tag
def notify_url(data, key, subkey, admin, interval):
    number = len(data[key][subkey])
    if number==0:
        return ''
    return "<a href='?admin={1}&date_from={2[date_from]}&time_from={2[time_from]}&date_to={2[date_to]}&time_to={2[time_to]}&key={3}&subkey={4}'>{0}</a>".format(number, admin, interval, key, subkey)


# Сортирует словарь по значению
@register.filter(name='rsort')
def listreversesort(value):
    if isinstance(value, dict):
        new_dict = SortedDict()
        key_list = sorted(value.items(), key=lambda x: x[1], reverse=True)
        for key, val in key_list:
            new_dict[key] = val
        return new_dict.items()
    elif isinstance(value, list):
        return sorted(value, reverse=True)
    else:
        return value
    listsort.is_safe = True
