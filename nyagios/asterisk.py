# -*- coding: utf-8 -*-
import requests
# from requests.auth import HTTPBasicAuth
from requests.exceptions import ConnectionError, Timeout, ReadTimeout, ConnectTimeout
import threading
import traceback
import logging
import sys
import urllib2
import random
from django.utils import timezone
from datetime import timedelta
from nyagios.schedule import get_duties
from nyagios.models import AsteriskNotification, NotifyLog, Incident, Comment
from nyagios.settings import ASTERISK_URL, DONT_REALLY_CALL
from time import sleep
from nyagios.regexp_replacer import replace

requests.packages.urllib3.disable_warnings()  # suppress noisy messages
logger = logging.getLogger(__name__)


def _call(d, incidents, test=False):
    incident = list(incidents)[0]
    msg = u'Привет! Это баба из службы мониторинга. [[slnc 300]]'
    msg += u'Новый инцидент в проекте {}. [[slnc 300]]'.format(incident.project)
    if len(incidents) == 1:
        if len(incident.services) > 1:
            service_text = u'{0} и ещё {1} сервисов'.format(
                    replace(incident.services[0].service_description),
                    len(incident.services) - 1)
        elif len(incident.services) == 0:
            service_text = u'без сервисов'
        else:
            service_text = replace(incident.services[0].service_description)
        msg += u'Хост {host}. ' \
               u'Сервис [[input PHONE]]{service}[[input TEXT]]'.format(
                host=replace(list(incidents)[0].hosts[0].host_name),
                service=service_text)
    else:
        msg += u'Хост {host} [[slnc 400]]' \
               u'и ещё [[input PHONE]]{count}[[input TEXT]] проблемы'.format(
                host=replace(list(incidents)[0].hosts[0].host_name),
                count=len(incidents))
    msg += u'[[slnc 400]] ' \
           u'[[slnc 300]] Если не можешь помочь - нажми 0. ' \
           u'[[slnc 300]] забрать инциденты - 1. ' \
           u'[[slnc 300]] не моя проблема - 2. ' \
           u'[[slnc 300]] соединить с саппортом - 3.' \
           u'[[slnc 300]] соединить с площадкой - 4.' \
           u'[[slnc 300]] прослушать ещё раз - звёздочка.'
    logger.debug(msg)
    try:
        url = u'{url}/?number={phone}&id={incidents}&assigned_to={assigned_to}&duty={duty}&message={message}'.format(
            url=ASTERISK_URL,
            phone=urllib2.quote(d[0]),
            incidents=u'&id='.join(map(lambda x: str(x.id), incidents)),
            message=urllib2.quote(msg.encode('utf8')),
            assigned_to=d[1],
            duty=u'1' if d[2] else u'0'
        )
        if test:
            print(url)
            return True
        logger.debug(u'Invoking _call {}'.format(url))
        response = requests.get(url, timeout=300).json()
        if response['Error']:
            error_text = u'Asterisk have returned error: {0}\nfor request: {1}'.format(response, url)
            raise ValueError(error_text)
        if response['Result']['Answered']:
            logger.debug(u'Call to {} success'.format(d[1]))
            return True
        else:
            nl = NotifyLog()
            nl.incident = incident
            nl.assigned_to = d[1]
            nl.author = 'Asterisk'
            nl.was_duty = d[2]
            if response['Result']['Reason'] == 3:
                # Занято или не отвечает
                nl.answer = 'no_answer'
            elif response['Result']['Reason'] == 1:
                # Абонент не абонент
                nl.answer = 'unreachable'
            else:
                # Непонятно. Считаем, что анричабл.
                nl.answer = 'unreachable'
            nl.save()
            logger.debug(u'Call to {} failed'.format(d[1]))
            return False

    except (ValueError, ConnectionError, Timeout, ConnectTimeout, ReadTimeout):
            t, v, trace = sys.exc_info()
            logger.error(u'Cant connect to asterisk:\n type: {0}\nvalue: {1}\n\n{2}'.format(
                t.__name__,
                v,
                '\n'.join(traceback.format_tb(trace))
            ))
            for i in incidents:
                Comment.objects.get_or_create(
                        incident=i,
                        author='Asterisk',
                        system=True,
                        text=u'Cant connect to asterisk:\n type: {0}\nvalue: {1}'.format(t.__name__, v)
                )
            return False


def _dont_call(d, incidents):
    logger.warning(u'Dont calling anyone as DONT_REALY_CALL setting was set')
    logger.debug(u'duties: {0}\nincidents: {1}'.format(d, incidents))
    # return random exit status
    r = bool(random.randint(0, 1))
    logger.debug(u'Returning {0}'.format(r))
    globals()['call_count'] = dict()
    try:
        if globals()['call_count'][d] < 4:
            globals()['call_count'][d] += 1
            return False
        else:
            return True
    except KeyError:
        globals()['call_count'][d] = 1

if DONT_REALLY_CALL:
    _call = _dont_call


class RPCThread(threading.Thread):
    def __init__(self, i, n):
        self.incidents = i
        threading.Thread.__init__(self, name=n)

    def assigned(self):
        # Проверяет, назначен ли инцидент и удаляет из списка уже назначенные инциденты.
        # Возвращает True, если инцидентов в списке не осталось.
        for i in list(self.incidents):
            # Перечитаем инцидент из базы (на случай, если кто-то забрал его из веб-морды)
            i = Incident.objects.get(id=i.id)
            # Проверяем, назначен ли инцидент
            if i.assigned_to:
                logger.debug(
                        u'Removing incident {0} from call list as it is already assigned.'.format(i.id))
                self.incidents.remove(i)
            # Проверяем, есть ли в инциденте проблемные сервисы
            if reduce(lambda x, y: x and y, [s.service.is_ok() for s in i.statuslogs.all()]):
                logger.debug(
                        u'Removing incident {0} from call list as it have no problems.'.format(i.id))
                self.incidents.remove(i)
        if not self.incidents:
            return True
        return False

    def lock(self):
        # Создаёт в базе флаг, означающий, что по этим инцидентам уже звоним в другом треде.
        for i in list(self.incidents):
            i = Incident.objects.get(id=i.id)
            an = AsteriskNotification.objects.get_or_create(incident=i)[0]
            if an.lock and an.mtime >= timezone.now() - timedelta(minutes=10):
                # По этому инциденту уже звоним. Удаляем его из списка.
                self.incidents.remove(i)
            elif an.lock and an.mtime < timezone.now() - timedelta(minutes=10):
                # Флаг просрочен. Либо кто-то любит потрепаться, либо от астериска не пришёл ответ
                # Добавляем коммент к инциденту, обновляем флаг и звоним снова.
                Comment(
                    incident=i,
                    author='Asterisk',
                    system=True,
                    text=u'Timeout to receive a call status have been exceeded. Call time: {0}'.format(an.mtime)
                ).save()
                an.save()
            else:
                # По этому инциденту не звоним. Значит, сейчас будем. Устанавливаем флаг.
                an.lock = True
                an.save()
        if not self.incidents:
            # Инцидентов в списке не осталось. Видимо, уже звоним по всем.
            return True
        return False

    def unlock(self):
        for i in self.incidents:
            an = AsteriskNotification.objects.get_or_create(incident=i)[0]
            an.lock = False
            an.save()

    def not_my_problem(self, name):
        # Админ ранее ответил "не моя проблема"
        for i in self.incidents:
            if NotifyLog.objects.filter(incident=i, assigned_to=name, answer='not_my_problem'):
                logger.debug(u'{0} answered "not_my_problem" for incident {1}.'.format(name, i.id))
                return True
        return False

    def cant_help(self, name):
        # Админ ответил "не могу помочь" менее 30 минут назад
        for i in self.incidents:
            if NotifyLog.objects.filter(
                    incident=i,
                    assigned_to=name,
                    answer='cant_help',
                    ctime__gt=timezone.now() - timedelta(minutes=30)):
                logger.debug(u'{0} answered "cant_help" for incident {1} less than 30 min ago.'.format(name, i.id))
                return True
        return False

    def unreachable(self, name):
        # Админ был недоступен менее 20 минут назад
        for i in self.incidents:
            if NotifyLog.objects.filter(
                    incident=i,
                    assigned_to=name,
                    answer='unreachable',
                    ctime__gt=timezone.now() - timedelta(minutes=20)):
                logger.debug(u'{0} was "unreachable" for incident {1} less than 20 min ago.'.format(name, i.id))
                return True
        return False

    def no_answer(self, name):
        # Админ не ответил на звонок менее 10 минут назад
        for i in self.incidents:
            if NotifyLog.objects.filter(
                    incident=i,
                    assigned_to=name,
                    answer='no_answer',
                    ctime__gt=timezone.now() - timedelta(minutes=10)):
                logger.debug(u'{0} wasnt answered for incident {1} less than 10 min ago.'.format(name, i.id))
                return True
        return False

    def filter_duties(self, duties):
            # Приводим список ответственных к виду ниже и исключаем из списка тех,
            # кто ответил not_my_problem, кроме начальства (priority >=10),
            # тех, кто не смог помочь, тех, кто был недоступен
            # и тех, кто не взял трубку.
            # duties = [('номер_телефона', 'никнейм', флаг_дежурства, приоритет), ...]
            # Дежурные не исключаются из списка, если был недоступен или не брал трубку
            return [
                (d['phone'], d['name'],
                 True if 'duty_priority' in d and
                         d['duty_priority'] is not None and
                         int(d['duty_priority']) > 0 else False,
                 d['priority'])

                for d in duties if

                'phone' in d and                                          #
                'name' in d and                                           #
                'priority' in d and d['phone'] and                        # есть все необходимые поля и
                (                                                         # (
                    not self.cant_help(d['name']) and                     # не отвечал "не могу помочь"
                    (
                        not self.not_my_problem(d['name']) or             # (не отвечал "не моя проблема" или
                        d['priority'] >= 10                               # начальство админов))
                    ) and
                    (
                        (not self.unreachable(d['name']) and              # не был недоступен
                         not self.no_answer(d['name'])) or                # не (не взял трубку))
                        ('duty_priority' in d and
                         d['duty_priority'] is not None and
                         int(d['duty_priority']) > 0)                     # дежурный
                    )
                ) and (d['priority'] < 100)                               # не начальство техов
            ]

    def run(self):

        logger.debug(u'Starting thread {0} to async call'.format(self.name))
        logger.debug(self.incidents)
        schedule_data = get_duties(self.incidents.copy().pop())
        logger.debug(schedule_data)
        if not [d for d in schedule_data['duties'] if 'phone' in d and 'name' in d and d['phone']]:
            logger.debug(u'Thread {0}: No duties for this incident'.format(self.name))
            return
        _break = False
        if self.lock():
            # уже звоним в другом треде или ещё не получили ответ о последнем звонке
            logger.debug(u'Thread {0}: Call to this set of incidents locked'.format(self.name))
            return
        while not _break:
            duties = self.filter_duties(schedule_data['duties'])
            logger.debug(u'Thread {0}: Duties: {1}'.format(self.name, duties))

            if self.assigned():
                logger.debug(u'Thread {0}: All incidents already assigned'.format(self.name))
                return

            try:
                logger.debug(u'Thread {0}: Iteration started'.format(self.name))
                asterisk_notification = AsteriskNotification.objects.get_or_create(
                        incident=self.incidents.copy().pop())[0]
                if not _break and asterisk_notification.lvl < 13:
                    # звоним дежурным
                    logger.debug(u'Thread {0}: Calling dutie.'.format(self.name))
                    # инкрементируем счётчик попыток
                    asterisk_notification.lvl += 1
                    asterisk_notification.save()
                    # Выбираем из списка на обзвон только дежурных и
                    # сортируем, ставя вниз тех, кому уже звонили
                    for d in sorted(
                            [d for d in duties if d[2]],
                            key=lambda x: self.unreachable(x[1]) or self.no_answer(x[1]),
                            reverse=True
                    ):
                        if not _break:
                            if not self.assigned():
                                # инцидент всё ещё не назначен
                                if _call(d, self.incidents):
                                    # прекращаем перебор админов, если дозвонились
                                    _break = True
                                else:
                                    # не дозвонились
                                    if asterisk_notification.lvl == 13:
                                        # третья безуспешная попытка
                                        pass
                                    else:
                                        # первая или вторая попытка
                                        # ждём минуту и пробуем ещё раз
                                        sleep(60)
                            else:
                                # инциденты кто-то забрал
                                _break = True

                # дежурным позвонили по три раза
                # переводим счётчик на недежурных
                elif asterisk_notification.lvl == 13:
                    asterisk_notification.lvl = 20

                elif not _break and 20 <= asterisk_notification.lvl < 22:
                    # Звоним другим в группе
                    logger.debug(u'Thread {0}: Calling others in group'.format(self.name))
                    if not [d for d in duties if d[3] < 10 and not d[2]]:
                        # не осталось админов - не дежурных и не начальства
                        # инкрементируем счётчик и прерывам перебор до следующего кронтаска
                        logger.debug(u'Thread {0}: All are unreachable/cant_help/whatever. '
                                     u'Sleep 2 min and try one more time.'.format(self.name))
                        asterisk_notification.lvl += 1
                        asterisk_notification.save()
                        _break = True
                    else:
                        # список обзвона не пуст
                        for d in duties:
                            # перебираем админов
                            if not _break and d[3] < 10 and not d[2]:
                                # не начальство и не дежурный
                                if not self.assigned():
                                    # инциденты не назначены. Звоним.
                                    if _call(d, self.incidents):
                                        # дозвонились
                                        _break = True
                                    else:
                                        # не дозвонились. Продолжаем перебор
                                        pass
                                else:
                                    # инциденты кто-то забрал
                                    _break = True

                # дважды добились ситуации, когда из не-начальства звонить некому.
                # переводим стрелку счётчика на начальство
                elif asterisk_notification.lvl == 22:
                    self.unlock()
                    asterisk_notification.lvl = 30

                # Звоним начальству
                elif not _break and asterisk_notification.lvl >= 30:
                    logger.debug(u'Thread {0}: Calling other not in group'.format(self.name))
                    for d in duties:
                        # перебираем
                        if not _break and d[3] >= 10:
                            # приоритет > 10  - начальство
                            if not self.assigned():
                                # не назначен. Звоним
                                if _call(d, self.incidents):
                                    # дозвонились
                                    _break = True
                                else:
                                    # не очень дозвонились
                                    pass
                            else:
                                # инциденты кто-то забрал
                                _break = True
                    # похоже, обзвонили абсолютно всех
                    # сбрасываем счётчик и заново начинаем с дежурных
                    asterisk_notification.lvl = 10
                    asterisk_notification.save()
            except BaseException as exc:
                logger.error(u'Thread {0}: Exception occured while trying to call: {1}'.format(self.name, exc))
            finally:
                asterisk_notification.save()
                # Не снимаем флаг обзвона. Будет снят асинхронным вызовом api_notifylog
                # или по таймауту при одном из следующих обзвонов.
                # unlock()
        logger.debug(u'Thread {0} finished'.format(self.name))


class Asterisk:
    def __init__(self):
        self.th = {}

    def call(self, incidents, thread_name):
        self.th[thread_name] = RPCThread(incidents, thread_name)
        self.th[thread_name].start()

asterisk = Asterisk()
