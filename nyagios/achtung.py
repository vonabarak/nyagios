# -*- coding: utf-8 -*-

import logging
from django.utils.timezone import now
from nyagios.models import Incident, Host, Service, Nagios, StatusLog

logger = logging.getLogger(__name__)


class AchtungFSM:
    def __init__(self):
        self.incident_count = 15
        self.incident = None
        # almost for tests, nagios should exists in production
        self.nagios, _ = Nagios.objects.get_or_create(
            fqdn='nagios.domain'
        )
        self.host, _ = Host.objects.get_or_create(
            nagios=self.nagios,
            host_name='ACHTUNG',
            hostgroup='generic-new'
        )
        self.service, _ = Service.objects.get_or_create(
            host=self.host,
            service_description='ACHTUNG',
            notifications_enabled=1,
            notification_period='24x7',
        )

    def _achtung_activated(self):
        # print('ACHTUNG!!')
        logger.warning(u'Achtung mode activated. No messages.')
        return self

    def _achtung_deactivated(self):
        # print('ACHTUNG is over (-: Sun is shining!')
        logger.warning(u'Achtung mode deactivated. Messaging restored.')
        return self

    def set_achtung(self):
        # print('SET ACHTUNG')
        if not self.get_achtung():
            self.service.current_state = self.service.CRITICAL
            self.service.last_hard_state = self.service.CRITICAL
            self.service.last_check = now()
            self.service.plugin_output = u'ACHTUNG'
            self.service.save()
            self.incident = Incident.objects.create(hidden=True)
            StatusLog.objects.create(
                host=self.host,
                service=self.service,
                incident=self.incident,
                text=u'[action=2] OK (ok_notify) -> CRITICAL (not_ok_notify): ACHTUNG!!!'
            )
            self._achtung_activated()
            # call method that can be overrided
            if hasattr(self, 'on_achtung') and callable(self.on_achtung):
                self.on_achtung()
            return True
        return False

    def unset_achtung(self):
        # print('UNSET ACHTUNG')
        if self.get_achtung():
            reason = u'Sun is shining. Weather is sweet (-:'
            StatusLog.objects.create(
                host=self.host,
                service=self.service,
                incident=self.incident,
                text=u'[action=1] CRITICAL (not_ok_notify) -> OK (ok_notify): ' + reason
            )
            self.service.plugin_output = reason
            self.service.current_state = self.service.OK
            self.service.last_hard_state = self.service.OK
            self.service.last_check = now()
            self.service.save()
            self.incident.etime = now()
            self.incident.save()
            self.incident = None
            self._achtung_deactivated()
            if hasattr(self, 'on_achtung_over') and callable(self.on_achtung_over):
                self.on_achtung_over()
            return True
        return False

    def get_achtung(self):
        # print('GET ACHTUNG')
        try:
            self.incident = Incident.objects.get(statuslogs__host=self.host, etime=None)
            self.incident.services = [self.service]
            self.service.save()  # Чтобы сервис не был удалён обновляем его atime
            return True
        except Incident.DoesNotExist:
            return False
        except Incident.MultipleObjectsReturned:
            self.incident = Incident.objects.filter(statuslogs__host=self.host, etime=None).last()
            self.incident.services = [self.service]
            return True

    def check_achtung(self):
        inc_count = len(Incident.objects.filter(etime=None, assigned_to=None, hidden=False, unimportant=False))
        if inc_count >= self.incident_count:
            self.set_achtung()
        elif inc_count < self.incident_count:
            self.unset_achtung()
        return self.get_achtung()
