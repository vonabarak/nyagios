# -*- coding: utf-8 -*-
import requests
from requests.auth import HTTPBasicAuth
from requests.exceptions import ConnectionError, Timeout, ReadTimeout, ConnectTimeout
import threading
import json
import sys
import traceback
import logging
from django.db.models import Q
from nyagios.settings import TELEGRAM_MESSENGER_URL, TELEGRAM_AUTH_DATA, TELEGRAM_DEBUG
from nyagios.russian_numerical import time_ago
from nyagios.models import TelegramNotification
from nyagios.schedule import get_duties
from nyagios.achtung import AchtungFSM
import nyagios.incident


requests.packages.urllib3.disable_warnings()  # suppress noisy messages
logger = logging.getLogger(__name__)
# logger = logging.getLogger('')

catchall_chat = 'CatchAll'


def u(s):
    if type(s) != unicode:
        try:
            return s.decode('utf-8')
        except UnicodeDecodeError:
            t, v, trace = sys.exc_info()
            logger.error(u'Unicode decode error:\n type: {0}\nvalue: {1}\n\n{2}'.format(
                t.__name__,
                v,
                '\n'.join(traceback.format_tb(trace))
            ))
            return u'<Some text missing. See logs for unicode errors.>'
    return s


def get_hostgroup_duties(hostgroup_name, priority=5):
    url = 'https://schedule.domain/api/duty?group={}'.format(hostgroup_name)
    response = requests.get(url, verify=False)
    response.encoding = 'utf-8'
    try:
        return [d['name'] for d in response.json()[u'duties'] if int(d['priority']) < priority]
    except KeyError:
        return list()
    except ConnectionError:
        return list()


class Telegram(object):

    def __init__(self):
        self.url = TELEGRAM_MESSENGER_URL
        self.headers = {'content-type': 'application/json'}
        self.auth = HTTPBasicAuth(*TELEGRAM_AUTH_DATA)
        self.catchall = catchall_chat
        self.achtung = AchtungFSM()
        self.achtung.on_achtung = self.on_achtung
        self.achtung.on_achtung_over = self.on_achtung_over

    def _call_method(self, method, **kwargs):
        """Call remote method via json-rpc"""

        payload = {
            'method': method,
            'params': kwargs,
            'jsonrpc': '2.0',
            'id': 0
        }

        class RPCThread(threading.Thread):
            def __init__(self, pl, conn_data):
                self.payload = pl
                self.url = conn_data['url']
                self.headers = conn_data['headers']
                self.auth = conn_data['auth']
                threading.Thread.__init__(self)

            def run(self):
                logger.debug(u'Calling RPC method {method} with args {kwargs}'.format(
                    method=method,
                    kwargs=kwargs
                ))

                try:
                    response = requests.post(
                        self.url,
                        data=json.dumps(self.payload),
                        headers=self.headers,
                        auth=self.auth,
                        timeout=180,
                    ).json()
                    if 'error' in response.keys():
                        logger.warning(json.dumps(response['error']))
                        return None
                    return response['result']
                except (ValueError, ConnectionError, Timeout, ConnectTimeout, ReadTimeout):
                    t, v, trace = sys.exc_info()
                    logger.error(u'Cant send a telegram notification:\n type: {0}\nvalue: {1}\n\n{2}'.format(
                        t.__name__,
                        v,
                        '\n'.join(traceback.format_tb(trace))
                    ))
        RPCThread(payload, {'url': self.url, 'headers': self.headers, 'auth': self.auth}).start()

    def on_achtung(self):
        logger.info(u'Executing on_achtung function.')
        incidents = nyagios.incident.incidents_with_probleminfo(
            'id', Q(etime=None, assigned_to=None, hidden=False, unimportant=False))
        hosts = u', '.join([u', '.join([h.host_name for h in i.hosts]) for i in incidents])
        services = u', '.join(list(reduce(
            lambda x, y: x | y, [set([s.service_description for s in i.services]) for i in incidents])))
        self._call_method('chat_message',
                          to=self.catchall,
                          text=u'ACHTUNG!!! Шеф, всё пропало! {0} инцидентов. '
                               u'хосты: {1} сервисы: {2}'.format(len(incidents), hosts, services))

    def on_achtung_over(self):
        logger.info(u'Executing on_achtung_over function.')
        self._call_method('chat_message', to=self.catchall, text=u'Всё хорошо. (ну или почти всё)')

    def user_message(self, user, text):
        t = u(text)
        if self.achtung.check_achtung():
            logger.debug(u' user_message wasnt sent due to achtung mode'.format(user, t))
            return
        logger.debug(u'user_message {0} {1}'.format(user, t))
        if TELEGRAM_DEBUG:
            return self._call_method('user_message', to='vonabarak', text=t)
        else:
            return self._call_method('user_message', to=user, text=t)

    def chat_message(self, chat, text):
        t = u(text)
        if self.achtung.check_achtung():
            logger.debug(u'chat_message wasnt sent due to achtung mode'.format(chat, t))
            return
        logger.debug(u'chat_message {0} {1}'.format(chat, t))
        if TELEGRAM_DEBUG:
            return self._call_method('user_message', to='vonabarak', text=t)
        else:
            return self._call_method('chat_message', to=chat, text=t)

    def send_notification(self, incident, text, service=None):
        """Send message about incident to corresponding chat or user
        depending on incident's current escalation level"""
        telegram_notification = TelegramNotification.objects.get_or_create(incident=incident)[0]
        logger.info(u'Sending message about incident #i{0} to escalation level {1}'.format(
            incident.id, telegram_notification.lvl)
        )
        try:
            msg = u'#i{id} {text}'.format(
                id=incident.id,
                text=u(text)
            )
            if incident:
                if service:
                    schedule_data = get_duties(service)
                else:
                    schedule_data = get_duties(incident)
                if 'project' not in schedule_data:
                    # Если для инцидента нет хостгруппы, то сообщение должно попасть в CatchAll чатик
                    schedule_data['project'] = catchall_chat
                if telegram_notification.lvl == 0:
                    # Уведомление дежурных
                    duties = [
                        d['name'] for d in schedule_data['duties']
                        if 'duty_priority' in d and
                        d['duty_priority'] is not None and
                        int(d['duty_priority'])
                        ]
                    if duties:
                        for d in duties:
                            self.user_message(d, msg)
                    else:
                        # Дежурных по хостгруппе не нашлось
                        self.chat_message(schedule_data['project'], msg)
                elif telegram_notification.lvl == 1:
                    # Уведомление в чатик проекта
                    self.chat_message(schedule_data['project'], msg)
                elif telegram_notification.lvl == 2:
                    # Уведомление в общий чатик
                    self.chat_message(catchall_chat, msg)
                else:
                    # такого не должно случиться.
                    logger.info(
                        u'Incident #i{0} have wrong notification lvl. Do not notify anyone.'.format(incident.id)
                    )
        except BaseException as e:
            logger.error(
                u'An exception occurred during sending notification for incident #{0}: {1}'.format(incident.id, e)
            )

    def first_notification(self, incident, service):
        logger.debug(u'#i{0} first notification'.format(incident.id))
        telegram_notification = TelegramNotification.objects.get_or_create(incident=incident)[0]
        telegram_notification.lvl = 0
        telegram_notification.save()
        msg = u'Новый инцидент с хостом {host}. ' \
            u'Сервис {service} сменил статус на {new}: {output}.'.format(
                host=u(service.host.host_name),
                service=u(service.service_description),
                new=u(service.last_hard_state),
                output=u(service.plugin_output),
                id=incident.id,
            )
        self.send_notification(incident, msg, service)

    def second_notification(self, incident):
        logger.debug(u'#i{0} second notification'.format(incident.id))
        if incident.unimportant:
            logger.info(u'Incident #i{0} is unimportant. Do not notify anyone.'.format(incident.id))
        elif incident.hidden:
            logger.info(u'Incident #i{0} is hidden. Do not notify anyone.'.format(incident.id))
        else:
            service = incident.statuslogs.all()[0].service
            telegram_notification = TelegramNotification.objects.get_or_create(incident=incident)[0]
            telegram_notification.lvl = 1
            telegram_notification.save()
            msg = u'Назначьте ответственного по инциденту c хостом {host}.\n' \
                u'(Ответственный не назначен {time})\n' \
                u'Сервис {service} {output}'.format(
                    host=u(service.host.host_name),
                    service=u(service.service_description),
                    new=u(service.last_hard_state),
                    output=u(service.plugin_output),
                    id=incident.id,
                    time=u(time_ago(incident.ctime, False))
                )
            self.send_notification(incident, msg)

    def third_notification(self, incident):
        logger.debug(u'#i{0} third notification'.format(incident.id))
        if incident.unimportant:
            logger.info(u'Incident #i{0} is unimportant. Do not notify anyone.'.format(incident.id))
        elif incident.hidden:
            logger.info(u'Incident #i{0} is hidden. Do not notify anyone.'.format(incident.id))
        else:
            service = incident.statuslogs.all()[0].service
            telegram_notification = TelegramNotification.objects.get_or_create(incident=incident)[0]
            telegram_notification.lvl = 2
            telegram_notification.save()
            msg = u'Назначьте ответственного по инциденту c хостом {host}.\n' \
                u'(Ответственный не назначен {time})\n' \
                u'Сервис {service} {output}'.format(
                    host=u(service.host.host_name),
                    service=u(service.service_description),
                    new=u(service.last_hard_state),
                    output=u(service.plugin_output),
                    id=incident.id,
                    time=u(time_ago(incident.ctime, False))
                )
            self.send_notification(incident, msg)

    def closing_notification(self, incident):
        logger.debug(u'#i{0} closing notification'.format(incident.id))
        if incident.assigned_to:
            msg = u'Инцидент закрыт. Спасибо {assigned_to}.'.format(
                id=incident.id,
                assigned_to=u(incident.assigned_to)
            )
        else:
            msg = u'Инцидент закрыт (само починилось)'.format(id=incident.id)
        self.send_notification(incident, msg)
        # telegram_notification = TelegramNotification.objects.get_or_create(incident=incident)[0]
        # telegram_notification.lvl = 3
        # telegram_notification.save()

    def reopen_notification(self, incident, author):
        logger.debug(u'#i{0} reopen notification'.format(incident.id))
        telegram_notification = TelegramNotification.objects.get_or_create(incident=incident)[0]
        telegram_notification.lvl = 0
        telegram_notification.save()
        msg = u'Инцидент переоткрыт {author}.'.format(
            id=incident.id,
            author=author
        )
        self.send_notification(incident, msg)

    def unimportant_notification(self, incident, author):
        msg = u'Инцидент отложен {author}.'.format(
            id=incident.id,
            author=author
        )
        self.send_notification(incident, msg)

    def important_notification(self, incident, author):
        msg = u'{author} снял флаг "отложен".'.format(
            id=incident.id,
            author=author
        )
        self.send_notification(incident, msg)

    def comment_notification(self, comment):
        logger.debug(u'#i{0} comment notification'.format(comment.incident.id))
        incident = comment.incident
        msg = u'{user} оставил комментарий к инциденту: {text}'.format(
            id=incident.id,
            user=u(comment.author),
            text=u(comment.text)
        )
        self.send_notification(incident, msg)

    def assign_notification(self, incident, author, answer_text=u''):
        logger.debug(u'#i{0} assign notification'.format(incident.id))
        if not incident.assigned_to:
            msg = u'{author} отказался от тикета'.format(author=u(author))
        elif incident.assigned_to == author:
            msg = u'{author} забрал инцидент'.format(author=u(author))
        else:
            msg = u'{author} назначил ответственным {assigned_to}'.format(
                author=u(author),
                assigned_to=u(incident.assigned_to)
            )
        if answer_text:
            msg += u' ({0})'.format(u(answer_text))
        self.send_notification(incident, msg)

telegram = Telegram()
