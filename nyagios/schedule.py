#!/usr/bin/env python
# encoding: utf-8
import json
import urllib2
import urllib
import logging
import sys
import traceback
from datetime import datetime
from django.utils.timezone import get_current_timezone
import MySQLdb
import SOAPpy
import re
from django.core.cache import cache, get_cache
from hashlib import md5
from xml.sax import SAXParseException
import nyagios.models

# Модуль для работы с сервисом расписаний


def get_duties(incorserv, use_cache=True):
    logger = logging.getLogger(__name__)
    proj_cache = get_cache('projects')
    if isinstance(incorserv, nyagios.models.Incident):
        hosts = set()
        hostgroups = set()
        services = set()
        for log in incorserv.statuslogs.all():
            hosts.add(log.host)
            hostgroups.add(log.host.hostgroup)
            services.add(log.service)
        if len(services) == 1 and len(hosts) == 1:
            hostname = urllib.quote_plus(hosts.copy().pop().host_name)
            servicename = urllib.quote_plus(services.copy().pop().service_description)
            hostgroupname = urllib.quote_plus(hostgroups.copy().pop())
            url = 'https://schedule.domain/api/duty?group={0}&host={1}&service={2}'.format(
                    hostgroupname, hostname, servicename)
        elif len(hosts) == 1:
            hostname = urllib.quote_plus(hosts.copy().pop().host_name)
            hostgroupname = urllib.quote_plus(hostgroups.copy().pop())
            url = 'https://schedule.domain/api/duty?group={0}&host={1}'.format(hostgroupname, hostname)
        elif len(services) == 1:
            hostgroupname = urllib.quote_plus(hostgroups.copy().pop())
            servicename = urllib.quote_plus(services.copy().pop().service_description)
            url = 'https://schedule.domain/api/duty?group={0}&service={1}'.format(hostgroupname, servicename)
        else:
            if not hostgroups:
                hostgroupname = 'no_hostgroup'
            else:
                hostgroupname = hostgroups.copy().pop()
            url = 'https://schedule.domain/api/duty?group={0}'.format(hostgroupname)
    elif isinstance(incorserv, nyagios.models.Service):
        hostname = urllib.quote_plus(incorserv.host.host_name)
        servicename = urllib.quote_plus(incorserv.service_description)
        hostgroupname = urllib.quote_plus(incorserv.host.hostgroup)
        url = 'https://schedule.domain/api/duty?group={0}&host={1}&service={2}'.format(
                hostgroupname, hostname, servicename)
    else:
        # В качестве агрумента передан не инцидент и не сервис. Паникуем.
        raise ValueError
    if use_cache:
        result = proj_cache.get(md5(url).hexdigest())
        if result is not None:
            return result
    # дефолтовый пустой список на случай ошибок
    result = {"duties": [{'name': '', 'priority': '1'}], "service": None, "project": "CatchAll"}
    try:
        result = json.load(urllib2.urlopen(url, timeout=120))
        result['duties'].append({'name': ''})  # кастомный ответственный
    except urllib2.URLError:
        # не удалось получить список из шедулера
        # возвращаем пустой результат и не кэшируем его
        t, v, trace = sys.exc_info()
        logger.warn(u'Cant get shedule data:\n type: {0}\nvalue: {1}\n\n{2}'.format(
                t.__name__,
                v,
                '\n'.join(traceback.format_tb(trace))
            ))
        return result
    except ValueError:
        # non-JSON answer
        logger.warn('Non-Json answer from schedule. Using default empty list.')
        return result
    result['request'] = url
    for d in result.get('duties'):
        if d.get('priority'):
            d['priority'] = int(d.get('priority'))
        if d.get('comment'):
            d['phone'] = re.sub(
                '[^0-9\+]',
                '',
                d.get('comment'))  # Преобразуем телефон к числовому виду
    # кэшируем ответ и отдаём его
    proj_cache.set(md5(url).hexdigest(), result, 5*60)
    return result


# Получает список ответственных
def get_duty_list(hostgroups):
    logger = logging.getLogger(__name__)
    if hostgroups:
        hostgroup = list(hostgroups)[0]
    else:
        hostgroup = 'no_hostgroup'
    # TODO: допилить шедулер, чтобы он поддерживал несколько хостгрупп в одном
    # запросе
    url = 'https://schedule.domain/api/duty?group={0}'.format(hostgroup)
    result = json.load(urllib2.urlopen(url, timeout=120))

    logger.info("schedule request: {0}, reply: {1}".format(url, result))
    for d in result.get('duties'):
        d['priority'] = int(d.get('priority'))
        d['phone'] = re.sub(
            '[^0-9\+]',
            '',
            d.get('comment'))  # Преобразуем телефон к числовому виду

    result['duties'].append({'name': ''})  # кастомный ответственный
    return result

# Получает телефон ответственного


def get_phone(user):
    logger = logging.getLogger(__name__)

    url = 'https://schedule.domain/api/contacts?name={0}'.format(user)
    result = json.load(urllib2.urlopen(url, timeout=120))

    logger.info("schedule request: {0}, reply: {1}".format(url, result))

    return result.get('phone')


# Принимает время как строку или объект,
# при вызове без аргументов использует текущее время
def is_workhours(sometime=None):
    if sometime is None:
        time = get_current_timezone().localize(datetime.now())
    elif isinstance(sometime, str):
        time = get_current_timezone().localize(
            datetime.strptime(
                sometime,
                "%Y-%m-%d %H:%M:%S"))
    elif isinstance(sometime, datetime):
        time = sometime.astimezone(get_current_timezone())
    else:
        return "Unexpected type: {0}".format(type(sometime))

    if 10 <= time.hour < 19:
        return is_workday(time)

    return False


# Обращается к сервису "Производственный календарь"
def is_workday(my_datetime):
    logger = logging.getLogger(__name__)
    date = my_datetime.strftime("%Y-%m-%d")
    # Кэширование
    answer = cache.get('is_work_date_' + date)
    if answer is not None:
        return answer

    namespace = 'http://new.webservice.namespace'
    url = 'http://elb.esb.domain/services/prx.utility.work.stub'
    server = SOAPpy.SOAPProxy(url, namespace)
    try:
        res = server.getDayByDate(date)

        logger.info(
            "calendar request: getDayByDate({0}), reply: {1}".format(
                date,
                res))

        if res.type == 'work':
            answer = True
        elif res.type in ('weekend', 'holiday'):
            answer = False
        elif res.type == 'null':
            answer = None
        else:
            return res
    except SAXParseException as err:
        logger.error("Can't get workday info: {0}".format(err))
        return err
    except SOAPpy.Types.faultType as err:
        logger.error(
            "Can't get workday info (SOAPpy.Types.faultType): {0}".format(err))
        return err
    except NameError as err:
        logger.error("Can't get workday info ({0}): {1}".format(res, err))
        return err
    except:
        logger.error("Can't get workday info ({0}): {1}")
        return 'so bad' 

    cache.set('is_work_date_' + date, answer, 24 * 3600)
    return answer


# Получить список дежурных площадеров:
def get_dc_duties():
    res = cache.get('dc_duties')
    if res is not None:
        return res
    try:
        res = {'error': False, 'result': []}
        con = MySQLdb.connect(
            'dc.renter.ru',
            'monitor.domain',
            'SuyvM4K1L785',
            'rota5',
            connect_timeout=3
        )
        cur = con.cursor()
        cur.execute(
            'select user, dc, duty_type, duty_date from rota '
            'where duty_date=DATE(DATE_ADD(NOW(), INTERVAL -10 HOUR)) ORDER BY dc')
        for x in xrange(0, cur.rowcount):
            row = cur.fetchone()
            dc = row[1].replace('A', 'ДЦ-')
            if row[2] == 1:
                duty_type = 'день'
            elif row[2] == 2:
                duty_type = 'ночь'
            else:
                duty_type = row[2]
            date = get_current_timezone().localize(datetime.now())

            if 9 <= date.hour < 21:
                if duty_type == 'ночь':
                    continue
            elif duty_type == 'день':
                continue

            res['result'].append({
                'user': row[0],
                'dc': dc,
                'duty_type': duty_type,
                'duty_date': row[3],
            })
        cache.set('dc_duties', res, 300)
        con.close()
    except MySQLdb.Error as e:
        res['error'] = "%s (error %d)" % (e.args[1], e.args[0])

    return res
