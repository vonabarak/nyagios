# encoding: utf-8
# Модуль для работы с инцидентами
import logging
from django.db.models import Q, Count
from django.utils.timezone import get_current_timezone
from nyagios.models import Host, Service, Incident, StatusLog, Comment
from datetime import datetime, timedelta, time
import nyagios.notifier

# Находит/создает инцидент по хосту или сервису


def find_incident(service, create=False, any_service=False):
    logger = logging.getLogger(__name__)
    time_threshold = get_current_timezone().localize(
        datetime.now()) - timedelta(minutes=30)

    if service.separate_incidents:
        # Если сервис имеет флаг separate_incidents, то создавать новый инцидент
        # даже если существуют недавно созданные инциденты с тем же хостом
        history = StatusLog.objects.filter(
            ~Q(incident=None) &
            Q(incident__etime=None) &
            Q(service=service)
        ).order_by('-ctime').first()
    else:
        host_ids = [h.id for h in Host.objects.filter(host_name=service.host.host_name)]
        if any_service:
            # Подходят любые инциденты с данным хостом
            history = StatusLog.objects.filter(
                ~Q(incident=None) &
                Q(incident__etime=None) &                        # ищем открытые инциденты
                Q(host_id__in=host_ids)                          # по данному хосту
            ).order_by('-ctime').first()
        else:
            # подходят только инциденты с тем же сервисом либо созданные недавно
            # но не с сервисом с флагом отдельных инцидентов
            history = StatusLog.objects.filter(
                ~Q(incident=None) &
                Q(incident__etime=None) &
                Q(host_id__in=host_ids) &
                Q(service__separate_incidents=False) &
                Q(Q(service=service) | Q(incident__ctime__gt=time_threshold))
            ).order_by('-ctime').first()

    # Если получилось, возвращаем найденный инцидент
    if history and (service == history.service or not history.service.separate_incidents):
        incident = history.incident
    elif create:
        incident = Incident()
        incident.save()
        # Инцидент создан. Уведомляем в телеграм.
        nyagios.notifier.telegram.first_notification(incident, service)
    else:
        return None

    if history:
        action = 'update'
    else:
        action = 'create'
    logger.info(u"Incident {id} {action}d: {host}.{service}".format(
        id=incident.id,
        action=action,
        host=service.host.host_name,
        service=service.service_description,
    ))
    return incident


# Закрываем ничейные инциденты, если по ним нет проблем
# И уведомляем о неназначенных
def auto_close_incidents():
    logger = logging.getLogger(__name__)
    incidents = Incident.objects.filter(etime=None)

    # Для каждого инцидента
    for incident in incidents:
        is_ok = True
        logs = StatusLog.objects.filter(incident=incident)
        hostgroups = set()
        for log in logs:
            if log.service:
                # Нас интересуют только сервисы, по которым нужно уведомлять в данный момент
                # UPD: либо уведомлять уже не надо т.к. наступила ночь, но
                # инцидент мы хотим оставить открытым
                if log.service.need_notify() \
                or log.service.notification_period == Service.WORKHOURS \
                or log.service.notification_period == Service.DAYTIME:
                    is_ok &= log.service.is_ok()
                hostgroups.add(log.host.hostgroup)

        # Если у нас каким-то глюком образовался инцидент без привязанных событий
        if logs.count() == 0:
            logger.warning(
                "Found incident {0} without hosts and services. Deleting".format(
                    incident.id))
            incident.delete()
            continue

        if is_ok:
            if incident.autoclose:
                if not incident.assigned_to:
                    text = u'Инцидент закрыт автоматически (само починилось)'
                else:
                    text = u'Инцидент закрыт автоматически ({0} пофиксил?)'.format(
                        incident.assigned_to)

                nyagios.notifier.telegram.closing_notification(incident)
            else:
                continue

            incident.close()

            comment = Comment(
                incident=incident,
                author='system',
                system=True,
                text=text
            )
            comment.save()

            logger.info("Incident {0} autoclose".format(incident.id))
        else:
            # Инцидент не починился
            if not incident.assigned_to:
                # мало того, он даже никому не назначен
                if get_current_timezone().localize(datetime.now()) - incident.ctime > timedelta(minutes=30):
                    # уже более получаса
                    if not hasattr(incident, 'telegram_notification') or incident.telegram_notification.lvl == 0:
                        # второе уведомление
                        nyagios.notifier.telegram.second_notification(incident)
                    elif incident.telegram_notification > 0:
                        # третье уведомление
                        if get_current_timezone().localize(datetime.now()) - incident.telegram_notification.mtime > \
                                timedelta(minutes=60):
                            # Прошло более часа с предыдущего напоминания
                            # Делаем третье (или последующие) уведомление
                            nyagios.notifier.telegram.third_notification(incident)


def incidents_with_probleminfo(order_by, my_Q, state='not_ok'):
    incidents = Incident.objects.prefetch_related(
        'statuslogs__host',
        'statuslogs__service',
        'comments',
        'notifies',
    ).filter(my_Q).annotate(Count('statuslogs')).order_by(order_by)

    escalation_time = get_current_timezone().localize(
        datetime.now()) - timedelta(minutes=120)

    for i in incidents:
        hosts = set()
        hostgroups = set()
        services = set()
        for log in i.statuslogs.all():
            hosts.add(log.host)
            hostgroups.add(log.host.hostgroup)
            if log.service and not log.service.is_ok():
                services.add(log.service)

        if state == 'any':
            for h in hosts:
                for s in h.services.all():
                    if s.last_hard_state != Service.DELETED:
                        services.add(s)

        i.hosts = list(hosts)
        i.hostgroups = list(hostgroups)

        i.services = []
        for s in services:
            if s.service_description == 'ping':
                i.services.insert(0, s)
            else:
                i.services.append(s)

        # Цветовая маркировка срочности инцидента
        if not i.assigned_to and not i.unimportant:
            i.state = Service.CRITICAL
        elif not i.assigned_to and i.unimportant:
            i.state = Service.WARNING
        elif i.unimportant or i.mtime > escalation_time:
            i.state = Service.OK
        else:
            i.state = Service.WARNING

    return incidents
