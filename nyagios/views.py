# encoding: utf-8
# from collections import defaultdict
from datetime import datetime, date, timedelta, time
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q, Count, F
# from django.forms.models import model_to_dict
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404
from django.utils.timezone import get_current_timezone, localtime
from django.utils.translation import ugettext as _
from django.utils.html import escape
from django.core.cache import get_cache
from nyagios.incident import incidents_with_probleminfo, find_incident
from nyagios.models import Nagios, Host, Service, StatusLog, Incident, Comment, NotifyLog, UserProfile, AsteriskNotification, Motd
from nyagios.nagios_handler import NagiosObject
from nyagios.russian_numerical import time_ago
from nyagios.schedule import get_duty_list, get_dc_duties, get_duties
from nyagios.notifier import telegram
from nyagios.two_factor import two_factor_auth
from nyagios.rt import Rt
from nyagios.settings import RT_URL, RT_AUTH_DATA
import json
import re
import requests
from dicttoxml import dicttoxml
from django.utils.datastructures import SortedDict


# Login page
def login_user(request):
    logout(request)
    client_ip = request.META['REMOTE_ADDR']

    two_factor = True
    if client_ip.startswith(settings.PRIVATE_IPS_PREFIX):
        two_factor = False

    error = ''
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        code = request.POST.get('code')

        user = authenticate(username=username, password=password)
        if two_factor:
            two_factor_success, two_factor_reason = two_factor_auth(
                username, code)
        else:
            two_factor_success = True

        if user is not None:
            if user.is_active:
                if two_factor_success:
                    login(request, user)
                    return HttpResponseRedirect(
                        request.REQUEST.get(
                            'next',
                            '/'))
                else:
                    error = two_factor_reason
            else:
                error = 'err_user_disabled'
        else:
            error = 'err_invalid_password'

    return render(request, 'login.html', {
        'next': request.REQUEST.get('next'),
        'error': error,
        'two_factor': two_factor
    })


# Главная страница мониторинга
@login_required(login_url='/login/')
def index(request):
    return HttpResponseRedirect(reverse('incidents'))


@login_required(login_url='/login/')
def current_problems(request):
    debug = ''

    query = Q()
    # Отображаем только выбранные статусы
    states_display = {}
    profile = request.user.profile
    hide_nagios = profile.info.get('hide_nagios', [])

    display_something = False
    for state in Service.STATES:
        states_display[state] = False
        if request.GET.get(state) == 'on':
            display_something = True
            states_display[state] = True
            query.add(Q(current_state=state), Q.OR)

    # Если ничего не выбрано, по умолчанию отображаем статусы с is_ok == False
    if not display_something:
        for state in Service.STATES:
            if not Service.is_ok_ref[state]:
                states_display[state] = True
                query.add(Q(current_state=state), Q.OR)

    query.add(~Q(host__nagios__fqdn__in=hide_nagios), Q.AND)

    # Отображаем только проблемы, для которых включены уведомления
    if request.GET.get('show_all'):
        show_all = True
    else:
        show_all = False
        query.add(Q(notifications_enabled=True), Q.AND)

    services = Service.objects.select_related('host').filter(
        query
    ).order_by('-mtime')

    return render(request, 'current_problems.html', {
        'show_all': show_all,
        'services': services,
        'states_display': states_display,
        'debug': debug
    })

# Открытые инциденты


@login_required(login_url='/login/')
def incidents(request):
    period = int(request.GET.get('period', 0))
    debug = ''

    profile = request.user.profile
    hide_nagios = profile.info.get('hide_nagios', [])
    profile.save()  # Обновляем время последнего посещения
    if request.POST.get('join'):
        # Склеивание инцидентов
        incidents = sorted(request.POST.getlist('join'))
        first_incident = get_object_or_404(Incident, id=incidents.pop(0))
        # Перепривязываем все события к первому инциденту, остальные удаляем
        StatusLog.objects.filter(
            incident__in=incidents).update(
            incident=first_incident)
        Comment.objects.filter(
            incident__in=incidents).update(
            incident=first_incident)
        NotifyLog.objects.filter(
            incident__in=incidents).update(
            incident=first_incident)
        # Ищем инцидент с ответственным
        assigned_incident = Incident.objects.filter(
            Q(pk__in=incidents) &
            ~Q(assigned_to=None)
        ).first()
        if assigned_incident:
            first_incident.assigned_to = assigned_incident.assigned_to
            first_incident.save()
        Incident.objects.filter(pk__in=incidents).delete()
        if len(incidents) > 1:
            notification_text = u'{0} склеил инциденты {1}'.format(
                request.user.username,
                u', '.join([u'#i' + i for i in incidents]),
            )
            comment_text = u'инциденты {0} склеены'.format(u', '.join(incidents))
        elif len(incidents) == 1:
            notification_text = u'{0} склеил инцидент #i{1}'.format(request.user.username, incidents[0])
            comment_text = u'инцидент {0} склеен'.format(incidents[0])
        if incidents:
            Comment.objects.create(
                incident=first_incident,
                author=request.user.username,
                system=True,
                text=comment_text
            )
            telegram.send_notification(first_incident, notification_text)
        return HttpResponseRedirect(reverse('incidents'))

    show_filter = ~Q(hidden=True)
    if request.GET.get('hidden'):
        show_filter = Q()
        count_hidden = 0
    else:
        count_hidden = Incident.objects.filter(Q(etime=None) & ~Q(
            statuslogs__host__nagios__fqdn__in=hide_nagios) & Q(hidden=True)).count()

    if period:
        ctime = get_current_timezone().localize(
            datetime.now()) - timedelta(hours=period)
        incidents = incidents_with_probleminfo(
            '-mtime', Q(ctime__gt=ctime) & ~Q(statuslogs__host__nagios__fqdn__in=hide_nagios))
    else:
        incidents = incidents_with_probleminfo(
            '-mtime', Q(etime=None) & ~Q(statuslogs__host__nagios__fqdn__in=hide_nagios) & show_filter)

#    incidents = incidents.filter(~Q(hidden=True))
    time_threshold = get_current_timezone().localize(
        datetime.now()) - timedelta(minutes=15)

    escalation_time = get_current_timezone().localize(
        datetime.now()) - timedelta(minutes=120)

    # Загружаем список юзеров, недавно заходивших в мониторинг
    online_time = get_current_timezone().localize(
        datetime.now()) - timedelta(minutes=5)

    online = UserProfile.objects.filter(atime__gt=online_time)

    # Делаем список для popup-а по новым ницидентам
    new_problems = []
    if profile.info.get('popup_alert', True):
        new_problems_time = get_current_timezone().localize(
            datetime.now()) - timedelta(seconds=90)
        for i in incidents:
            if i.ctime > new_problems_time:
                new_problems.append(i)

    # Сообщения дня
    motds = Motd.objects.filter(active=True)
    # Ахтунг
    achtung = telegram.achtung
    achtung.check_achtung()

    return render(request, 'incidents.html', {
        'period': period,
        'debug': debug,
        'incidents': incidents,
        'new_problems': new_problems,
        'escalation_time': escalation_time,
        'hide_nagios': hide_nagios,
        'online': online,
        'dc_duties': get_dc_duties(),
        'count_hidden': count_hidden,
        'motds': motds,
        'achtung': achtung.incident
    })

# Подробная информация об инциденте


def incident(request, id):
    return HttpResponseRedirect(reverse('incident_comments', args=(id,)))


@login_required(login_url='/login/')
def hide(request):
    id = request.GET.get('id')
    action = request.GET.get('action')
    incident = Incident.objects.get(id=id)
    author = request.user.username
    if action == 'hide':
        incident.hidden = True
        Comment(incident=incident, author=author, system=True,
                text='Инцидент скрыт!').save()
    else:
        incident.hidden = False
        Comment(incident=incident, author=author, system=True,
                text='Инцидент раскрыт').save()
    incident.save()

    return HttpResponseRedirect(reverse('incident_comments', args=(id,)))


@login_required(login_url='/login/')
def rm(request):
    id = request.GET.get('id')
    incident = get_object_or_404(Incident, id=id)
    # author = request.user.username
    stlg_list = list(incident.statuslogs.all())
    incident.delete()
    for stlg in stlg_list:
        stlg.service.current_state = 'OK'
        stlg.service.last_hard_state = 'OK'
        stlg.service.save()
        stlg.delete()
    return HttpResponseRedirect(reverse('incidents'))


@login_required(login_url='/login/')
def incident_comments(request, id):
    limit = int(request.GET.get('limit', 10))
    with_comments_only = request.GET.get('with_comments_only', False)
    show_unimportant = request.GET.get('show_unimportant', False)
    try:
        incident = incidents_with_probleminfo(
            'id',
            Q(pk=id),
            state=request.GET.get('state'),
        )[0]
    except IndexError:
        return HttpResponseNotFound('<h1>Нет такого инцидента</h1>')

    comments = Comment.objects.filter(incident=id).order_by('ctime')
    author = request.user.username
    text = request.POST.get('text', '')
    action = request.POST.get('action') or request.GET.get('action')

    # Добавляем к комментариям лог обзвона
    log = NotifyLog.objects.filter(incident=id).order_by('ctime')
    comments_and_log = []
    for l in log:
        if l.answer == NotifyLog.ANSWER_OK:
            continue
        l.system = True
        l.text = ''

        if l.author != l.assigned_to:
            l.text += "Уведомление {assigned_to}: ".format(
                author=l.author,
                assigned_to=l.assigned_to.encode('utf-8'),
            )

        if l.answer_text != '':
            l.text += l.answer_text.encode('utf-8') + " "
        l.text += "({0})".format(_(l.answer).encode('utf-8'))
        comments_and_log.append(l)

    for c in comments:
        comments_and_log.append(c)
    comments_and_log = sorted(comments_and_log, key=lambda k: k.ctime)

    # Подгружаем комментарии от предыдущих инцидентов
    prevous_logs = StatusLog.objects.filter(
        host__in=incident.hosts).order_by('-ctime')
    prevous_incidents = []
    for l in prevous_logs:
        have_comments = True
        if with_comments_only and l.incident:
            have_comments = False
            for c in l.incident.comments.all():
                if not c.system:
                    have_comments = True

        if l.incident and l.incident != incident and l.incident not in prevous_incidents and have_comments:
            if len(prevous_incidents) >= limit > 0:
                break
            prevous_incidents.append(l.incident)
            if show_unimportant:
                l.incident.comments_filtered = l.incident.comments.all()
            else:
                l.incident.comments_filtered = l.incident.comments.filter(
                    system=False)
            if l.service.host in incident.hosts:
                l.incident.services = [l.service]
        elif l.incident:
            for i in prevous_incidents:
                if l.incident == i and l.service not in i.services:
                    if l.service.host in incident.hosts:
                        i.services.append(l.service)

    min_text_len = 2

    if action is not None:
        if len(text) >= min_text_len:
            comment = Comment(
                incident=incident,
                author=author,
                text=text
            )
            comment.save()
            incident.save()
            telegram.comment_notification(comment)

        if action == 'unimportant' and not incident.unimportant:
            Comment(incident=incident, author=author, system=True,
                    text=u'Инцидент отложен').save()
            telegram.unimportant_notification(incident, author)
            incident.unimportant = True
            incident.save()

        elif action == 'important' and incident.unimportant:
            Comment(incident=incident, author=author, system=True,
                    text=u'Убран флаг "отложен"').save()
            incident.unimportant = False
            incident.save()
            telegram.important_notification(incident, author)

        elif action == 'close' and not incident.etime:
            can_close = True
            problems = []
            for s in incident.services:
                if not s.is_ok() and s.need_notify():
                    can_close = False
                    problems.append(
                        s.service_description + " " + str(s.fsm_state()))

            if can_close:
                Comment(incident=incident, author=author, system=True,
                        text='[closed]').save()
                telegram.closing_notification(incident)
                incident.close()

            else:
                return HttpResponseRedirect(
                    reverse(
                        'incident_comments',
                        args=(
                            id,
                        )) +
                    '?error=err_cant_close&problems=' + ", ".join(problems)
                )

        elif action == 'reopen' and incident.etime:
            Comment(incident=incident, author=author, system=True,
                    text='[reopened]').save()
            incident.etime = None
            telegram.reopen_notification(incident, author)
            incident.save()
        elif action == 'autoclose':
            incident.autoclose = True
            incident.save()
        elif action == 'noautoclose':
            incident.autoclose = False
            incident.save()
        elif len(text) < min_text_len:
            return HttpResponseRedirect(
                reverse(
                    'incident_comments',
                    args=(
                        id,
                    )) +
                '?error=err_too_short')
        return HttpResponseRedirect(reverse('incident_comments', args=(id,)))

    return render(request, 'incident_info.html', {
        'block': 'comments',
        'incident': incident,
        'comments': comments_and_log,
        'prevous_incidents': prevous_incidents,
        'limit': limit,
        'LIMITS': [5, 10, 100, 1000, 0],
        'show_unimportant': show_unimportant,
        'with_comments_only': with_comments_only,
    })


@login_required(login_url='/login/')
def incident_status(request, id):
    try:
        incident = incidents_with_probleminfo(
            'id',
            Q(pk=id),
            state=request.GET.get('state'),
        )[0]
    except IndexError:
        return HttpResponseNotFound('<h1>Нет такого инцидента</h1>')

    statuslog = StatusLog.objects.filter(incident=id).order_by('-ctime')

    return render(request, 'incident_info.html', {
        'block': 'status',
        'incident': incident,
        'log': statuslog,
    })


@login_required(login_url='/login/')
def incident_dc_ticket(request, id):
    try:
        incident = incidents_with_probleminfo(
            'id',
            Q(pk=id),
            state=request.GET.get('state'),
        )[0]
    except IndexError:
        return HttpResponseNotFound('<h1>Нет такого инцидента</h1>')

    full_path = request.build_absolute_uri(reverse('incident', args=[id]))
    result = None

    # Список хостов в инциденте
    hosts = list()
    map(lambda s: s.host.host_name in hosts or hosts.append(s.host.host_name), incident.statuslogs.all())

    if request.POST:
        post = True
        dc_queue = request.POST.get('dc-queue')
        subj = request.POST.get('subj')
        body = request.POST.get('body')
        rt = Rt(RT_URL+'/REST/1.0/', RT_AUTH_DATA[0], RT_AUTH_DATA[1])
        if rt.login():
            ticket = rt.create_ticket(
                Queue=dc_queue,
                Subject=subj,
                Requestors=request.user.username+'@agava.com',
                Text=body.replace('\n', ' '),  # C многострочным текстом в API всё плохо.
                # Status='Resolved'
            )
            if ticket != -1:
                result = u'<a href="{rt_url}Ticket/Display.html?id={ticket}" target="_blank">Тикет создан</a>'.format(
                    rt_url=RT_URL,
                    ticket=ticket
                )
                comment = Comment(
                    incident=incident,
                    author=request.user.username,
                    text=u'Создан тикет: {rt_url}Ticket/Display.html?id={ticket}'.format(rt_url=RT_URL, ticket=ticket),
                    system=False
                )
                comment.save()
            else:
                result = u'Не удалось создать тикет )-:'
        else:
            result = u'Не удалось залогиниться в {url} пользователем {rtuser}'.format(
                url=RT_URL,
                rtuser=RT_AUTH_DATA[0]
            )
    else:
        post = False

    return render(request, 'incident_info.html', {
        'block': 'dc_ticket',
        'incident': incident,
        'post': post,
        'hosts': hosts,
        'full_path': full_path,
        'result': result,
    })


# Назначение ответственного за инцидент
@login_required(login_url='/login/')
def incident_assign(request, id):
    try:
        incident = incidents_with_probleminfo(
            'id',
            Q(pk=id),
        )[0]
    except IndexError:
        return HttpResponseNotFound('<h1>Нет такого инцидента</h1>')

    author = request.user.username
    assigned_to = request.REQUEST.get('user')

    if assigned_to:
        answer = request.REQUEST.get('answer')
        if answer not in NotifyLog.ANSWER_TYPES:
            raise KeyError(
                'Incorrect answer type: not in {0}'.format(
                    NotifyLog.ANSWER_TYPES))

        if assigned_to == u'nobody':
            assigned_to = u''

        notifylog = NotifyLog(
            incident=incident,
            author=author,
            assigned_to=assigned_to,
            answer=answer,
            answer_text=request.REQUEST.get('answer_text', u''),
            was_duty=request.REQUEST.get('duty', False)
        )
        notifylog.save()

        if answer == NotifyLog.ANSWER_OK or assigned_to == '':
            if incident.assigned_to == assigned_to:
                text = u'Повторное уведомление {0}'.format(
                    incident.assigned_to)
            elif not assigned_to:
                text = u'Ответственный удалён!'
            else:
                text = u'Ответственный изменен на {0}'.format(
                    assigned_to)
            if assigned_to == '':
                incident.assigned_to = None
            else:
                incident.assigned_to = assigned_to

            if notifylog.answer_text:
                text += u': ' + notifylog.answer_text

            comment = Comment(
                incident=incident,
                author=author,
                text=text,
                system=True,
            )
            comment.save()
            incident.save()  # обновляем incident.mtime
            telegram.assign_notification(incident, author, request.REQUEST.get('answer_text', u''))

            return HttpResponseRedirect(reverse(
                'incident_comments',
                args=(id,)
            ))
        else:
            return HttpResponseRedirect(reverse('incident_assign', args=(id,)))

    log = NotifyLog.objects.filter(incident=incident).order_by('ctime')

    schedule_error = None
    schedule = get_duties(incident)
    for u in schedule['duties']:
        u['try_count'] = log.filter(assigned_to=u['name']).count()
        u['try_ok'] = log.filter(
            assigned_to=u['name'],
            answer=NotifyLog.ANSWER_OK).count()
    return render(request, 'incident_info.html', {
        'block': 'assign',
        'incident': incident,
        'schedule': schedule,
        'log': log,
        'schedule_error': schedule_error,
        'answers': NotifyLog.ANSWER_TYPES,
    })


# Последние события в нагиосе
@login_required(login_url='/login/')
def status_log(request):
    # Получаем список записей, делим на страницы
    hide_nagios = request.user.profile.info.get('hide_nagios', [])
    host = request.GET.get('host', '')
    service = request.GET.get('service', '')
    description = request.GET.get('description', '')
    regex = request.GET.get('regex', '')

    if regex:
        regex = 'checked'
        if not host:
            host = '.*'
        if not service:
            service = '.*'
        if not description:
            description = '.*'
        list = StatusLog.objects.filter(
            host__host_name__regex=host,
            service__service_description__regex=service,
            text__regex=description,
        ).order_by('-ctime')
    else:
        list = StatusLog.objects.filter(
            host__host_name__contains=host,
            service__service_description__contains=service,
            text__contains=description,
        ).exclude(host__nagios__fqdn__in=hide_nagios).order_by('-ctime')
    per_page = request.GET.get('per_page', 25)
    paginator = Paginator(list, per_page)
    page = request.GET.get('page')

    # Обеспечиваем передачу параметров фильтрации при листании
    queries_without_page = request.GET.copy()
    if 'page' in queries_without_page:
        del queries_without_page['page']

    try:
        log = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        log = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        log = paginator.page(paginator.num_pages)

    return render(request, 'status_log.html', {
        'log': log,
        'queries': queries_without_page,
        'PER_PAGE_VARS': [10, 25, 100, 500, 1000],
        'per_page': int(per_page),
        'host': host,
        'service': service,
        'description': description,
        'regex': regex,
    })


# Суточный отчёт по событиям

@login_required(login_url='/login/')
def report(request):
    hide_nagios = request.user.profile.info.get('hide_nagios', [])
    min_len = int(request.GET.get('min_len', 15))
    assigned = bool(request.GET.get('assigned', ''))
    comments = bool(request.GET.get('comments', ''))
    format = '%Y-%m-%d %H:%M'
    if request.GET.get('date_from'):
        date_from = datetime.strptime(request.GET['date_from'], format)
    else:
        date_from = datetime.now().replace(
            hour=19,
            minute=0,
            second=0,
            microsecond=0
        ) - timedelta(days=1)

    if request.GET.get('date_to'):
        date_to = datetime.strptime(request.GET['date_to'], format)
    else:
        date_to = datetime.now()

    my_q = Q(id=0)
    if assigned and comments:
        my_q = ~Q(notifies__assigned_to=None) | Q(comments__system=0)
    elif assigned:
        my_q = ~Q(notifies__assigned_to=None)
    elif comments:
        my_q = Q(comments__system=0)

    incidents = incidents_with_probleminfo(
        'ctime',
        (
            #            (
            #                Q(etime__gte=date_from)
            #                & Q(etime__lte=date_to)
            #            ) |
            (
                Q(ctime__gte=date_from)
                & Q(ctime__lte=date_to)
                & (
                    Q(etime__gt=F('ctime') + timedelta(minutes=min_len)) |
                    Q(etime=None) |
                    my_q
                )
            )
        )
        & ~Q(statuslogs__host__nagios__fqdn__in=hide_nagios)
    )

    for i in incidents:
        report = []
        # Состояние самого инцидента
        if i.ctime < get_current_timezone().localize(date_from):
            i.from_past = True
        for l in i.statuslogs.all():
            output = re.sub('^.*\): ', '', l.text)
            m = re.search('-> (.*) \(.*ok.*\): ', l.text)
            if m:
                new_state = m.group(1)
            else:
                new_state = ''
            report.append({
                'ctime': l.ctime,
                'text': u"{state} {host}/{service}: {output}".format(
                    state=new_state,
                    host=l.host.host_name,
                    service=l.service.service_description,
                    output=escape(output)
                )
            })

        if i.etime:
            report.append({
                'ctime': i.etime,
                'text': "ok"
            })
        else:
            report.append({
                'ctime': get_current_timezone().localize(datetime.now()),
                'text': "актуально"
            })

        # Обзвон
        for n in i.notifies.all():
            text = ''
            if n.author != n.assigned_to:
                text += "{author} -> <b>{assigned_to}</b>: ".format(
                    author=n.author,
                    assigned_to=n.assigned_to.encode('utf-8'),
                )
            else:
                text += "<b>{assigned_to}</b>: ".format(
                    assigned_to=n.assigned_to.encode('utf-8'),
                )

            if n.answer_text:
                text += n.answer_text.encode('utf-8') + " "
            text += "({0})".format(_(n.answer).encode('utf-8'))

            report.append({
                'ctime': n.ctime,
                'text': text,
            })

        # Комментарии
        for n in i.comments.all():
            if not n.system:
                report.append({
                    'ctime': n.ctime,
                    'text': "<b>{author}</b>: {text}".format(
                        author=n.author,
                        text=n.text.encode('utf-8'),
                    )
                })

        # Сортируем всю эту солянку по времени
        i.report = sorted(report, key=lambda k: k['ctime'])

    return render(request, 'report.html', {
        'incidents': incidents,
        'date_from': date_from,
        'date_from_str': date_from.strftime(format),
        'date_to_str': date_to.strftime(format),
        'min_len': min_len,
        'assigned': assigned,
        'comments': comments,
    })


# Информация о сервере

@login_required(login_url='/login/')
def hostinfo(request, host):
    url = 'https://stat.dc.domain/search/?q={0}&group=all&fqdn=yes&comment=yes'.format(
        host)
    return HttpResponseRedirect(url)

# Информация о сервисе


@login_required(login_url='/login/')
def serviceinfo(request, hostgroup, service):
    url = 'https://tech.agava.net/wiki/Monitor/{0}#{1}'.format(
        hostgroup, service)
    return HttpResponseRedirect(url)

# отчёт по SLA


@login_required(login_url='/login/')
def sla(request):
    hide_nagios = request.user.profile.info.get('hide_nagios', [])
    debug = ''
    format = '%Y-%m-%d %H:%M'
    if request.GET.get('date_from'):
        date_from = datetime.strptime(request.GET['date_from'], format)
    else:
        date_from = datetime.now().replace(
            hour=19,
            minute=0,
            second=0,
            microsecond=0
        ) - timedelta(days=1)

    if request.GET.get('date_to'):
        date_to = datetime.strptime(request.GET['date_to'], format)
    else:
        date_to = datetime.now()

    incidents = incidents_with_probleminfo(
        'ctime',
        Q(ctime__gte=date_from) &
        Q(ctime__lte=date_to) &
        ~Q(statuslogs__host__nagios__fqdn__in=hide_nagios)
    )

    # Проверяем соблюдение SLA для каждого инцидента
    sla_events = []
    for i in incidents:
        sla_event = {'i': i, 'problem': []}
        notified = i.notifies.order_by('ctime')

        # etime нужен для вычисления SLA-эвентов, а у незакрытых инцидентов его
        # нет - используем now()
        if i.etime:
            etime = i.etime
        else:
            etime = get_current_timezone().localize(datetime.now())

        # Слишком долго висел инцидент без попыток обзвона
        if notified:
            delta = notified.first().ctime - i.ctime
        else:
            delta = etime - i.ctime

        if delta > timedelta(minutes=20):
            if notified:
                problem = 'Время начальной реакции ({support} -> {admin}): {delta}'.format(
                    delta=delta,
                    support=notified.first().author,
                    admin=notified.first().assigned_to.encode('utf-8'),
                )
            else:
                problem = 'Инцидент был проигнорирован.'
            sla_event['problem'].append(problem)

        # Слишком долго не могли найти ответственного
        assigned = i.notifies.filter(
            answer=NotifyLog.ANSWER_OK).order_by('ctime').first()
        if assigned:
            delta = assigned.ctime - i.ctime
        else:
            delta = etime - i.ctime

        if delta > timedelta(
                minutes=60) and notified and (
                not assigned or notified.first().ctime != assigned.ctime):
            if assigned:
                problem = 'Ответственного ({admin}) искали в течение {delta}'.format(
                    delta=delta,
                    admin=assigned.assigned_to.encode('utf-8'),
                )
            else:
                problem = 'Ответственного найти не смогли.'
            sla_event['problem'].append(problem)

        # Обзвонили слишком много людей
        admins = set()
        for n in notified:
            if n.assigned_to != '':
                admins.add(n.assigned_to.encode('utf-8'))

        if len(admins) > 2:
            problem = 'Обзвонили {len} человек: {admins}'.format(
                len=len(admins),
                admins=', '.join(admins),
            )
            sla_event['problem'].append(problem)

        # Инцидент с ответственным долго висел без пометок
        # (todo)

        # Если хотя бы одно нарушение было, создаем sla-эвент
        if len(sla_event['problem']) > 0:
            sla_events.append(sla_event)

    return render(request, 'sla.html', {
        #'debug': sla_events,
        'sla_events': sla_events,
        'incidents': incidents,
        'date_from': date_from.strftime(format),
        'date_to': date_to.strftime(format),
    })


@login_required(login_url='/login/')
def nagios_filter(request):
    nagios = Nagios.objects.all()
    profile = request.user.profile

    if request.POST:
        hide_nagios = []
        for n in nagios:
            if not request.POST.get(n.fqdn):
                hide_nagios.append(n.fqdn)
        profile.info['hide_nagios'] = hide_nagios
        profile.info['popup_alert'] = bool(
            int(request.POST.get('popup_alert')))
        profile.save()
        return HttpResponseRedirect(reverse('incidents'))
    else:
        hide_nagios = profile.info.get('hide_nagios', [])
        popup_alert = profile.info.get('popup_alert', True)

    return render(request, 'nagios_filter.html', {
        'nagios': nagios,
        'hide_nagios': hide_nagios,
        'popup_alert': popup_alert,
    })


@login_required(login_url='/login/')
def api_descr(request):
    return render(request, 'api_descr.html', {
        'nagios': Nagios.objects.all(),
    })


def api_incidents(request):
    client_ip = request.META['REMOTE_ADDR']
    if not client_ip.startswith(settings.PRIVATE_IPS_PREFIX):
        return HttpResponse('Internet access denied')

    format = request.GET.get('format', 'json-pretty')
    show = request.GET.get('show', 'critical')
    hide_nagios = request.GET.getlist('hide_nagios')
    result = []

    query = Q(etime=None) & ~Q(statuslogs__host__nagios__fqdn__in=hide_nagios) \
        & ~Q(hidden=True)
    if show not in ['critical', 'warning', 'all']:
        return HttpResponse('unknown show "{0}"'.format(show))

    incidents = incidents_with_probleminfo('-mtime', query)

    for i in incidents:
        if i.state == Service.OK and show != 'all':
            continue
        elif i.state == Service.WARNING and show not in ['all', 'warning']:
            continue

        services = [s.service_description for s in i.services]
        hosts = []
        nagios = set()
        for h in i.hosts:
            hosts.append(h.host_name)
            nagios.add(h.nagios.fqdn)

        mmin = (
            (get_current_timezone().localize(
                datetime.now()) - i.mtime).seconds) / 60

        result.append(SortedDict([
            ('id', i.id),
            ('state', i.state),
            ('hosts', hosts),
            ('services', services),
            ('nagios', list(nagios)),
            ('assigned_to', i.assigned_to),
            ('minutes_ago', mmin),
        ]))

    if format == 'json':
        return HttpResponse(
            json.dumps(result),
            content_type="application/json")
    elif format == 'json-pretty':
        return HttpResponse(json.dumps(
            result,
            indent=4,
            separators=(',', ': ')
        ), content_type="application/json")
    elif format == 'xml':
        return HttpResponse(
            dicttoxml(
                result,
                attr_type=False),
            content_type="text/xml")
    else:
        return HttpResponse('unknown format "{0}"'.format(format))


def api_hostgroups(request):
    class hashabledict(dict):

        def __hash__(self):
            return hash(tuple(sorted(self.items())))

    format = request.GET.get('format', 'json-pretty')

    hostgroups = set()
    hosts = Host.objects.filter(~Q(last_hard_state=Host.DELETED))
    for h in hosts:
        hostgroup = hashabledict({
            'name': h.hostgroup,
            'alias': h.nagios.fqdn,
        })
        hostgroups.add(hostgroup)
    result = list(hostgroups)

    if format == 'json':
        return HttpResponse(
            json.dumps(result),
            content_type="application/json")
    elif format == 'json-pretty':
        return HttpResponse(json.dumps(
            result,
            indent=4,
            separators=(',', ': ')
        ), content_type="application/json")
    elif format == 'xml':
        return HttpResponse(
            dicttoxml(
                result,
                attr_type=False),
            content_type="text/xml")
    else:
        return HttpResponse('unknown format "{0}"'.format(format))


def api_telegram(request):

    def is_duty(name, i):
        return name in [d['name'] for d in get_duties(i)['duties'] if 'duty_priority' in d and d['duty_priority']]

    ok_message_list = [u'ом', u'займусь', u'ок', u'ok']
    assign_message_list = [u'assign', u'назначить']
    delay_message_list = [u'delay', u'unimportant', u'отложить', u'неважно']
    try:
        rawreq = request.META['wsgi.input'].read(int(request.META['CONTENT_LENGTH']))
    except KeyError:
        rawreq = request.META['wsgi.input'].read()
    try:
        req = json.loads(rawreq)
    except ValueError:
        # cannot load json from request data
        return HttpResponseBadRequest('cannot load json from request data')
    if 'message' not in req.keys() or 'request_msg' not in req.keys():
        # return '400 Bad request' if request doesn't contain required fields
        return HttpResponseBadRequest('request doesn\'t contain required fields')
    message = req['message']
    request_msg = req['request_msg']
    r = re.match(r'^#i([0-9]+) (.*)', request_msg['body'])
    if r:
        incident_id = r.group(1)
        # text = r.group(2)
    else:
        # message isn't a reply to a message about incident
        return HttpResponseNotFound("message isn't a reply to a message about incident")
    try:
        incident = Incident.objects.get(id=incident_id)
    except Incident.DoesNotExist:
        return HttpResponseNotFound('Incident with such id does not exists')
    # project = message['recipient']
    user = message['sender']
    # msg_id = message['id']

    if message['body'].lower() in ok_message_list:
        # sender of message wants to take an incident
        incident.assigned_to = user
        incident.save()
        notifylog = NotifyLog(
            incident=incident,
            author=user,
            assigned_to=user,
            answer='ok',
            answer_text='',
            was_duty=is_duty(incident.assigned_to, incident),
        )
        notifylog.save()
        text = u'Ответственный изменен на {0}'.format(user)
        comment = Comment(
            incident=incident,
            author=user,
            text=text,
            system=True,
        )
        comment.save()
        telegram.assign_notification(incident, user)

    elif message['body'].lower().split(' ')[0] in assign_message_list:
        # sender wants to assign an incident to a person
        try:
            regroups = re.match(r'(\S+) (\S+)( .+)?', message['body'])
            assigned_to = regroups.group(2).lower()
            answer_text = regroups.group(3) or ''
        except AttributeError:
            return HttpResponseNotFound("Can't parse")
        incident.assigned_to = message['body'].lower().split(u' ')[1]
        incident.save()
        notifylog = NotifyLog(
            incident=incident,
            author=user,
            assigned_to=assigned_to,
            answer='ok',
            answer_text=answer_text,
            was_duty=is_duty(incident.assigned_to, incident),
        )
        notifylog.save()
        text = u'{0} назначил ответственного {1}'.format(user, assigned_to)
        comment = Comment(
            incident=incident,
            author=user,
            text=text,
            system=True,
        )
        comment.save()
        telegram.assign_notification(incident, user, answer_text)

    elif message['body'].lower() in delay_message_list:
        Comment(incident=incident, author=user, system=True,
                text=u'Инцидент отложен').save()
        telegram.unimportant_notification(incident, user)
        incident.unimportant = True
        incident.save()

    else:
        # sender just wants to leave a comment
        comment = Comment(
            incident=incident,
            author=user,
            text=message['body'],
        )
        comment.save()
        incident.save()
        telegram.comment_notification(comment)
    return HttpResponse('ok')


def api_notifylog(request):
    # check http basic auth
    if 'HTTP_AUTHORIZATION' in request.META:
        import base64
        auth = request.META['HTTP_AUTHORIZATION'].split()
        if len(auth) == 2:
            if auth[0].lower() == "basic":
                uname, passwd = base64.b64decode(auth[1]).split(':')
                user = authenticate(username=uname, password=passwd)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        request.user = user
    # request for auth if not authenticated
    if not request.user.is_authenticated():
        response = HttpResponse('Unauthorized', status=401)
        response['WWW-Authenticate'] = 'Basic realm="Restricted"'
        return response

    # check for required parameters in GET request
    # return 404 if something wrong
    result = None
    try:
        incident_id = int(request.GET.get('incident', '0'))
    except ValueError:
        incident_id = 0
    if incident_id == 0:
        result = HttpResponseNotFound('Specify incident')
    assigned_to = request.GET.get('assigned_to', None)
    if assigned_to is None:
        result = HttpResponseNotFound('Specify person to assign incident')
    author = request.GET.get('author', None)
    if author is None:
        result = HttpResponseNotFound('Specify author')
    answer = request.GET.get('answer', None)
    if answer not in NotifyLog.ANSWER_TYPES:
        result = HttpResponseNotFound('Unknown answer type')
    duty = True
    try:
        duty = int(request.GET.get('duty', '1'))
    except Exception:
        pass
    # print(u'Adding notifylog for'
    #       u' incident: {id},'
    #       u' assigned_to: {assigned_to},'
    #       u' author: {author},'
    #       u' answer: {answer},'
    #       u' duty: {duty}.'
    #       u''.format(
    #         id=incident_id,
    #         assigned_to=assigned_to,
    #         author=author,
    #         answer=answer,
    #         duty=duty))

    # if all right then create notifylog instance, save it
    # and return 200 OK
    if result is None:
        incident = get_object_or_404(Incident, id=incident_id)
        notifylog = NotifyLog(
            incident=incident,
            author=author,
            assigned_to=assigned_to,
            answer=answer,
            answer_text=request.GET.get('answer_text', ''),
            was_duty=duty
        )
        notifylog.save()
        # если для инцидента установлен флаг обзвона - снимаем его. Дозвонились.
        an = AsteriskNotification.objects.filter(incident=incident)
        if an:
            an[0].lock = False
            an[0].save()
        if answer == NotifyLog.ANSWER_OK:
            incident.assigned_to = assigned_to
            incident.save()
            telegram.assign_notification(incident, author, answer)
            Comment(
                incident=incident,
                author=author,
                text=u'Ответственный изменен на {0}'.format(assigned_to),
                system=True).save()
        else:
            pass
        result = HttpResponse('OK')
    return result


@login_required(login_url='/login/')
def create_incident(request):
    author = request.user.username
    host_id = request.POST.get('host')
    text = request.POST.get('text', '')

    error = None
    if host_id and text:
        host = get_object_or_404(Host, id=host_id)
        service = get_object_or_404(
            Service,
            host=host,
            service_description='ping')

        # Создаем пустой инцидент
        incident = Incident(etime=None, assigned_to=None, autoclose=False)
        incident.save()

        # Создаем запись в логе, привязываем ее к инциденту
        statuslog = StatusLog(
            host=host,
            service=service,
            incident=incident,
            text=text[0:254],
        )
        statuslog.save()

        telegram.first_notification(incident, service)
        # Пишем комментарий к инциденту
        comment = Comment(
            incident=incident,
            author=author,
            text=text,
        )
        comment.save()
        telegram.comment_notification(comment)
        return HttpResponseRedirect(
            reverse(
                'incident_comments',
                args=(
                    incident.id,
                )) +
            '?state=any')

    elif request.POST:
        error = 'err_empty_fields'

    return render(request, 'create_incident.html', {
        'hosts': Host.objects.all().order_by('host_name'),
        'error': error,
        'text': text,
    })


@login_required(login_url='/login/')
def stat_admin(request):
    debug = ''
    min_cnt = int(request.GET.get('min_cnt', 5))
    dformat = '%Y-%m-%d'
    tformat = '%H:%M'
    if request.GET.get('date_to'):
        date_to = datetime.strptime(request.GET['date_to']+request.GET['time_to'], dformat+tformat)
    else:
        date_to = datetime.now().replace(hour=10, minute=0, second=0, microsecond=0)

    if request.GET.get('date_from'):
        date_from = datetime.strptime(request.GET['date_from']+request.GET['time_from'], dformat+tformat)
    else:
        date_from = date_to  - timedelta(days=7) 


    log = NotifyLog.objects.filter(
        Q(ctime__gte=date_from)
        & Q(ctime__lte=date_to)
    ).order_by('-ctime')

    stat = {}
    
    # Убираем из логов повторные уведомления
    log_filtered = {}
    for l in log:
        key = str(l.incident_id) + l.assigned_to 
        
        if log_filtered.get(key):
            # Если по инциденту одного админа уведомляли несколько раз,
            # засчитываем только лучший ответ (ANSWER_ОК)
            if l.answer == NotifyLog.ANSWER_OK and log_filtered[key].answer != NotifyLog.ANSWER_OK:
                log_filtered[key] = l
        else:
            log_filtered[key] = l

    for l in sorted(log_filtered.values(), key=lambda k: k.id):
        if l.answer == NotifyLog.ANSWER_OK:
            ok = True 
        else:
            ok = False
        
        support = l.author
        admin = l.assigned_to
        if admin == '':
            continue
        duty = l.was_duty
        if not stat.get(admin):
            stat[admin] = {
                'total': {'all': [], 'ok': [], 'duty': [], 'duty_ok':[]},
                'self': {'all': [], 'ok': [], 'duty': [], 'duty_ok':[]},
                'assign': {'all': [], 'ok': [], 'not_ok': [], 'duty_ok': [], 'duty_not_ok': []},
                'asterisk': {'all': [], 'ok': [], 'not_ok': [], 'duty_ok': [], 'duty_not_ok': []},
                'availability': {'all': [], 'duty': []},
            }


        stat[admin]['total']['all'].append(l)
        if ok:
            stat[admin]['total']['ok'].append(l)
        if duty:
            stat[admin]['total']['duty'].append(l)
        if duty&ok:
            stat[admin]['total']['duty_ok'].append(l)

        if support == admin:
            # Самоназначение
            stat[admin]['self']['all'].append(l)
            if duty:
                stat[admin]['self']['duty'].append(l)
            if ok:
                stat[admin]['self']['ok'].append(l)
            if duty&ok:
                stat[admin]['self']['duty_ok'].append(l)

        elif support == 'Asterisk':
            # Астериск
            stat[admin]['asterisk']['all'].append(l) 
            if ok:
                stat[admin]['asterisk']['ok'].append(l)
                if duty:
                    stat[admin]['asterisk']['duty_ok'].append(l)
            else:
                stat[admin]['asterisk']['not_ok'].append(l)
                if duty:
                    stat[admin]['asterisk']['duty_not_ok'].append(l)
        else:
            # Уведомление саппортом
            stat[admin]['assign']['all'].append(l) 
            if ok:
                stat[admin]['assign']['ok'].append(l)
                if duty:
                    stat[admin]['assign']['duty_ok'].append(l)
            else:
                stat[admin]['assign']['not_ok'].append(l)
                if duty:
                    stat[admin]['assign']['duty_not_ok'].append(l)
    
    interval = {
        'date_from': date_from.strftime(dformat),
        'time_from': date_from.strftime(tformat),
        'date_to': date_to.strftime(dformat),
        'time_to': date_to.strftime(tformat),
    }
    # Если у нас спросили статистику по конкретному админу, возвращаем ее
    if (request.GET.get('admin')):
        admin = request.GET.get('admin')
        key = request.GET.get('key')
        subkey = request.GET.get('subkey')
        return render(request, 'stat/notify_log.html', {
            'stat': stat[admin][key][subkey],
            'interval': interval,
        })

    # Вычисляем доступность админов
    for admin, data in stat.items():
        if len(stat[admin]['total']['all']) < min_cnt:
            del stat[admin]
            continue
        try:
            ok_cnt = len(stat[admin]['total']['ok']) - len(stat[admin]['self']['ok'])
            all_cnt = len(stat[admin]['total']['all']) - len(stat[admin]['self']['all'])
            stat[admin]['availability']['all'] = 100 * ok_cnt / all_cnt
        except ZeroDivisionError:
            stat[admin]['availability']['all'] = 0
        try:
            ok_cnt = len(stat[admin]['total']['duty_ok']) - len(stat[admin]['self']['duty_ok'])
            all_cnt = len(stat[admin]['total']['duty']) - len(stat[admin]['self']['duty'])
            stat[admin]['availability']['duty'] = 100 * ok_cnt / all_cnt
        except ZeroDivisionError:
            stat[admin]['availability']['duty'] = 0

    return render(request, 'stat/admin.html', {
        'stat': stat,
        'interval': interval,
        'min_cnt': min_cnt
    })


@login_required(login_url='/login/')
def whois(request):
    marker_hostgroups = [
        'generic-new',
        'cpanel',
        '3.col.agava.net',
        'winhosting']
    duties = {}
    for i in marker_hostgroups:
        duties[i] = get_duty_list([i])

    return render(request, 'whois.html', {
        'duties': duties,
    })


# Запланировать даунтайм хоста/сервиса
@login_required(login_url='/login/')
def downtime(request):
    host = get_object_or_404(Host, id=request.REQUEST.get('host'))
    duration = int(request.REQUEST.get('duration', 60))
    result = ''

    if request.REQUEST.get('action', '') == 'downtime':
        service_id = request.REQUEST.get('service', '0')
        nagios = NagiosObject(host.nagios)

        if service_id == 'all':
            result, error = nagios.schedule_downtime_host(
                host_name=host.host_name,
                duration=duration,
                author=request.user.username,
                comment='from monitoring.domain api'
            )
            result += error
            service = host.services.filter(service_description='ping').first()
            service_name = 'всех сервисов'
        else:
            service = get_object_or_404(Service, id=service_id)
            result, error = nagios.schedule_downtime_service(
                host_name=host.host_name,
                service_description=service.service_description,
                duration=duration,
                author=request.user.username,
                comment='from monitoring.domain api'
            )
            result += error
            service_name = 'сервиса {0}'.format(service.service_description)

        incident = find_incident(
            service,
            #            create=True,
            any_service=True
        )

        Comment(
            incident=incident,
            author=request.user.username,
            text="Плановый даунтайм {service} на {hours}".format(
                service=service_name,
                hours=time_ago(get_current_timezone().localize(
                    datetime.now()) - timedelta(seconds=duration), ago=False)
            )
        ).save()
        if not error:
            return HttpResponseRedirect(
                reverse(
                    'incident_comments',
                    args=(
                        incident.id,
                    )))

    return render(request, 'downtime.html', {
        'host': host,
        'result': result,
    })


@login_required(login_url='/login/')
def statistics(request, sort_by):

    dformat = '%Y-%m-%d'
    tformat = '%H:%M'
    if request.GET.get('date_to'):
        date_to = datetime.strptime(request.GET['date_to']+request.GET['time_to'], dformat+tformat)
    else:
        date_to = datetime.now().replace(hour=10, minute=0, second=0, microsecond=0)

    if request.GET.get('date_from'):
        date_from = datetime.strptime(request.GET['date_from']+request.GET['time_from'], dformat+tformat)
    else:
        date_from = date_to  - timedelta(days=7) 

    days = 0
    daycounter = date_from
    while date_to > daycounter:
        days += 1
        daycounter += timedelta(days=1)
    incbydays = int(request.GET.get('incbydays', 0))
    incbyservices = int(request.GET.get('incbyservices', 0))
    incbyhosts = int(request.GET.get('incbyhosts', 1))
    allincidents = int(request.GET.get('allincidents', 0))

    # Incidents from 10am `days` days ago to now
    incidents = Incident.objects.filter(
        Q(ctime__gt=get_current_timezone().localize(date_from)) & Q(ctime__lt=get_current_timezone().localize(date_to))
    )
    # dictionary of all hosts to hostgroups and hostnames
    host2hg = dict()
    for host in Host.objects.all():
        host2hg[host.id] = (host.host_name, host.hostgroup)

    service2desc = dict()
    for service in Service.objects.all():
        service2desc[service.id] = service.service_description

    # dictionary of requested incidents to corresponding statuslogs
    inc2stlg = dict()
    for stlg in StatusLog.objects.filter(
                    Q(incident__ctime__gt=get_current_timezone().localize(date_from)
                      ) & Q(incident__ctime__lt=get_current_timezone().localize(date_to))):
        if stlg.incident_id in inc2stlg:
            inc2stlg[stlg.incident_id].append(stlg)
        else:
            inc2stlg[stlg.incident_id] = [stlg]

    # used to set a unique id to each instance of class Helper
    globals()['last_id'] = 0

    class Helper:
        def __init__(self, name=None):
            self.id = globals()['last_id']
            globals()['last_id'] += 1
            self.name = name
            self.incidents = set()
            self.admins = dict()
            self.problem_days = [
                (
                    get_current_timezone().localize(date_from + timedelta(days=x)),
                    set()
                ) for x in range(days+1)
                ]
            self._inc_by_hosts = dict()

        def __getattr__(self, item):
            if item == 'incidents_count':
                return len(self.incidents)
            elif item == 'problem_days_count':
                return reduce(lambda a, b: a + b, map(lambda x: 1 if x[1] else 0, self.problem_days))
            elif item == 'byhosts':
                return self._inc_by_hosts.values()
            else:
                raise AttributeError

        def addincident(self, i, s, nested=False):
            # Detect timeouts
            if not hasattr(i, 'timed_out'):
                i.timed_out = 0
                i.debug = ''

            if (
                s.text.find('Empty response') > 0 or
                s.text.find('Socket timeout') > 0 or
                s.text.find('Timed Out') > 0 or
                s.text.find('timed out') > 0
            ) and not nested:
                i.timed_out += 1
                i.debug += "[{0}] ".format(s.text)

            # Add set of hosts and services as incident's attributes so we dont have to use DB every time to get it
            if not hasattr(i, 'hosts_'):
                i.hosts_ = set()
            if not hasattr(i, 'services_'):
                i.services_ = set()
            i.hosts_.add(host2hg[s.host_id][0])
            i.services_.add(service2desc[s.service_id])
            if len(i.services_) > 4:
                i.services_string = '{0} сервисов'.format(len(i.services_))
            elif len(i.services_) > 2:
                i.services_string = '{0} сервиса'.format(len(i.services_))
            elif len(i.services_) == 2:
                i.services_string = ', '.join(i.services_)
            else:
                i.services_string = ''.join(i.services_)

            if (len(i.services_) - i.timed_out) <= 0:
                i.timeout_class = 'unimportant' 
            else:
                i.timeout_class = ''
#                i.services_string += "[{0}-{1}]".format(len(i.services_), i.timed_out)
#                i.services_string += i.debug

            if i not in self.incidents:
                self.admins[i.assigned_to] = self.admins.get(i.assigned_to, 0) + 1
                self.incidents.add(i)

            self.problem_days[
                (localtime(i.ctime).date() - get_current_timezone().localize(date_from).date()).days
            ][1].add(i)
            if incbyhosts and not nested:
                if host2hg[s.host_id][0] not in self._inc_by_hosts:
                    self._inc_by_hosts[host2hg[s.host_id][0]] = Helper(host2hg[s.host_id][0])
                self._inc_by_hosts[host2hg[s.host_id][0]].addincident(i, s, True)

    def top_hostgroups():
        hostgroups = dict()
        for i in incidents:
            try:
                # for each ststuslog
                for s in inc2stlg[i.id]:
                    hg = host2hg[s.host_id][1]
                    if hg not in hostgroups:
                        hostgroups[hg] = Helper(hg)
                    hostgroups[hg].addincident(i, s)
            except KeyError:
                # incident w/o statuslogs
                continue
        return hostgroups.values()

    def top_projects():
        def resolve_project(hostgroup_name):

            proj_cache = get_cache('projects')
            project = proj_cache.get(hostgroup_name)
            if project is not None:
                return project

            url = 'https://schedule.domain/api/duty?group={}'.format(hostgroup_name)
            response = requests.get(url, verify=False)
            response.encoding = 'utf-8'
            try:
                project = response.json()['project']
            except KeyError:
                # probably scheduler was requested for hostgroup 'no_hostgroup'
                project = 'no_project'
            proj_cache.set(hostgroup_name, project, 24*3600)
            return project

        projects = dict()
        for i in incidents:
            try:
                for s in inc2stlg[i.id]:
                    pr = resolve_project(host2hg[s.host_id][1])
                    if pr not in projects:
                        projects[pr] = Helper(pr)
                    projects[pr].addincident(i, s)
            except KeyError:
                # incident w/o statuslogs
                continue
        return projects.values()

    def top_hosts():
        hosts = dict()
        for i in incidents:
            # for each statuslog
            try:
                for s in inc2stlg[i.id]:
                    hostname = host2hg[s.host_id][0]
                    if hostname not in hosts:
                        hosts[hostname] = Helper(hostname)
                    hosts[hostname].addincident(i, s)
            except KeyError:
                # incident w/o statuslogs
                continue
        return hosts.values()

    return render(request, 'stat/hosts.html', {
        'incbydays': incbydays,
        'incbyservices': incbyservices,
        'incbyhosts': incbyhosts,
        'allincidents': allincidents,
        'interval': {
            'date_from': date_from.strftime(dformat),
            'time_from': date_from.strftime(tformat),
            'date_to': date_to.strftime(dformat),
            'time_to': date_to.strftime(tformat),
        },
        'days': days,
        'top_hostgroups': sort_by == 'hostgroups' and top_hostgroups(),
        'top_hosts': sort_by == 'hosts' and top_hosts(),
        'top_projects': sort_by == 'projects' and top_projects()
    })


@user_passes_test(lambda u: 'admin' in [g.name for g in u.groups.all()], login_url='/login/')
def motd(request):
    error = False
    if request.POST:
        subj = request.POST.get('subj', '')
        body = request.POST.get('body', '')
        author = request.user.username
        active = request.POST.get('active', 'on')
        if type(subj) != unicode:
            subj = subj.decode('utf8')
        if type(body) != unicode:
            body = body.decode('utf8')
        if not body:
            error = 'Не заполнены обязательные поля'
        else:
            id = int(request.POST.get('id', '0'))
            if id:
                motd = Motd.objects.get(id=id)
                motd.subj = subj
                motd.body = body
                motd.active = active
                motd.author = author
            else:
                motd = Motd(subj=subj, body=body, active=active, author=author)
            motd.save()
    else:
        id = int(request.GET.get('id', '0'))
        action = request.GET.get('action', '')
        if action == 'switch_active':
            m = get_object_or_404(Motd, id=id)
            if m.active:
                m.active = False
            else:
                m.active = True
            m.save()
        elif action == 'delete':
            m = get_object_or_404(Motd, id=id)
            m.delete()
        elif action == 'edit':
            m = get_object_or_404(Motd, id=id)
            return render(request, 'motd.html', {
                'edit': True,
                'motd': m,
                'error': error,
                'motds': Motd.objects.all().reverse()
            })
        else:
            pass
    return render(request, 'motd.html', {
        'error': error,
        'motds': Motd.objects.all().reverse()
    })


@user_passes_test(lambda u: 'admin' in [g.name for g in u.groups.all()], login_url='/login/')
def separate_incidents(request):
    author = request.user.username
    host_pattern = ''
    service_pattern = ''
    sep_inc = 'on'
    urgent = 'on'
    if request.POST:
        host_pattern = request.POST.get('host', '')
        service_pattern = request.POST.get('service', '')
        sep_inc = request.POST.get('sep_inc', '')
        urgent = request.POST.get('urgent', '')
        active_q = Q()
        if sep_inc:
            active_q = Q(separate_incidents=True)
        if urgent:
            active_q = active_q | Q(urgent=True)
        services = Service.objects.filter(Q(host__host_name__contains=host_pattern) &
                                          Q(service_description__contains=service_pattern) &
                                          ~Q(current_state='DELETED') &
                                          active_q)

    else:
        for k, v in request.GET.dict().items():
            if k.startswith('sep_inc_'):
                service = Service.objects.get(id=int(k[8:]))
                sep_incs = bool(int(v))
                service.separate_incidents = sep_incs
                service.save()
            if k.startswith('urgent_'):
                service = Service.objects.get(id=int(k[7:]))  # crop urgent_<service_id> to just <service_id>
                urgents = bool(int(v))
                service.urgent = urgents
                service.save()
        services = Service.objects.filter(Q(separate_incidents=True) & ~Q(current_state='DELETED'))
    error = None
    return render(request, 'separate_incident_service.html', {
        'hosts': Host.objects.all().order_by('host_name'),
        'host_pattern': host_pattern,
        'service_pattern': service_pattern,
        'sep_inc': sep_inc,
        'urgent': urgent,
        'services': services,
        'error': error
    })
