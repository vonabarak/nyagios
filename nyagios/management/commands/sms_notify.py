# encoding: utf-8
import sys
import re
import time
import requests
from requests.auth import HTTPBasicAuth
import json
from django.core.management.base import BaseCommand, CommandError
from nyagios.models import Service, User, Group, Comment
from nyagios.sms import sms_send
from nyagios.schedule import get_phone
from django.db.models import Q
from nyagios.incident import incidents_with_probleminfo
from nyagios.settings import TELEGRAM_MESSENGER_URL, TELEGRAM_AUTH_DATA


class Command(BaseCommand):
    args = '[nagios_fqdn]'
    help = 'Update ACL counters from file'

    def handle(self, *args, **options):
        dry_run = True
        if args and args[0] == 'send':
            dry_run = False
            print "Боевой запуск!"
        else:
            print "Тестовый запуск. Вызывайте с параметром 'send' для боевой отправки"

        query = Q(etime=None)
        incidents = incidents_with_probleminfo('-mtime', query)

        report = ''
        for i in incidents:
            if i.state == Service.WARNING and i.assigned_to:
                hosts = []
                for h in i.hosts:
                    hosts.append(h.host_name)

                # Отправка уведомления в телеграм
                telegram_text = u"#i{id} {assigned}, оставь комментарий к инциденту с хостом {hosts}".format(
                    id=i.id,
                    assigned=i.assigned_to,
                    hosts=", ".join(hosts)
                )
                payload = {
                    'method': 'user_message',
                    'params': {'to': i.assigned_to, 'text': telegram_text},
                    'jsonrpc': '2.0',
                    'id': 0
                }
                response = None
                try:
                    response = requests.post(
                        TELEGRAM_MESSENGER_URL,
                        data=json.dumps(payload),
                        headers={'content-type': 'application/json'},
                        auth=HTTPBasicAuth(*TELEGRAM_AUTH_DATA)
                    ).json()
                except BaseException as e:
                    print(e)

                # не получилось уведомить в телеграм; пишем СМС
                user = User.objects.filter(username=i.assigned_to)
                group = Group.objects.get(name='sms_notify')
                if user and group in user[0].groups.all():
                    need_notify = True
                else:
                    need_notify = False
                phone = get_phone(i.assigned_to)
                text = "{assigned_to}, please leave a comment on the incident {id}: {hosts}".format(
                    id=i.id,
                    hosts=", ".join(hosts),
                    assigned_to=i.assigned_to
                )
                if (not response or 'error' in response.keys()) and need_notify and phone:
                    res = 'dry_run'
                    if not dry_run:
                        res = sms_send(phone, text[0:160])
                        if res == 'OK':
                            log_text = 'уведомление {0}'.format(i.assigned_to)
                        else:
                            log_text = 'ошибка уведомления {0}'.format(res)
                        Comment(
                            incident=i,
                            author='sms',
                            system=True,
                            text=log_text
                        ).save()

                    report += "{phone} {text} ({res})\n".format(
                        phone=phone,
                        text=text,
                        res=res,
                    )
                else:
                    report += "# не уведомляю {phone}: {text}\n".format(
                        phone=phone,
                        text=text
                    )

        print report
