# -*- coding: utf8 -*-

import sys
import time
import datetime
import multiprocessing as mp
from setproctitle import setproctitle
from django.core.management.base import BaseCommand
from django.db import connection
from django.utils.timezone import get_current_timezone
from nyagios.models import Nagios, Host, Service, StatusLog, Incident, Comment
from nyagios.incident import auto_close_incidents, find_incident
from nyagios.nagios_handler import NagiosObject
from nyagios.zabbix import Zabbix

# таймаут синхронизации с нагиосом/заббиксом (сек)
timeout = 180


def create_or_close_incident(create, nagios_id, reason):
    """Создаёт или закрывает инцидент по фейковому хосту/сервису,
    абстрагирующим синхронизацию с нагиосом/заббиксом"""
    nagios = Nagios.objects.get(id=nagios_id)
    # создаём или получаем фейковый хост
    host = Host.objects.get_or_create(
            nagios=nagios,
            hostgroup='nagios',
            host_name=nagios.fqdn,
    )[0]
    host.notifications_enabled = 1
    host.notification_period = '24x7'
    host.last_check = get_current_timezone().localize(datetime.datetime.now())
    host.next_check = get_current_timezone().localize(datetime.datetime.now() + datetime.timedelta(minutes=1))
    host.save()
    # создаём или получаем фейковый сервис
    service = Service.objects.get_or_create(
            host=host,
            service_description='sync',
    )[0]
    service.notifications_enabled = 1
    service.notification_period = '24x7'
    service.last_check = get_current_timezone().localize(datetime.datetime.now())
    service.next_check = get_current_timezone().localize(datetime.datetime.now() + datetime.timedelta(minutes=1))
    service.save()

    if create:
        # необходимо создать инцидент
        # инкрементируем счётчик фэйлов
        nagios.error_count += 1
        nagios.save()
        if nagios.error_count <= 5:
            # Ждём 5 фэйлов. Рано паниковать.
            return
        service.plugin_output = reason
        service.current_state = service.CRITICAL
        service.last_hard_state = service.CRITICAL
        service.save()
        incident = find_incident(service=service, create=True)
        statuslog = StatusLog(
                host=host,
                service=service,
                incident=incident,
                text="[action=2] OK (ok_notify) -> CRITICAL (not_ok_notify): " + reason
        )
        statuslog.save()
        # добавляем ошибку, возникшую при попытке синхронизации в качестве коммента к инциденту
        if nagios.error:
            Comment.objects.get_or_create(
                    incident=incident,
                    author='system',
                    system=True,
                    text=nagios.error
            )

    else:
        # необходимо закрыть инцидент, если таковой был создан
        # сбрасываем счетчик фэйлов
        nagios.error_count = 0
        nagios.save()
        incidents = Incident.objects.filter(statuslogs__host=host, statuslogs__service=service, etime=None)
        incident = incidents[0] if incidents else None
        if incident:
            # ставим сервису статус "ОК", инцидент закроется автоматически после выполнения auto_close_incidents()
            statuslog = StatusLog(
                    host=host,
                    service=service,
                    incident=incident,
                    text="[action=1] CRITICAL (not_ok_notify) -> OK (ok_notify): " + reason
            )
            statuslog.save()
            service.plugin_output = reason
            service.current_state = service.OK
            service.last_hard_state = service.OK
            service.save()


class SyncProcess(mp.Process):
    """Процесс асинхронной синхронизации (-:
    """
    def __init__(self, nagios_id):
        mp.Process.__init__(self)
        self.nagios_id = nagios_id

    def run(self):
        # Драйвер БД джанги может распидорасить после форка. Закрываем коннект, новый откроется автоматически
        connection.close()
        # Скачиваем и парсим файлы, сохраняя их в базу
        db = Nagios.objects.get(pk=self.nagios_id)
        setproctitle("sync {fqdn} from {time}".format(
            fqdn=db.fqdn,
            time=time.strftime("%Y-%m-%d %H:%M:%S"),
        ))
        if not db.enabled:
            print("Skipping {0} - disabled".format(db.fqdn))
            sys.exit(0)
        if db.zabbix:
            start = time.time()
            if not Zabbix(db).create_incidents():
                sys.exit(2)
            print("{0} updated in {1:.1f} seconds".format(
                db.fqdn,
                (time.time() - start)
            ))
        else:
            try:
                nagios = NagiosObject(db)
                start = time.time()
                nagios.save_nagios_info_to_db()
                # Если обновление прошло успешно, проводим ревизию удаленных хостов/сервисов
                nagios.clean_deprecated_objects()
                print("{0} updated in {1:.1f} seconds".format(
                    db.fqdn,
                    (time.time() - start)
                ))
            except IOError as err:
                print("{0} update failed: {1}".format(
                    db.fqdn,
                    err
                ))
                sys.exit(2)


class Command(BaseCommand):
    args = '[nagios_fqdn]'
    help = 'Update ACL counters from file'

    def handle(self, *args, **options):
        # Парсим аргументы
        if len(args) > 0:
            nagios_list = Nagios.objects.filter(fqdn__in=args)
            if len(nagios_list) == 0:
                self.stderr.write(
                    "Nagios with fqdn {0} not found in DB".format(
                        args
                    ))
                sys.exit(1)
        else:
            nagios_list = Nagios.objects.all()

        all_start = time.time()
        # создаём процессы
        processes = [SyncProcess(n.id) for n in nagios_list]
        # запускаем процессы
        map(lambda proc: proc.start(), processes)
        connection.close()
        while True:
            # время, оставшееся до таймаута
            time_remainig = timeout - (time.time() - all_start)
            # если хотябы один из процессов не завершися и таймаут не наступил - спим
            if reduce(lambda x, y: x or y, [p.is_alive() for p in processes]) and time_remainig > 0:
                time.sleep(1)
            # либо все процессы завершились, либо наступил таймаут
            else:
                for p in processes:
                    # если процесс ещё не завершился - киляем его и создаём инцидент
                    if p.is_alive():
                        print("terminating process {}".format(p.nagios_id))
                        p.terminate()
                        create_or_close_incident(
                                create=True,
                                nagios_id=p.nagios_id,
                                reason="Cannot sync. Process killed after timeout {} sec.".format(timeout))
                    # если процесс завершился - проверяем его exitcode и при необходимости создаём инцидент
                    else:
                        if p.exitcode == 0:
                            create_or_close_incident(create=False, nagios_id=p.nagios_id, reason="Synced successfully")
                        else:
                            create_or_close_incident(
                                create=True,
                                nagios_id=p.nagios_id,
                                reason="Sync process exited due to I/O error.")
                            print("process {0} finished with errorcode {1}".format(p.nagios_id, p.exitcode))
                break
        # закрываем инциденты, все сервисы которых починились
        auto_close_incidents()
        self.stdout.write(
            "Elapsed time: {0:.2f}".format(
                time.time() -
                all_start))
