# -*- coding: utf8 -*-

from django.core.management.base import BaseCommand
import threading
import urllib2
import datetime
from nyagios.regexp_replacer import replace
from nyagios.settings import DICTOR_URL
from django.db.models import Q
from django.utils.timezone import get_current_timezone
import nyagios.incident
import nyagios.notifier
import requests

requests.packages.urllib3.disable_warnings()  # suppress noisy messages

speak_for = [
    'smol',
    'zalyalowa',
    'puleglot',
    'paranoic',
    'kostinan',
    # 'vonabarak',
    'softded',
    'rodion',
    'ppdv',
    'abelov'
]


class Command(BaseCommand):
    help = u'Call duties via asterisk api'

    def __init__(self):
        BaseCommand.__init__(self)
        self.semaphore = threading.Semaphore()

    def say(self, text):
        class RPCThread(threading.Thread):
            def __init__(self, t, s):
                self.semaphore = s
                try:
                    self.url = DICTOR_URL + u"?message={}".format(
                        urllib2.quote(t.encode('utf8')))
                except UnicodeEncodeError:
                    pass
                    # logger.error(u'Cant speak!')
                threading.Thread.__init__(self)

            def run(self):
                # logger.debug(u'Acquiring semaphore to say some text')
                self.semaphore.acquire()
                # logger.debug(u'Calling speaker with text "{}"'.format(text))
                try:
                    requests.get(self.url, timeout=300)
                    # logger.debug(u'Dictor has finished to speak')
                except BaseException as e:
                    print(e)
                    # logger.error(u'Cant speak! {}'.format(e))
                finally:
                    # logger.debug(u'Releasing semaphore')
                    self.semaphore.release()
        RPCThread(replace(text), self.semaphore).start()

    def handle(self, *args, **options):

        unassigned = nyagios.incident.incidents_with_probleminfo(
            'id', Q(etime=None, assigned_to=None, hidden=False, unimportant=False))
        if nyagios.notifier.telegram.achtung.check_achtung():
            self.say(u'Ахтунг, господа! {0} неназначенных инцидентов. [[rate 40]]Боль. Тлен. Безысходность.'.format(
                    len(unassigned)))
            return
        for i in unassigned:
            service = i.statuslogs.all()[0].service
            self.say(u'Новый инцидент с хостом {host}. Сервис {service} сменил статус на {new}.'.format(
                        host=service.host.host_name,
                        service=service.service_description,
                        new=service.last_hard_state
            ))

        overdued = nyagios.incident.incidents_with_probleminfo(
            'id', Q(etime=None, hidden=False, unimportant=False, mtime__lt=get_current_timezone().localize(
                        datetime.datetime.now() - datetime.timedelta(hours=3))))
        for i in overdued:
            service = i.statuslogs.all()[0].service
            if i.assigned_to in speak_for:
                self.say(u'{assigned_to}, оставь комментарий к инциденту с хостом {host}'.format(
                            host=service.host.host_name,
                            assigned_to=i.assigned_to
                ))
