# encoding: utf-8
import sys
import re
import time
from django.core.management.base import BaseCommand, CommandError
from nyagios.models import Comment
from django.db.models import Q
from nyagios.incident import incidents_with_probleminfo
from datetime import datetime, timedelta
from django.utils.timezone import get_current_timezone


class Command(BaseCommand):
    args = '[nagios_fqdn]'
    help = 'Update ACL counters from file'

    def handle(self, *args, **options):
        delta = timedelta(days=3)
        time_threshold = get_current_timezone().localize(
            datetime.now()) - delta
        query = Q(etime=None) & \
            Q(unimportant=True) & \
            Q(mtime__lt=time_threshold)

        incidents = incidents_with_probleminfo('-mtime', query)

        res = 'Максимальный разрешенный период откладывания: {0}\n'.format(
            delta
        )
        for i in incidents:
            hosts = []
            for h in i.hosts:
                hosts.append(h.host_name)

            res += 'Сброшен флаг у инцидента №{id} ({hosts}), ответственный {a}\n'.format(
                id=i.id,
                hosts=", ".join(hosts),
                a=i.assigned_to,
                delta=delta
            )
            Comment(
                incident=i,
                author='flush_flags',
                system=True,
                text='Допустимый период откладывания истек'.format(delta)
            ).save()
            i.unimportant = False
            i.save()

        print res
