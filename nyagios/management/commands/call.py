# -*- coding: utf8 -*-

from django.core.management.base import BaseCommand
from nyagios.asterisk import asterisk
from nyagios.incident import incidents_with_probleminfo
from nyagios.models import Incident
from nyagios.notifier import telegram
from django.db.models import Q
from django.utils.timezone import get_current_timezone
from datetime import datetime, timedelta
import nyagios.schedule
import logging
import uuid

logger = logging.getLogger('nyagios.asterisk')


class Command(BaseCommand):
    help = u'Call duties via asterisk api'

    def handle(self, *args, **kwargs):
        incident_sets = dict()
        # Ахтунг-моуд
        if telegram.achtung.get_achtung():
            logger.warning(u'Emergency mode. Calling urgent incidents only')
            # Звоним только по срочным инцидентам
            all_incidents = incidents_with_probleminfo(
                'id',
                Q(
                    etime=None,
                    assigned_to=None,
                    statuslogs__service__urgent=True
                )
            )
        else:
            # Звоним по срочным инцидентам и инцедентам, не назначенным более 10 минут.
            all_incidents = incidents_with_probleminfo(
                'id',
                Q(
                    etime=None,
                    ctime__lt=get_current_timezone().localize(datetime.now() - timedelta(minutes=10)),
                    assigned_to=None,
                    hidden=False
                ) | Q(
                    etime=None,
                    assigned_to=None,
                    statuslogs__service__urgent=True
                )
            )
        logger.debug(all_incidents)
        for i in all_incidents:
            schedule_data = nyagios.schedule.get_duties(i)
            project = schedule_data['project'] if 'project' in schedule_data else 'no_project'

            i.project = project
            duty_set = frozenset([d['name'] for d in schedule_data['duties'] if d['name'] != ''])

            # группирование инцидетов по ответственным
            if duty_set not in incident_sets:
                incident_sets[duty_set] = set()
            incident_sets[duty_set].add(i)
        threads = list()
        for dset, iset in incident_sets.items():
            thread_name = uuid.uuid4().get_hex()
            logger.info(
                u'\nStarting thread {thread}'
                u'\nDuty set: {dset}'
                u'\nIncidents: {iset}'
                u'\nProjects: {projects}'.format(
                    thread=thread_name,
                    dset=dset,
                    iset=[i.id for i in iset],
                    projects=u', '.join(set([i.project for i in iset]))
                )
            )
            threads += [thread_name]
            asterisk.call(iset, thread_name)

        for x in threads:
            if asterisk.th[x]:
                print(u'Waiting for thread {0} to exit'.format(x))
                asterisk.th[x].join()
