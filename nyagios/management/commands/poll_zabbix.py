
from django.core.management.base import BaseCommand
from nyagios.zabbix import Zabbix
from nyagios.models import Nagios


class Command(BaseCommand):
    help = 'poll zbbix through api to add zabbix triggers to monitoring as incidents'

    def handle(self, *args, **kwargs):
        for i in Nagios.objects.filter(zabbix=True):
            if i.enabled:
                Zabbix(i).sync_services()
