# -*- coding: utf-8 -*-
import sys
import re
import os
import subprocess
import logging
from collections import defaultdict
from filecmp import cmp
from datetime import datetime, timedelta, time
from time import mktime
from django.db.models import Q
from django.utils.timezone import get_current_timezone
from django.conf import settings
from nyagios.models import Nagios, Host, Service, StatusLog, Comment
from nyagios.incident import find_incident, auto_close_incidents


# Отображает в логах, какой экземпляр нагиоса в данный момент обрабатывается


class CustomAdapter(logging.LoggerAdapter):

    def process(self, msg, kwargs):
        # return '[%s] %s' % (self.extra['nagios'], msg.decode('utf-8')), kwargs
        return u'[{0}] {1}'.format(self.extra['nagios'], msg), kwargs


class NagiosObject:
    filenames_short = ('objects.cache', 'status.dat')
    hosts = {}
    hostgroups = {}
    remote_user = 'nagios_rsync'
    status_file_location = '/var/spool/nagios/status.dat'
    objects_file_location = '/var/spool/nagios/objects.cache'
    cmd_file_location = '/var/lib/nagios3/rw/nagios.cmd'
    private_key = os.path.join(settings.BASE_DIR, '.ssh', 'id_rsa')
    rsync_params = [
        '--archive',
        '--rsh="ssh -oStrictHostKeyChecking=no -c arcfour -i {0}"'.format(private_key),
        '--quiet',
        '--timeout=5',
        '--copy-links',
    ]

    def __init__(self, nagios, use_cache=False):
        self.hosts = {}
        self.hostgroups = {}
        self.files = []
        self.nagios = nagios
        self.use_cache = use_cache
        logger = logging.getLogger(__name__)
        self.logger = CustomAdapter(logger, {'nagios': self.nagios.fqdn})
        self.logger.info(
            "Nagios object initialized. use_cache={0}".format(use_cache))

    # Скачивает новые файлы нагиоса
    def __rsync_to_tmp_files(self):
        stdout = None
        stderr = None
        try:
            dst_dir = os.path.join(settings.CACHE_DIR, self.nagios.fqdn)
            os.mkdir(dst_dir)
        except OSError:
            pass

        try:
            command = 'rsync {params} {user}@{host}:{status} {user}@{host}:{objects} {dst_dir}'.format(
                params=" ".join(
                    self.rsync_params),
                host=self.nagios.fqdn,
                user=self.remote_user,
                status=self.status_file_location,
                objects=self.objects_file_location,
                dst_dir=dst_dir,
            )

            self.logger.info(command)
            pipe = subprocess.Popen(
                command,
                shell=True,
                universal_newlines=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            stdout, stderr = pipe.communicate()

        except subprocess.CalledProcessError as e:
            stderr = "{0}\nExit code: {1}".format(e.output, e.returncode)
            os.rmdir(dst_dir)

        return stdout, stderr

    def __ssh_pipe(self, text):
        try:
            command = 'ssh -oStrictHostKeyChecking=no -i {key} {user}@{host} "cat >> {cmd_file}; echo OK"'.format(
                user=self.remote_user,
                host=self.nagios.fqdn,
                cmd_file=self.cmd_file_location,
                key=self.private_key
            )
            pipe = subprocess.Popen(
                command,
                shell=True,
                universal_newlines=True,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            stdout, stderr = pipe.communicate(input=text)
        except subprocess.CalledProcessError as e:
            stderr = "{0}\nExit code: {1}".format(e.output, e.returncode)
        return stdout + "\ncommand: {0}".format(command), stderr

    # Перемещает временные файлы, забэкапив старую версию
    def __move_and_rotate_cached(self):
        for tmp_filename in self.filenames_short:
            tmp_file = os.path.join(self.nagios.fqdn, tmp_filename)
            # nagios2.agava.net.status.dat
            long_name = ".".join([self.nagios.fqdn, tmp_filename])
            # nagios2.agava.net.status.dat.20141213_11:00
            history_name = ".".join([
                self.nagios.fqdn,
                tmp_filename,
                str(datetime.now())
            ])

            if (os.path.isfile(long_name)
                    and cmp(tmp_file, long_name)):
                # Если файл не изменился, просто удаляем скаченную версию
                self.logger.info("{0} not changed".format(long_name))
                os.remove(tmp_file)
            else:
                # Бэкапим старый файл для истории
                if os.path.isfile(long_name):
                    os.rename(
                        long_name,
                        os.path.join('history', history_name)
                    )
                # Заливаем новый файл на его место
                os.rename(tmp_file, long_name)
                self.logger.info("{0} updated".format(long_name))
        os.rmdir(self.nagios.fqdn)

    # Скачивает status.dat и objects.cache
    def download_nagios_files(self):
        os.chdir(settings.CACHE_DIR)
        if self.use_cache:  # Используется в тестах
            self.logger.info("Using cached files")
        else:
            # Скачиваем новую версию файлов
            stdout, stderr = self.__rsync_to_tmp_files()

            if stderr:
                self.nagios.error = stderr
                self.logger.error(stderr)
            else:
                self.nagios.error = None
                self.logger.info("download success")
                self.__move_and_rotate_cached()

            # Сохраняем обновленную информацию об ошибке или ее отсутствии
            self.nagios.save()
            if stderr:
                return IOError(stderr)

        # Если скачивание/парсинг кэша прошли успешно
        for file in self.filenames_short:
            full_name = os.path.join(
                settings.CACHE_DIR,
                ".".join([self.nagios.fqdn, file])
            )
            if os.path.isfile(full_name):
                self.files.append(full_name)
            else:
                error = "File {0} not exists".format(full_name)
                self.logger.error(error)
                return IOError(error)
        self.logger.info("Using files {0}".format(self.files))
        return 0

    def __parse_nagios_file(self, source):
        conf = []
        f = open(source, 'r')
        # Преобразуем status.dat или objects.cache в массив
        for line in f.readlines():
            line = line.strip()
            matchObject = re.match(r"""
                        (?:\s*define)?  # ключевое слово define присутствует только в object-файле
                        \s*
                        (\w+?)          # тип объекта, который мы хотим получить (например, 'host')
                        (status)?       # в status-файле (для nagios>=4.0) объекты имеют постфикс ('hoststatus')
                        \s+
                        {               # после фигурной скобки будет следовать перечисление свойств объекта
                        """, line, re.X)
            matchAttr = re.match(r"""
                        \s*
                        (\w+)           # имя свойства объекта (например 'current_state')
                        (?:=|\s+)       # разделитель ('=' в status-файле, '\s+' в object-файле)
                        (.*)            # значение свойства ('0')
                        """, line, re.X)
            matchEndObject = re.match(r"""
                        \s*}            # конец перечисления атрибутов
                        """, line, re.X)
            if len(line) == 0 or line[0] == '#':
                pass
            elif matchObject:
                identifier = matchObject.group(1)
                cur = [identifier, {}]
            elif matchAttr:
                attribute = matchAttr.group(1)
                value = matchAttr.group(2).strip()
                cur[1][attribute] = value
            elif matchEndObject and cur:
                conf.append(cur)
                del cur

        # Заполняем словарь self.hosts
        for object, content in conf:
            if object == 'host':
                self.__parse_host(content)
            elif object == 'service':
                self.__parse_service(content)
            elif object == 'hostgroup':
                self.__parse_hostgroup(content)

        # После того как все хосты добавлены в словарь, расставляем хостгруппы
        for hostgroup, content in self.hostgroups.items():
            for member in content['members']:
                if self.hosts.get(member):
                    self.hosts[member]['hostgroup'] = hostgroup

    def __parse_host(self, content):
        host = content['host_name']
        self.logger.debug("Found host '{host}'".format(host=host))

        if content.get('state_type') == '0':
            content['state_type'] = Host.SOFT
        elif content.get('state_type') == '1':
            content['state_type'] = Host.HARD

        if not self.hosts.get(host):
            self.hosts[host] = {
                'services': {
                    'ping': {
                        'service_description': 'ping'}}}
        for field in content.keys():
            self.hosts[host][field] = content[field]
            self.hosts[host]['services']['ping'][field] = content[field]

        for field in ('current_state', 'last_hard_state'):
            int_state = int(content.get(field, -1))
            if content.get('has_been_checked') == '0':
                str_state = Host.PENDING
            elif int_state == 0:
                str_state = Host.OK
            elif int_state == 1:
                str_state = Host.CRITICAL
            elif int_state == 2:
                str_state = Host.UNKNOWN
            else:
                str_state = None

            if str_state:
                self.hosts[host][field] = str_state
                self.hosts[host]['services']['ping'][field] = str_state

    def __parse_service(self, content):
        host = content['host_name']
        service = content['service_description']

        if content.get('state_type') == '0':
            content['state_type'] = Service.SOFT
        elif content.get('state_type') == '1':
            content['state_type'] = Service.HARD

        if not self.hosts.get(host):
            self.hosts[host] = {'services': {}}

        if not self.hosts[host]['services'].get(service):
            self.hosts[host]['services'][service] = {}

        for field in content.keys():
            self.hosts[host]['services'][service][field] = content[field]

        for field in ('current_state', 'last_hard_state'):
            int_state = int(content.get(field, -1))
            if content.get('has_been_checked') == '0':
                str_state = Service.PENDING
            elif int_state == 0:
                str_state = Service.OK
            elif int_state == 1:
                str_state = Service.WARNING
            elif int_state == 2:
                str_state = Service.CRITICAL
            elif int_state == 3:
                str_state = Service.UNKNOWN
            else:
                str_state = None
            if str_state:
                self.hosts[host]['services'][service][field] = str_state

        self.logger.debug(
            "Found service '{host}/{service}': {state}, {type}".format(
                host=host,
                service=service,
                state=self.hosts[host]['services'][service].get(
                    'last_hard_state', '-'),
                type=self.hosts[host]['services'][service].get(
                    'state_type', '-')
            ))

    def __parse_hostgroup(self, content):
        hostgroup = content['hostgroup_name']
        alias = content['alias']
        members = content.get('members', '')

        if alias == 'system':
            self.logger.debug(
                "Skipped 'system' hostgroup {0}".format(hostgroup))
            return

        self.logger.debug(
            "Found hostgroup '{hostgroup}' with members: {members}".format(
                hostgroup=hostgroup,
                members=members))

        self.hostgroups[hostgroup] = {
            'alias': alias,
            'members': members.split(','),
        }

    # Возвращает данные из нагиоса в виде словаря
    def get_nagios_info(self):
        if len(self.files) != 2:
            res = self.download_nagios_files()
            if res != 0:
                raise res

        for file in self.files:
            self.__parse_nagios_file(file)

        return self.hosts

    def __db_objectwithstate_update(self, object, statusLog):
        pass

    def __db_host_update(self, host, db_host):
        if not db_host:
            db_host = Host(
                nagios=self.nagios,
                host_name=host['host_name']
            )

        for hostattr in db_host._meta.get_all_field_names():
            if hostattr == 'services':
                continue
            if host.get(hostattr):
                setattr(db_host, hostattr, host[hostattr])

        db_host.save()

        return db_host

    def __db_service_update(self, db_host, service, db_service):
        if not db_service:
            db_service = Service(
                host=db_host,
                service_description=service['service_description']
            )
            db_service.save()

        old_last_hard_state = db_service.last_hard_state
        statusLog = StatusLog(host=db_host, service=db_service)

        for serviceattr in db_service._meta.get_all_field_names():
            if serviceattr == 'host':
                # ссылка на хост является внешним ключом, он уже задан выше
                continue

            if service.get(serviceattr):
                if serviceattr == 'plugin_output':
                    try:
                        service[serviceattr] = service[serviceattr].decode('utf-8')
                    except BaseException as e:
                        service[serviceattr] = u'cannot show plugin output as text: {0}'.format(e)
                setattr(db_service, serviceattr, service[serviceattr])

        # Даунтайм/отключенные нотификации/уведомления только в рабочее время,
        # установленные для хоста, наследуются всеми его сервисами
        if db_host.notifications_enabled == False:
            db_service.notifications_enabled = False
        if db_host.scheduled_downtime_depth:
            db_service.scheduled_downtime_depth = True
        if db_host.notification_period == Host.WORKHOURS:
            db_service.notification_period = Service.WORKHOURS
        if db_host.notification_period == Host.DAYTIME:
            db_service.notification_period = Service.DAYTIME

        # КОНЕЧНЫЙ АВТОМАТ, определяющий action - логику создания инцидентов
        # в зависимости от начального и конечного состояния

        db_service.save()
        # для тестов критически важно сделать save() до вычисления action
        # так как при сохранении прописывается фейковый atime, определяющий результаты некоторых тестов

        fsm = db_service.old_state
        action = fsm.action(db_service.fsm_state())

        statusLog.text = (
            u"[action={action}] {old} ({old_fsm}) -> {new} ({new_fsm}): {output}".format(
                action=action,
                old=old_last_hard_state,
                new=db_service.last_hard_state,
                old_fsm=db_service.old_state,
                new_fsm=db_service.fsm_state(),
                output=db_service.plugin_output,
            ))

        self.logger.debug(u"{host}.{service} statusLog: {text}".format(
            host=db_host.host_name,
            service=db_service.service_description,
            text=statusLog.text
        ))

        statusLog.text = statusLog.text[:254]

        self.logger.debug(
            "atime={0}, need_notify={1}".format(
                db_service.atime,
                db_service.need_notify()))

        if action != fsm.no_action:
            if action == fsm.create:
                statusLog.incident = find_incident(
                    db_service,
                    create=True,
                )
            elif action == fsm.find:
                statusLog.incident = find_incident(
                    db_service
                )
            statusLog.save()

    # Обновляет базу данных
    def save_nagios_info_to_db(self):
        if len(self.hosts) == 0:
            self.get_nagios_info()

        db_hosts_exist = {}
        db_hosts_updated = {}
        for h in Host.objects.filter(nagios=self.nagios):
            hash = " ".join([
                h.host_name,
                h.hostgroup,
                str(int(h.notifications_enabled)),
                h.notification_period,
                str(int(h.scheduled_downtime_depth)),
            ])
            db_hosts_updated[hash] = h

            hash = " ".join([
                h.host_name,
            ])
            db_hosts_exist[hash] = h

        db_services_exist = {}
        db_services_updated = {}
        for s in Service.objects.filter(host__nagios=self.nagios):
            hash = " ".join([
                s.host.host_name,
                s.service_description,
                str(int(s.notifications_enabled)),
                s.notification_period,
                s.current_state,
                s.last_hard_state,
                str(s.last_check),
            ])
            db_services_updated[hash] = s

            hash = " ".join([
                s.host.host_name,
                s.service_description,
            ])
            db_services_exist[hash] = s

        for host in self.hosts.values():
            hash = " ".join([
                host.get('host_name'),
                host.get('hostgroup', 'no_hostgroup'),
                host.get('notifications_enabled'),
                host.get('notification_period'),
                host.get('scheduled_downtime_depth'),
            ])
            db_host = db_hosts_updated.get(hash)
            if not db_host:
                db_host = self.__db_host_update(
                    host, db_hosts_exist.get(
                        host.get('host_name')))

                # Обновляем фейковый сервис ping
                # host['service_description'] = 'ping'
                # host['services']['ping'] = host

            for service in host['services'].values():
                hash = " ".join([
                    host.get('host_name'),
                    service['service_description'],
                    service['current_state'],
                    service['last_hard_state'],
                    service['notifications_enabled'],
                    service['notification_period'],
                    str(service['last_check']),
                ])
                if not db_services_updated.get(hash):
                    hash = " ".join([
                        host.get('host_name'),
                        service['service_description'],
                    ])
                    self.__db_service_update(
                        db_host,
                        service,
                        db_services_exist.get(hash))

        self.nagios.mtime = get_current_timezone().localize(datetime.now())
        self.nagios.save()
        self.logger.info("DB updated")

    # Очищает базу от удаленных объектов
    def clean_deprecated_objects(self):
        clean_problems_threshold = get_current_timezone().localize(
            datetime.now()) - timedelta(minutes=90)

        if self.nagios.mtime < clean_problems_threshold and self.nagios.enabled:
            # В случае, если не обновлялся сам нагиос,
            # ничего трогать не стоит
            # (если только его не отключили - в этом случае смело чистим)
            return 0

        services = Service.objects.filter(
            Q(atime__lt=clean_problems_threshold)
            & Q(host__nagios=self.nagios)
            & ~Q(current_state=Service.DELETED)
        )

        # Из базы ничего не удаляем (чтобы не нарушить целостность отчётов),
        # просто меняем статус на DELETED
        for s in services:
            if s.current_state != Service.DELETED:
                self.logger.warning(
                    "Found deprecated service! {0}.{1}".format(
                        s.host.host_name,
                        s.service_description
                    ))
                statusLog = StatusLog(host=s.host, service=s)
                statusLog.text = (
                    "[action={action}] {old} ({old_fsm}) -> DELETED".format(
                        action=1,
                        old=s.last_hard_state,
                        old_fsm=s.old_state,
                    ))
                statusLog.incident = find_incident(s)
                statusLog.save()

                s.current_state = Service.DELETED
                s.last_hard_state = Service.DELETED
                s.plugin_output = 'Service deleted from nagios ({0})'.format(
                    s.host.nagios.fqdn
                )

                s.save()

                if s.service_description == 'ping':
                    s.host.current_state = Host.DELETED
                    s.host.last_hard_state = Host.DELETED
                    s.host.save()

    def schedule_downtime_host(self, host_name, duration, author, comment=''):
        dt = datetime.now()
        timestamp = int(mktime(dt.timetuple()))
        start_time = timestamp
        end_time = timestamp + duration

        request = '[{now}] SCHEDULE_HOST_DOWNTIME;{host_name};{start_time};{end_time};{fixed};{trigger_id};{duration};{author};{comment}\n'.format(
            now=timestamp,
            host_name=host_name,
            start_time=start_time,
            end_time=end_time,
            fixed=1,
            trigger_id=0,
            duration=0,
            author=author,
            comment=comment,
        )

        stdout, stderr = self.__ssh_pipe(request)
        return "request: {0}\nstdout: {1}\n".format(
            request,
            stdout), stderr

    def schedule_downtime_service(
            self,
            host_name,
            service_description,
            duration,
            author,
            comment=''):
        dt = datetime.now()
        timestamp = int(mktime(dt.timetuple()))
        start_time = timestamp
        end_time = timestamp + duration

        request = '[{now}] SCHEDULE_SVC_DOWNTIME;{host_name};{service_description};{start_time};{end_time};{fixed};{trigger_id};{duration};{author};{comment}\n'.format(
            now=timestamp,
            host_name=host_name,
            service_description=service_description,
            start_time=start_time,
            end_time=end_time,
            fixed=1,
            trigger_id=0,
            duration=0,
            author=author,
            comment=comment,
        )

        stdout, stderr = self.__ssh_pipe(request)
        return "request: {0}\nstdout: {1}\n".format(
            request,
            stdout), stderr
