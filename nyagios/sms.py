#!/usr/bin/env python
# encoding: utf-8
import base64
import urllib2
import logging

# Модуль для отправки смс


def sms_send(to, text):
    logger = logging.getLogger(__name__)

    username = 'monitoring.domain'
    password = '3PdB9JmesEJvQAGc'
    url = 'https://sms.agava.net/api/sms/'

    data = """<?xml version="1.0" encoding="UTF-8"?>
<sms>
    <msg_from>{msgfrom}</msg_from>
    <msg_to>{to}</msg_to>
    <msg_text>{text}</msg_text>
</sms>""".format(msgfrom='AGAVA',
                 to=to,
                 text=text,
                 )

    request = urllib2.Request(url, data)
    base64string = base64.encodestring(
        '%s:%s' %
        (username, password)).replace(
        '\n', '')
    request.add_header("Content-type", "text/xml")
    request.add_header("Authorization", "Basic {0}".format(base64string))

    try:
        res = urllib2.urlopen(request, timeout=5)
        result = res.read()
    except urllib2.HTTPError as err:
        result = "{0} {1} {2}".format(err.code, err.msg, err.fp.read())

    logger.info("Request: {0}\nResponse: {1}".format(data, result))

    if '<code>OK</code>' in result:
        return 'OK'
    else:
        return result
