# -*- encoding: utf-8 -*-

import datetime
import logging
from django.utils.timezone import now
from django.db import connection
from django.db.models import Q
from pyzabbix import ZabbixAPI, ZabbixAPIException
from nyagios.incident import find_incident
from nyagios.models import Service, Host, StatusLog
from requests.adapters import ConnectionError
from django.core.exceptions import MultipleObjectsReturned
from django.core.cache import get_cache

logger = logging.getLogger(__name__)


def truncate_text(text):
    if len(text) > 360:
        return text[:350] + u'[...some text truncated...]'
    else:
        return text


class Zabbix:
    
    def __init__(self, nagios):
        self.cache = get_cache('zabbix')
        self.nagios = nagios
        self.zapi = ZabbixAPI('https://'+self.nagios.fqdn)
        self.triggers = None
        self.connected = False
        self.zapi_version = [0, 0, 0]
        try:
            self.zapi.login(self.nagios.login, self.nagios.password)
            self.connected = True
            self.nagios.error = None
            self.zapi_version = map(int, self.zapi.apiinfo.version().split('.'))
            # logger.debug(u'Successfully logged into {}'.format(self.nagios.fqdn))
        except ZabbixAPIException:
            logger.error(u'cannot log into zabbix {0}'.format(self.nagios.fqdn))
            self.nagios.error = u'cannot log into zabbix {0}'.format(self.nagios.fqdn)
        except ConnectionError:
            logger.error(u'cannot connect to zabbix {0}'.format(self.nagios.fqdn))
            self.nagios.error = u'cannot connect to zabbix {0}'.format(self.nagios.fqdn)
        self.nagios.save()

    def get_hostgroup(self, hostid):
        group_prefix = 'nyagios_'
        cache_key = 'hostgroup#' + str(self.nagios.id) + '_' + hostid
        hostgroups = self.cache.get(cache_key)
        if hostgroups is None:
            hostgroups = self.zapi.hostgroup.get(hostids=hostid)
            self.cache.set(cache_key, hostgroups, 3600 * 27 * 7)
            # logger.debug(u'Got hostgroup {0} for hostid {1} from zabbix'.format(hostgroups, hostid))
        else:
            pass
            # logger.debug(u'Got hostgroup {0} for hostid {1} from cache'.format(hostgroups, hostid))
        for hg in hostgroups:
            # if zabbix hostgroup like nyagios_foobar
            # set nyagios hostgroup to foobar
            if hg['name'].startswith(group_prefix):
                return hg['name'][len(group_prefix):]
        return 'no_hostgroup'

    def service_from_trigger(self, t, sync=False):
        # get host object or create it if doesn't exists
        host, _ = Host.objects.get_or_create(
            host_name=t['host'], nagios_id=self.nagios.id
        )
        hostgroup = self.get_hostgroup(t['hostid'])
        if host.hostgroup != hostgroup:
            host.hostgroup = hostgroup
            host.save()

        # get or create corresponding service object
        try:
            service = Service.objects.get(
                host=host,
                service_description=t['description'],
            )
        except Service.DoesNotExist:
            service = Service(
                host=host,
                service_description=t['description'],
            )
        except MultipleObjectsReturned:
            # delete all services with dat host and description except last
            _services = list(Service.objects.filter(
                host__host_name=host.host_name,
                service_description=t['description']).all())
            for i in _services[:-2]:
                i.delete()
            service = _services[-1]
        # print('{1} Got service {0}'.format(service.service_description, now()))
        # Zabbix Severity
        if int(t['priority']) <= 1:
            # Not classified or Information
            service.notifications_enabled = 0
            service.notification_period = 'unknown'
        elif int(t['priority']) == 2:
            # Warning
            service.notifications_enabled = 1
            service.notification_period = 'workhours'
        elif int(t['priority']) == 3:
            # Average
            service.notifications_enabled = 1
            service.notification_period = 'daytime'
        elif int(t['priority']) >= 4:
            # High or Disaster
            service.notifications_enabled = 1
            service.notification_period = '24x7'

        if t['comments'] != u'':
            service.plugin_output = t['comments']
        else:
            service.plugin_output = t['expression']
        if sync:
            service.atime = now()
            service.last_check = now()
            service.next_check = now() + datetime.timedelta(hours=1)

        service.save()
        return service

    def poll(self):
        """Wrapper around zabbix api v2 and v3 to poll triggers"""
        try:
            if self.zapi_version[0] == 2:
                self.poll_v2()
            elif self.zapi_version[0] == 3:
                self.poll_v3()
            else:
                # cannot connect or unsupported api version
                return False
            self.nagios.mtime = now()
            self.nagios.save()
        except ConnectionError:
            logger.error(u'zabbix {0} do not respond'.format(self.nagios.fqdn))
            self.nagios.error = u'zabbix {0} do not respond'.format(self.nagios.fqdn)
            self.nagios.save()
            return False
        return True

    def poll_v3(self):
        """Poll triggers from zabbix v3 via its api"""
        _triggers = self.zapi.trigger.get(
            skipDependent=1,
            monitored=1,
            active=1,
            output='extend',
            expandDescription=1,
            expandExpression=1,
            expandComment=1,
            selectHosts=1,
            filter={'value': 1, 'state': 0}
        )
        self.triggers = list()
        for t in _triggers:
            cache_key = 'host' + str(self.nagios.id) + '_' + '_'.join(sorted([h['hostid'] for h in t['hosts']]))
            host_list = self.cache.get(cache_key)
            if host_list is None:
                host_list = self.zapi.host.get(
                    filter={'hostid': [h['hostid'] for h in t['hosts']]},
                    output=['name']
                )[0]
                self.cache.set(cache_key, host_list, 3600*24*7)
            for k, v in host_list.items():
                if k == 'name':
                    t['host'] = v
                elif k == 'hostid':
                    t['hostid'] = v
            self.triggers.append(t)

    def poll_v2(self):
        """Poll triggers from zabbix v2 via its api"""
        self.triggers = self.zapi.trigger.get(
            # only_true=1,
            skipDependent=1,
            monitored=1,
            active=1,
            output='extend',
            expandDescription=1,
            expandExpression=1,
            expandComment=1,
            expandData='host',
            filter={"value": 1}
        )

    def get_all_triggers(self):
        try:
            if self.zapi_version[0] == 2:
                return self.get_all_triggers_v2()
            elif self.zapi_version[0] == 3:
                return self.get_all_triggers_v3()
            else:
                # cannot connect or unsupported api version
                return False
        except ConnectionError:
            logger.error(u'zabbix {0} do not respond'.format(self.nagios.fqdn))
            return False


    def get_all_triggers_v2(self):
        return self.zapi.trigger.get(
            monitored=1,
            active=1,
            output='extend',
            expandDescription=1,
            expandExpression=1,
            expandComment=1,
            expandData='host',
        )

    def get_all_triggers_v3(self):
        _triggers = self.zapi.trigger.get(
            monitored=1,
            active=1,
            output='extend',
            expandDescription=1,
            expandExpression=1,
            expandComment=1,
            selectHosts=1,
        )

        triggers = list()
        for t in _triggers:
            # cache key looks like 'host16_10153' where 16 is nagios id and 10153 is zabbix's hostid
            cache_key = 'host' + str(self.nagios.id) + '_' + '_'.join(sorted([h['hostid'] for h in t['hosts']]))

            # trying to get hostname from cache
            host_list = self.cache.get(cache_key)

            # if there is no such item in cache, get it from zabbix and add to cache
            if host_list is None:
                host_list = self.zapi.host.get(
                    filter={'hostid': [h['hostid'] for h in t['hosts']]},
                    output=['name']
                )[0]
                self.cache.set(cache_key, host_list, 3600 * 24 * 7)

            # add hostname to trigger item
            for k, v in host_list.items():
                if k == 'name':
                    t['host'] = v
                elif k == 'hostid':
                    t['hostid'] = v
            triggers.append(t)
        return triggers

    def set_services_ok(self):
        """Search for non-closed incidents in db and close them if there is no such service got from zabbix"""
        if not self.connected:
            return False
        # not-OK services from this zabbix
        services = Service.objects.filter(
            Q(host__nagios_id=self.nagios.id) &
            (~Q(current_state='OK') | ~Q(last_hard_state='OK')) &
            ~Q(current_state='DELETED')
        )
        # check for presence of each service from unresolved incidents
        # in new zabbix response
        for service in services:
            ok = False
            # if there is no such host in new response so service is ok
            hostname = service.host.host_name
            if hostname not in [t['host'] for t in self.triggers]:
                ok = True

            # if host present in new response we have to check it's services separately
            else:
                if service.service_description not in \
                        [t['description'] for t in
                            self.triggers if t['host'] == hostname]:
                    ok = True

            # service have become ok
            if ok:
                old_last_hard_state = service.last_hard_state
                service.last_hard_state = service.OK
                service.current_state = service.OK
                service.atime = now()
                service.save()
                text = u'[action={action}] {old} ({old_fsm}) -> {new} ({new_fsm}): {output}'.format(
                        action=service.old_state.action(service.fsm_state()),
                        old=old_last_hard_state,
                        new=service.last_hard_state,
                        old_fsm=service.old_state,
                        new_fsm=service.fsm_state(),
                        output=truncate_text(service.plugin_output),
                )
                statuslog = StatusLog(
                    host=service.host,
                    service=service,
                    text=text,
                )
                statuslog.save()
                logger.debug(u'Statuslog created for host {0} service {1} {2}'.format(
                    service.host.host_name, service.service_description, statuslog.text))
                # bind statuslog to incident
                fsm = service.old_state
                action = fsm.action(service.fsm_state())
                if action == fsm.find:
                    statuslog.incident = find_incident(service=service)


    def create_incidents(self, poll=True):
        """Creating incidents based on triggers we've got from zabbix"""
        # set poll=False for use existing self.triggers (useful for testing)
        if poll:
            if not self.poll():
                return False
        self.set_services_ok()
        for t in self.triggers:
            # print('start parsing trigger {0}'.format(t))
            service = self.service_from_trigger(t)
            host = service.host

            old_last_hard_state = service.last_hard_state
            service.last_hard_state = service.CRITICAL
            service.current_state = service.CRITICAL
            service.last_check = now()
            service.next_check = now() + datetime.timedelta(minutes=1)
            # print('{1} Service saved {0}'.format(service.service_description, now()))

            text = u'[action={action}] {old} ({old_fsm}) -> {new} ({new_fsm}): {output}'.format(
                action=service.old_state.action(service.fsm_state()),
                old=old_last_hard_state,
                new=service.last_hard_state,
                old_fsm=service.old_state,
                new_fsm=service.fsm_state(),
                output=truncate_text(service.plugin_output)
            )

            action = service.old_state.action(service.fsm_state())
            # print(service.service_description, action)
            if action:
                statuslog = StatusLog(
                    host=host,
                    service=service,
                    text=text
                )
            if action == service.old_state.create:
                incident = find_incident(service=service, create=True)
                logger.debug(u'Incident {0} created for service {1}'.format(incident.id, service.id))
                statuslog.incident = incident
            elif action == service.old_state.find:
                incident = find_incident(service=service)
                statuslog.incident = incident
            elif action == service.old_state.no_action:
                pass
                #just for feng-shui
            if action:
                statuslog.save()
                logger.debug(u'Statuslog {3} created for host {0} service {1} {2}'.format(
                    t['host'], t['description'], statuslog.text, statuslog.id))
            service.atime = now()
            service.save()
        return True

    def sync_services(self):

        # marks service as deleted
        def delete_service(h, s):
            if s.current_state != Service.DELETED:
                logger.warn(u'Found deprecated service! {0}.{1}'.format(h.host_name, s.service_description))
                statuslog = StatusLog(
                    host=h,
                    service=s,
                    text="[action={action}] {old} ({old_fsm}) -> DELETED".format(
                        action=1,
                        old=s.last_hard_state,
                        old_fsm=s.old_state
                    )
                )
                statuslog.incident = find_incident(s)
                statuslog.save()
                s.current_state = Service.DELETED
                s.last_hard_state = Service.DELETED
                s.plugin_output = 'Service deleted from zabbix'
                s.save()

        # dictionary of {'host': set(['service])}
        d = dict()
        # getting all triggers from zabbix
        for t in self.get_all_triggers():
            if t['host'] not in d:
                d[t['host']] = set()
            else:
                d[t['host']].add(t['description'])

            # Create service in nyagios if it doesn't exists
            self.service_from_trigger(t, sync=True)

        # marks services and hosts as deleted if there is no corresponding triggers
        for h in Host.objects.filter(nagios=self.nagios):
            if h.host_name not in d.keys():
                for s in h.services.all():
                    delete_service(h, s)
                h.current_state = Host.DELETED
                h.last_hard_state = Host.DELETED
                h.save()
            else:
                for s in h.services.all():
                    if s.service_description not in d[h.host_name]:
                        delete_service(h, s)