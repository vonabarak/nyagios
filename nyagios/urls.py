from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView
from django.views.decorators.csrf import csrf_exempt

from django.contrib import admin
from nyagios import views

admin.autodiscover()

# Examples:
# url(r'^$', 'nyagios.views.home', name='home'),
# url(r'^blog/', include('blog.urls')),
urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/favicon.ico')),
    url(r'^current_problems/$', views.current_problems, name='current_problems'),
    url(r'^incidents/$', views.incidents, name='incidents'),
    url(r'^incident/(\d+)/$', views.incident, name='incident'),
    url(r'^incident/(\d+)/comments', views.incident_comments, name='incident_comments'),
    url(r'^incident/(\d+)/status', views.incident_status, name='incident_status'),
    url(r'^incident/(\d+)/assign', views.incident_assign, name='incident_assign'),
    url(r'^incident/(\d+)/dc_ticket', views.incident_dc_ticket, name='incident_dc_ticket'),
    url(r'^status_log/', views.status_log, name='status_log'),
    url(r'^report/', views.report, name='report'),
    url(r'^sla/', views.sla, name='sla'),
    url(r'^hostinfo/(.*)', views.hostinfo, name='hostinfo'),
    url(r'^serviceinfo/(.*)/(.*)', views.serviceinfo, name='serviceinfo'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^nagios_filter/', views.nagios_filter, name='nagios_filter'),
    url(r'^api/$', views.api_descr, name='api_descr'),
    url(r'^api/incidents/$', views.api_incidents, name='api_incidents'),
    url(r'^api/hostgroups/$', views.api_hostgroups, name='api_hostgroups'),
    url(r'^api/telegram/$', csrf_exempt(views.api_telegram), name='api_telegram'),
    url(r'^api/notifylog/$', csrf_exempt(views.api_notifylog), name='api_notifylog'),
    url(r'^create_incident/$', views.create_incident, name='create_incident'),
    url(r'^stat/admin/$', views.stat_admin, name='stat_admin'),
    url(r'^stat/(hosts|hostgroups|projects)/$', views.statistics, name='statistics'),
    url(r'^whois/$', views.whois, name='whois'),
    url(r'^downtime/$', views.downtime, name='downtime'),
    url(r'^hide/$', views.hide, name='hide'),
    url(r'^rm/$', views.rm, name='rm'),
    url(r'^motd/$', views.motd, name='motd'),
    url(r'^separate_incidents/$', views.separate_incidents, name='separate_incidents'),
    url(r'^login/$', views.login_user, name='login'),
)
