# encoding: utf-8
from datetime import datetime
from django.utils.timezone import get_current_timezone


def declension(number, name):
    if isinstance(number, list):
        number = len(number)
    else:
        number = int(number)
    declension_dict = {
        'секунда': ['секунды', 'секунд'],
        'секунду': ['секунды', 'секунд'],
        'минута': ['минуты', 'минут'],
        'минуту': ['минуты', 'минут'],
        'час': ['часа', 'часов'],
        'день': ['дня', 'дней'],
        'год': ['года', 'лет'],
        'проблема': ['проблемы', 'проблем'],
        'сервис': ['сервиса', 'сервисов'],
        'хост': ['хоста', 'хостов'],
        'комментарий': ['комментария', 'комментариев'],
        'попытка': ['попытки', 'попыток'],
        'скрытый инцидент': ['скрытых инцидента', 'скрытых инцидентов'],
        '': ['', ''],
    }

    if number % 10 == 1 and number != 11:
        # 1 минута, 21 минута
        pass
    elif (number % 100 < 10 or number % 100 > 20) and 0 < number % 10 < 5:
        # 2 минуты, 22 минуты
        name = declension_dict[name][0]
    else:
        # 5 минут, 12 минут, 45 минут
        name = declension_dict[name][1]

    return "{0} {1}".format(number, name)


def time_ago(time, ago=True):
    time = time.astimezone(get_current_timezone())
    delta = get_current_timezone().localize(datetime.now()) - time

    days = abs(delta.days)
    secs = delta.seconds

    if delta.days < 0:
        days -= 1
        secs = abs(24 * 3600 - secs)

    if days > 365 * 40:
        return 'никогда'
    elif days > 365:
        text = declension(days / 365, 'год')
    elif days > 0:
        text = declension(days, 'день')
    elif secs < 60:
        text = declension(secs, 'секунду')
    elif secs < 2 * 3600:
        text = declension(secs / 60, 'минуту')
    else:
        text = declension(secs / 3600, 'час')

    if ago:
        if delta.days >= 0:
            text = text + " назад"
        else:
            text = "через " + text
    else:
        if delta.seconds < 0:
            text = "-" + text

    return text
    # return text + "[{0}d, {1}s] ({2}, {3})".format(delta.days,
    # delta.seconds, time,
