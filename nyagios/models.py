# encoding: utf-8
import os
from django.db import models
from datetime import datetime
from django.utils.datastructures import SortedDict
from django.utils.timezone import get_current_timezone
import nyagios.schedule
from django.contrib.auth.models import User, Group
import json


class EnumField(models.Field):
    """
    A field class that maps to MySQL's ENUM type.

    Usage:

    class Card(models.Model):
        suit = EnumField(values=('Clubs', 'Diamonds', 'Spades', 'Hearts'))

    c = Card()
    c.suit = 'Clubs'
    c.save()
    """

    def __init__(self, *args, **kwargs):
        try:
            self.values = kwargs.pop('values')
        except KeyError:
            self.values = ['']
        kwargs['choices'] = [(v, v) for v in self.values]
        kwargs['default'] = self.values[0]
        super(EnumField, self).__init__(*args, **kwargs)

    def db_type(self, connection):
        return "enum({0})".format(','.join("'%s'" % v for v in self.values))


class Nagios(models.Model):
    fqdn = models.CharField(max_length=255)
    enabled = models.BooleanField(default=True)
    zabbix = models.BooleanField(default=False)
    description = models.CharField(max_length=64, null=True)
    ctime = models.DateTimeField(auto_now_add=True)
    mtime = models.DateTimeField()
    error = models.TextField(null=True)
    error_count = models.IntegerField(default=0)
    login = models.CharField(max_length=64, null=True)
    password = models.CharField(max_length=64, null=True)

    def __str__(self):
        return u'id: "{id}", ' \
               u'enabled: "{enabled}", ' \
               u'fqdn: "{fqdn}", ' \
               u'description: "{description}".' \
               u''.format(
                id=self.id,
                enabled=self.enabled,
                fqdn=self.enabled,
                description=self.description
               )


class ObjectWithState(models.Model):
    # states
    OK = 'OK'
    WARNING = 'WARNING'
    CRITICAL = 'CRITICAL'
    UNKNOWN = 'UNKNOWN'
    PENDING = 'PENDING'
    DELETED = 'DELETED'

    # Что считать работающим хостом/сервисом
    is_ok_ref = dict()
    is_ok_ref[OK] = True
    is_ok_ref[WARNING] = True
    is_ok_ref[CRITICAL] = False
    is_ok_ref[UNKNOWN] = False
    is_ok_ref[PENDING] = True
    is_ok_ref[DELETED] = True

    STATES = (
        PENDING,
        OK,
        WARNING,
        CRITICAL,
        UNKNOWN,
        DELETED,
    )

    # state types
    HARD = 'HARD'
    SOFT = 'SOFT'
    STATE_TYPES = (HARD, SOFT)

    # notification periods
    ALWAYS = '24x7'
    WORKHOURS = 'workhours'
    DAYTIME = 'daytime'
    DEFAULT_NOTIFICATION_PERIOD = 'unknown'
    NOTIFICATION_PERIODS = (
        ALWAYS,
        WORKHOURS,
        DAYTIME
    )

    # Finite state machine states
    class FSM:

        """
        У нас 4 состояния: ok_notify, ok, not_ok_notify, not_ok.
        Возможные действия: создать инцидент, только найти существующий, либо ничего не делать.

        Таблица переходов хранится в action_table
        """

        notify = 1
        ok = 2
        not_ok = 0
        ok_notify = ok | notify
        not_ok_notify = notify

        no_action = 0
        find = 1
        create = 2

        action_table = {
            ok_notify: {
                not_ok_notify: create,
                not_ok: find
            },
            ok: {
                not_ok_notify: create,
                not_ok: find
            },
            not_ok_notify: {
                ok_notify: find,
                ok: find,
                not_ok: find
            },
            not_ok: {
                ok_notify: find,
                ok: find,
                not_ok_notify: create,
            },
        }

        def __init__(self, is_ok, is_notify):
            self.state = self.__state_from_params(is_ok, is_notify)

        def action(self, new_state):
            return self.action_table[self.state].get(
                new_state.get_state(),
                self.no_action
            )

        def __state_from_params(self, is_ok, is_notify):
            state = self.not_ok
            if is_ok:
                state |= self.ok

            if is_notify:
                state |= self.notify
            return state

        def get_state(self):
            return self.state

        def __str__(self):
            text = 'not_ok'
            if self.state & self.ok:
                text = 'ok'
            if self.state & self.notify:
                text += '_notify'
            return text

    current_state = EnumField(values=STATES, default=PENDING)
    last_hard_state = EnumField(values=STATES, default=PENDING)

    notification_period = EnumField(
        values=NOTIFICATION_PERIODS + (DEFAULT_NOTIFICATION_PERIOD,),
        default=DEFAULT_NOTIFICATION_PERIOD
    )

    notifications_enabled = models.BooleanField(default=True)

    state_type = EnumField(values=STATE_TYPES)
    plugin_output = models.TextField()

    current_attempt = models.IntegerField(default=0)
    max_attempts = models.IntegerField(default=0)

    last_check = models.DateTimeField(null=True)
    next_check = models.DateTimeField(null=True)

    problem_has_been_acknowledged = models.BooleanField(default=False)
    is_flapping = models.BooleanField(default=False)
    scheduled_downtime_depth = models.BooleanField(default=False)

    ctime = models.DateTimeField(auto_now_add=True)  # время создания объекта
    # время последний смены состояния
    mtime = models.DateTimeField(auto_now_add=True)
    # время последней загрузки из нагиоса
    atime = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __setattr__(self, attrname, value):
        if attrname in ('last_check', 'next_check'):
            # Сохраняем unixtimestamp как datetime
            if isinstance(value, str):
                value = get_current_timezone().localize(datetime.fromtimestamp(
                    int(value)))
        elif attrname in (
                'notifications_enabled',
                'problem_has_been_acknowledged',
                'is_flapping',
                'scheduled_downtime_depth',
        ):
            # Преобразуем целочисленные и булевые значения из str
            if isinstance(value, str):
                value = int(value)  # int преобразуется в boolean само
        elif attrname == 'notification_period':
            if value not in self.NOTIFICATION_PERIODS:
                value = self.DEFAULT_NOTIFICATION_PERIOD

        super(ObjectWithState, self).__setattr__(attrname, value)

    def __init__(self, *args, **kwargs):
        super(ObjectWithState, self).__init__(*args, **kwargs)
        self.old_state = self.FSM(self.is_ok(), self.need_notify())

    def need_notify(self):
        if not self.notifications_enabled:
            return False
        elif self.scheduled_downtime_depth:
            return False
        elif self.notification_period == 'workhours':
            workhours = nyagios.schedule.is_workhours(self.atime)
            if type(workhours) is bool:
                return workhours
            else:
                # raise IOError(workhours)
                return True
        elif self.notification_period == 'daytime':
            # print(self.atime.hour, self.id, self.ctime, self.mtime, self.atime)
            # now = get_current_timezone().localize(datetime.now())
            # В базе время хранится по Гринвичу. Наше рабочее время с 7 до 16 UTC
            if 16 <= self.atime.hour or self.atime.hour < 7:
                return False
        return True

    # возвращает одно из состояний конечного автомата (Finite State Machine):
    # ok_notify, ok, not_ok_notify, not_ok
    def fsm_state(self):
        fsm = self.FSM(self.is_ok(), self.need_notify())
        return fsm

    def is_ok(self):
        return self.is_ok_ref[self.last_hard_state]

    def save(self, *args, **kwargs):
        # В тестах время редактирования заменяется фейковым
        if os.environ.get('NOW'):
            for field in self._meta.local_fields:
                if field.name == "atime":
                    field.auto_now = False
            self.atime = get_current_timezone().localize(datetime.strptime(
                os.environ['NOW'], "%Y-%m-%d %H:%M:%S"))

        if self.fsm_state().get_state() != self.old_state.get_state():
            self.mtime = get_current_timezone().localize(datetime.now())
        super(ObjectWithState, self).save(*args, **kwargs)


class Host(ObjectWithState):
    nagios = models.ForeignKey(Nagios)
    host_name = models.CharField(max_length=255)
    hostgroup = models.CharField(max_length=64, default='no_hostgroup')

    class Meta:
        unique_together = ('nagios', 'host_name',)

    def __str__(self):
        return u'id: "{id}", ' \
               u'name: "{name}", ' \
               u'hostgroup: "{hostgroup}", ' \
               u'nagios: "{nagios}".' \
               u''.format(
                    id=self.id,
                    name=self.host_name,
                    hostgroup=self.hostgroup,
                    nagios=self.nagios)


class Service(ObjectWithState):
    host = models.ForeignKey(
        Host,
        related_name='services'
    )
    service_description = models.CharField(max_length=255)

    # Создавать отдельный инцидент по сервису, не объединяя с другими сервисами того же хоста.
    separate_incidents = models.BooleanField(default=False)

    # Звонить по инциденту сразу же и независимо от ахтунг-режима.
    urgent = models.BooleanField(default=False)

    class Meta:
        unique_together = ('host', 'service_description',)

    def __str__(self):
        return u'id: "{id}", ' \
               u'description: "{description}", ' \
               u'host: "{host}".' \
               u''.format(
                id=self.id,
                description=self.service_description,
                host=self.host
               )


class Incident(models.Model):
    assigned_to = models.CharField(max_length=255, null=True)
    unimportant = models.BooleanField(default=False)
    autoclose = models.BooleanField(default=True)
    hidden = models.BooleanField(default=False)
    ctime = models.DateTimeField(auto_now_add=True)
    mtime = models.DateTimeField(auto_now=True)
    etime = models.DateTimeField(null=True)

    def close(self):
        self.etime = get_current_timezone().localize(datetime.now())
        self.save()

    class Meta:
        get_latest_by = 'mtime'

    def __str__(self):
        return u'id: "{id}", ' \
               u'ctime: "{ctime}", ' \
               u'mtime: "{mtime}", ' \
               u'etime: "{etime}", ' \
               u'assigned: "{assigned}".' \
               u''.format(
                    id=self.id,
                    ctime=self.ctime,
                    mtime=self.mtime,
                    etime=self.etime,
                    assigned=self.assigned_to
                )


class StatusLog(models.Model):
    host = models.ForeignKey(Host)
    service = models.ForeignKey(
        Service,
        null=True,
        related_name='statuslogs'
    )
    incident = models.ForeignKey(
        Incident,
        null=True,
        related_name='statuslogs'
    )
    text = models.CharField(max_length=1024)
    ctime = models.DateTimeField(auto_now_add=True, db_index=True)

    class Meta:
        get_latest_by = 'ctime'

    def __str__(self):
        return u'id: "{id}", ' \
               u'incident: "{incident}", ' \
               u'host: "{host}", ' \
               u'hostgroup: "{hostgroup}", ' \
               u'service: "{service}", ' \
               u'text: "{text}", ' \
               u'ctime: "{ctime}".' \
               u''.format(
                    id=self.id,
                    incident=self.incident_id,
                    host=self.host.host_name,
                    hostgroup=self.host.hostgroup,
                    service=self.service.service_description,
                    text=self.text,
                    ctime=self.ctime
               )


class Comment(models.Model):
    incident = models.ForeignKey(Incident, related_name='comments')
    author = models.CharField(max_length=255)
    ctime = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    system = models.BooleanField(default=False)

    class Meta:
        get_latest_by = 'ctime'


class NotifyLog(models.Model):
    ANSWER_OK = 'ok'
    NOT_MY_PROBLEM = 'not_my_problem'

    ANSWER_TYPES = SortedDict([
        (ANSWER_OK, 'success'),
        ('cant_help', 'warning'),
        ('no_answer', 'danger'),
        ('unreachable', 'danger'),
        (NOT_MY_PROBLEM, 'info'),
    ])
    incident = models.ForeignKey(Incident, related_name='notifies')
    author = models.CharField(max_length=255)
    assigned_to = models.CharField(max_length=255)
    answer = EnumField(values=ANSWER_TYPES.keys())
    answer_text = models.CharField(max_length=255)
    ctime = models.DateTimeField(auto_now_add=True)
    was_duty = models.BooleanField(default=False)


class TelegramNotification(models.Model):
    incident = models.OneToOneField(Incident, primary_key=True, related_name='telegram_notification')
    mtime = models.DateTimeField(auto_now=True)
    # 0 = инцидент создан, уведомление дежурным
    # 1 = инцидент не присвоен 30 мин, уведомление в чатик проекта
    # 2 = инцидент не присвоен час, уведомление в общий чатик
    # 3 = инцидент закрылся, уведомление в зависимости от предыдущего уровня
    lvl = models.SmallIntegerField(default=0)


class AsteriskNotification(models.Model):
    incident = models.OneToOneField(Incident, primary_key=True, related_name='asterisk_notification')
    mtime = models.DateTimeField(auto_now=True)
    # 10 = инцидент создан, звонок дежурному
    # 20 = дежурный не ответил/не помог, звонок группе по кругу
    # 30 = никто не ответил/не помог, звонок начальству
    lvl = models.IntegerField(default=10)
    # флаг, означающий, что по этому инциденту уже звоним
    lock = models.BooleanField(default=False)


class Motd(models.Model):
    subj = models.CharField(max_length=255, default='')
    body = models.TextField()
    author = models.CharField(max_length=255)
    ctime = models.DateTimeField(auto_now_add=True)
    mtime = models.DateTimeField(auto_now=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return u'subj: {0}. body: {1}.'.format(self.subj, self.body)


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True)
    preferences = models.TextField()
    atime = models.DateTimeField(auto_now=True)

    def __init__(self, *args, **kwargs):
        super(UserProfile, self).__init__(*args, **kwargs)
        self.info = json.loads(self.preferences or '{}')

    def save(self, *args, **kwargs):
        self.preferences = json.dumps(self.info)
        super(UserProfile, self).save(*args, **kwargs)


User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
