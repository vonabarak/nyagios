# encoding: utf-8
from django.test import TestCase

import time
import calendar
from nyagios.models import Nagios, Host, Service, StatusLog, Incident, NotifyLog, AsteriskNotification
from nyagios.nagios_handler import NagiosObject
from django.utils.timezone import now
from nyagios.schedule import is_workhours
from nyagios.russian_numerical import time_ago
from nyagios.incident import auto_close_incidents, find_incident
from datetime import datetime, timedelta
from django.utils.timezone import get_current_timezone
import os
# from random import randint
from nyagios.notifier import telegram
from nyagios.zabbix import Zabbix
from nyagios.asterisk import Asterisk, RPCThread
from django.db.models import Q

test_user = 'vonabarak'


class A01_NagiosHandleTests(TestCase):
    fixtures = ['test_nagios.json', ]

    def setUp(self):
        self.real_nagios = Nagios.objects.get(fqdn='nagios2.agava.net')
        self.bad_nagios = Nagios(fqdn='10.0.0.1', mtime=now())
        self.test_nagios = Nagios.objects.get(fqdn='test')
        pass

    def test_09_cant_download(self):
        bad_nagios = NagiosObject(self.bad_nagios, use_cache=False)

        res = bad_nagios.download_nagios_files()
        self.assertIsInstance(res, IOError)

    def test_20_download(self):
        nagios = NagiosObject(self.real_nagios, use_cache=False)
        res = nagios.download_nagios_files()
        self.assertEqual(res, 0)

    def test_20_get_info(self):
        nagios = NagiosObject(self.real_nagios, use_cache=True)
        res = nagios.get_nagios_info()
        self.assertIsInstance(res, dict)

        self.assertGreater(len(res), 1)

        for host, hostinfo in res.items():

            self.assertIn(hostinfo.get('current_state'), Host.STATES)
            self.assertIn(
                hostinfo.get('notification_period'),
                ['24x7', 'workhours']
            )
            self.assertIsNot(hostinfo.get('hostgroup'), None)
            self.assertIsInstance(hostinfo.get('services'), dict)
            self.assertGreater(len(hostinfo.get('services')), 0)

            for service, serviceinfo in hostinfo.get('services').items():
                self.assertIn(
                    serviceinfo.get('current_state'),
                    Service.STATES
                )

    def test_05_update_db(self):
        nagios = NagiosObject(self.real_nagios, use_cache=True)
        nagios_info = nagios.get_nagios_info()
        nagios.save_nagios_info_to_db()

        # Проверяем обновление времени апдейта нагиоса
        nagios = Nagios.objects.get(fqdn='nagios2.agava.net')
        self.assertEqual(
            nagios.mtime.replace(
                second=0), now().replace(
                second=0, microsecond=0))

        # Проверяем наличие всех хостов в базе
        self.assertEqual(len(nagios_info), Host.objects.count())

        # Проверяем наличие всех сервисов в базе
        services_count = 0
        for host in nagios_info.values():
            self.assertEqual(host['state_type'], Host.HARD)
            services_count += len(host['services'])

        self.assertEqual(services_count, Service.objects.count())

        # Проверяем что насоздавались записи в логах
        statuslog_count = StatusLog.objects.count()

        # Проверяем что инциденты создались на каждый хост/сервис с проблемами
        incident_count = Incident.objects.count()
        host_problems = Host.objects.filter(
            last_hard_state__in=[Host.CRITICAL, Host.UNKNOWN]
        ).count()
        service_problems = Service.objects.filter(
            last_hard_state__in=[Service.CRITICAL, Service.UNKNOWN]
        ).count()

        # очень ленивая проверка :)
        # не работает, если есть хосты с плановым даунтаймом
        #self.assertTrue(
        #    host_problems <= incident_count <= host_problems + service_problems
        #)
        self.assertTrue(
            incident_count <= host_problems + service_problems
        )
        # Находим сервис в статусе CRITICAL, делаем вид что он был ОК
        service = Service.objects.filter(
            last_hard_state=Service.CRITICAL,
            notifications_enabled=True,
            notification_period=Service.ALWAYS,
            statuslogs__incident__isnull=False # чтобы хосты с плановым даунтаймом не ломали проверку
        ).last()
        # service = Service.objects.filter(
        #     ~Q(statuslogs__incident=None)
        # ).last()
        service.last_hard_state = Service.OK
        service.save()
        time.sleep(1)  # Чтобы изменения успели записаться в базу

        # Запускаем обновление статуса
        nagios = NagiosObject(self.real_nagios, use_cache=True)
        nagios.save_nagios_info_to_db()

        # Проверяем, что появилась запись в логе об изменении
        status_log = StatusLog.objects.all().order_by('-ctime').first()
        self.assertEqual(status_log.service.id, service.id)
        self.assertEqual(status_log.host.id, service.host.id)

        # Проверяем что обновление записалось к существующему инциденту
        self.assertIsNotNone(status_log.incident)
        self.assertEqual(StatusLog.objects.count(), statuslog_count + 1)
        self.assertEqual(Incident.objects.count(), incident_count)

    def test_00_workhours(self):
        work_times = {
            '2014-01-01 12:00:00': False,   # Новый Год
            '2014-03-08 12:00:00': False,   # 8 марта
            '2014-08-07 09:00:00': False,   # раб.утро
            '2014-08-07 12:00:00': True,    # раб.день (это было сегодня, да)
            '2014-08-07 19:05:00': False,   # раб.ночь
            '2014-09-07 12:00:00': False,   # выходной
            '2012-09-03 12:00:00': True,    # недалекое прошлое (будни)
            '1900-01-01 12:00:00': None,    # другая эпоха
            # далекое будущее, про которое сервис ещё не знает
            # При удачном стечении обстоятельств в далеком 2018 году эта проверка сломается. Live long and prosper!
            '2018-09-01 12:00:00': None,
        }

        self.assertIn(is_workhours(), (True, False), 'NOW()')

        for date, expected in work_times.items():
            result = is_workhours(date)
            self.assertEqual(
                result,
                expected,
                "{0} should return {1}".format(date, expected)
            )

    def test_10_synthetic_db(self):
        """
            Структура тестовой сети:

            Хост/сервис     State       Properties
            host1           CRITICAL    (notifications_disabled)
                .service1   OK

            host2           CRITICAL    (scheduled_downtime)
                .service2   CRITICAL
                .service3   OK
                .service4   OK

            host3           CRITICAL    (workhours)
                .service5   CRITICAL

            host4           CRITICAL    (24x7)
                .service6   CRITICAL

            host5           CRITICAL    (no_hostgroup, notification_period unknown)



        """
        # Волшебным образом переставляем часы на нерабочее время
        os.environ['NOW'] = '2014-08-07 20:00:00'
        self.assertEqual(is_workhours(os.environ['NOW']), False)

        # Забираем инфу из тестовых конфигов нагиоса
        nagios = NagiosObject(self.test_nagios, use_cache=True)
        nagios_info = nagios.get_nagios_info()
        nagios.save_nagios_info_to_db()

        # Проверяем наличие всех хостов в базе
        self.assertEqual(len(nagios_info), Host.objects.count())

        # N-1 хостов входят в одну хостгруппу
        self.assertEqual(
            len(nagios_info) - 1,
            Host.objects.filter(hostgroup='hostgroup1').count()
        )

        # У host1 отключены нотификации (только в status.dat)
        # У host4 отключены нотификации (только в objects.cache) - игнорируется,
        #                                           т.к. status.dat приоритетнее
        self.assertEqual(
            'host1',
            Host.objects.get(notifications_enabled=False).host_name
        )

        # Сервиса получается 2: host1.ping, host1.service1
        # (сервисы наследуют notifications_enabled=True от хоста)
        self.assertEqual(
            2,
            Service.objects.filter(notifications_enabled=False).count()
        )

        # У хоста host2 запланированный даунтайм,
        # который, кроме пинга, наследуется еще 3мя сервисами
        self.assertEqual(
            4,
            Service.objects.filter(
                scheduled_downtime_depth=True).count())

        # У хоста host3 уведомления только в рабочее время (наследуется сервисом)
        self.assertEqual(
            2,
            Service.objects.filter(
                notification_period='workhours').count())

        # host1, host2, host3, host4, host5 находятся в CRITICAL
        self.assertEqual(
            5,
            Host.objects.filter(last_hard_state=Host.CRITICAL).count()
        )

        # service2, service5, service6 также находятся в состоянии CRITICAL
        # (+ упавшие хосты тоже считается как 3 сервиса)
        self.assertEqual(
            5 + 3,
            Service.objects.filter(last_hard_state=Service.CRITICAL).count()
        )

        # Инцидента создалось всего 2: по host4 и host5
        time.sleep(5)
        self.assertEqual(2, Incident.objects.count())

        # Инцидент1: Записи в логах создались по host4.ping, host4.service6
        incident = Incident.objects.filter(
            statuslogs__host__host_name='host4').first()
        self.assertEqual('host4', incident.statuslogs.first().host.host_name)
        self.assertEqual(2, incident.statuslogs.count())
        for log in incident.statuslogs.all():
            self.assertIn(
                log.service.service_description,
                ('service6', 'ping')
            )

        # Инцидент2: одна запись в логах по host5.ping
        incident = Incident.objects.get(statuslogs__host__host_name='host5')
        self.assertEqual(
            'ping',
            incident.statuslogs.get().service.service_description
        )

        # Волшебным образом переставляем часы на рабочее время
        os.environ['NOW'] = '2014-08-07 11:00:00'
        self.assertEqual(is_workhours(os.environ['NOW']), True)

        # Дергаем базу еще раз
        nagios = NagiosObject(self.test_nagios, use_cache=True)
        nagios.save_nagios_info_to_db()
        time.sleep(3)

        # Проверяем, что у нас создался еще один инцидент по host3
        self.assertEqual(3, Incident.objects.count())
        incident = Incident.objects.last()

        for log in incident.statuslogs.all():
            self.assertEqual(
                'host3',
                log.host.host_name
            )
            self.assertIn(
                log.service.service_description,
                ('service5', 'ping')
            )

        self.assertEqual(incident.etime, None)

        # Волшебным образом переставляем часы на нерабочее время
        os.environ['NOW'] = '2014-08-07 21:00:00'

        # Дергаем базу еще раз
        nagios = NagiosObject(self.test_nagios, use_cache=True)
        nagios.save_nagios_info_to_db()

        # Вручную делаем вид, что host5 починился
        service = Service.objects.get(host__host_name='host5')
        service.last_hard_state = Service.OK
        service.save()

        # Запускаем автозакрытие инцидентов
        auto_close_incidents()

        # Проверяем, что инцидент по host5 закрылся, т.к. все проблемы решены
        incident = Incident.objects.get(statuslogs__host__host_name='host5')
        self.assertNotEqual(incident.etime, None)

        # Проверяем, что инцидент по host3 не закрылся
        # (уведомления по workhours-сервису отключились, но инцидент закрывать не надо)
        incident = Incident.objects.last()
        self.assertEqual(incident.etime, None)

    def test_40_russian_numerical(self):
        current_time = get_current_timezone().localize(datetime.now())

        test_times = [
            (timedelta(hours=25), 'через 1 день'),
            (timedelta(hours=47), 'через 1 день'),

            (timedelta(minutes=-15), '15 минут назад'),
            (timedelta(hours=-15), '15 часов назад'),
            (timedelta(hours=-23), '23 часа назад'),
            (timedelta(hours=-25), '1 день назад'),
            (timedelta(hours=-47), '1 день назад'),
            (timedelta(hours=-49), '2 дня назад'),
            (timedelta(days=-15), '15 дней назад'),
            (timedelta(days=-5 * 365), '5 лет назад'),
            (timedelta(minutes=15), 'через 15 минут'),
            (timedelta(hours=15), 'через 15 часов'),
            (timedelta(hours=23), 'через 23 часа'),
            (timedelta(hours=49), 'через 2 дня'),
            (timedelta(days=15, hours=1), 'через 15 дней'),
            (timedelta(days=3 * 366), 'через 3 года'),
            (timedelta(days=-50 * 365), 'никогда'),
            (timedelta(days=50 * 365), 'никогда'),
        ]

        for time_, expected in test_times:
            got = time_ago(current_time + time_)
            self.assertEqual(
                got,
                expected,
                "'{0}' вместо '{1}'".format(
                    got,
                    expected))


class A02_TelegramTests(TestCase):

    def test_10_telegram_basic_message(self):
        # Отправляем сообщение тестовому чуваку
        msg = 'Greetings from AGAVA service!'
        res = telegram.user_message(test_user, msg)
        self.assertIsNone(res)

    def test_50_telegram_group_message(self):
        # Отправляем сообщение в группу Example3
        msg = 'Greetings from AGAVA service!'
        res = telegram.chat_message('Example3', msg)
        self.assertIsNone(res)


class A03_ZabbixHandleTests(TestCase):
    def setUp(self):
        (self.nagios, created) = Nagios.objects.get_or_create(
            fqdn='flows2.agava.net',
            zabbix=1,
            defaults={'mtime': now(),
                      'login': 'demo',
                      'password': 'simple-pa$$',
                      }
        )
        self.zabbix = Zabbix(nagios=self.nagios)

        # Добавляем в кэш сведения о хостгруппах, типа, мы их раньше уже получили (-:
        for h in range(0, 99):
            cache_key = 'hostgroup#' + str(self.zabbix.nagios.id) + '_' + str(h)
            self.zabbix.cache.set(
                cache_key,
                [
                    {u'internal': u'1', u'flags': u'0', u'groupid': u'1', u'name': u'Discovered hosts'},
                    {u'internal': u'0', u'flags': u'0', u'groupid': u'2', u'name': u'nyagios_TestHostgroup'}
                ],
                3600)

    def test_01_cached_hostgroups(self):
        self.assertEqual(
            set([x['name'] for x in self.zabbix.cache.get('hostgroup#' + str(self.zabbix.nagios.id) + '_' + '10')]),
            {u'nyagios_TestHostgroup', u'Discovered hosts'}
        )

    def test_02_new_incidents(self):
        """Создание инцидентов"""
        self.zabbix.triggers = [
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'30712', u'flags': u'4', u'comments': u'', u'hostid': u'10', u'host': u'testhost11', u'expression': u'box48'},
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'32395', u'flags': u'4', u'comments': u'', u'hostid': u'11', u'host': u'testhost12', u'expression': u'box40'},
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'32687', u'flags': u'4', u'comments': u'', u'hostid': u'12', u'host': u'testhost13', u'expression': u'box10'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'33684', u'flags': u'4', u'comments': u'', u'hostid': u'13', u'host': u'testhost14', u'expression': u'box90'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'35235', u'flags': u'4', u'comments': u'', u'hostid': u'14', u'host': u'testhost15', u'expression': u'box37'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'35336', u'flags': u'4', u'comments': u'', u'hostid': u'15', u'host': u'testhost16', u'expression': u'box39'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'38161', u'flags': u'4', u'comments': u'', u'hostid': u'16', u'host': u'testhost17', u'expression': u'box29'},
            {u'description': u'service2', u'priority': u'1', u'triggerid': u'38205', u'flags': u'4', u'comments': u'', u'hostid': u'17', u'host': u'testhost18', u'expression': u'box29'},
        ]

        self.assertTrue(self.zabbix.create_incidents(poll=False))

        # Должно возникнуть 3 инцидента с сервисом service1
        self.assertEqual(
            len(set(Incident.objects.filter(Q(statuslogs__service__service_description='service1') & Q(etime=None)))),
            3
        )
        # один инцидент с хостом testhost11
        self.assertEqual(
            len(set(Incident.objects.filter(Q(statuslogs__host__host_name='testhost11') & Q(etime=None)))),
            1
        )
        # инцидент с testhost18 возникнуть не должен, т.к. priority = 1, т.е. только ворнинг
        self.assertEqual(
            len(set(Incident.objects.filter(Q(statuslogs__host__host_name='testhost18') & Q(etime=None)))),
            0
        )

    def test_03_same_incidents(self):
        """дважды вызванная функция create_incidents не должна создать дубликатов"""
        self.zabbix.triggers = [
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'30712', u'flags': u'4', u'comments': u'', u'hostid': u'10', u'host': u'testhost11', u'expression': u'box48'},
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'32395', u'flags': u'4', u'comments': u'', u'hostid': u'11', u'host': u'testhost12', u'expression': u'box40'},
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'32687', u'flags': u'4', u'comments': u'', u'hostid': u'12', u'host': u'testhost13', u'expression': u'box10'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'33684', u'flags': u'4', u'comments': u'', u'hostid': u'13', u'host': u'testhost14', u'expression': u'box90'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'35235', u'flags': u'4', u'comments': u'', u'hostid': u'14', u'host': u'testhost15', u'expression': u'box37'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'35336', u'flags': u'4', u'comments': u'', u'hostid': u'15', u'host': u'testhost16', u'expression': u'box39'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'38161', u'flags': u'4', u'comments': u'', u'hostid': u'16', u'host': u'testhost17', u'expression': u'box29'},
            {u'description': u'service2', u'priority': u'1', u'triggerid': u'38205', u'flags': u'4', u'comments': u'', u'hostid': u'17', u'host': u'testhost18', u'expression': u'box29'},
        ]

        self.assertTrue(self.zabbix.create_incidents(poll=False))
        self.assertTrue(self.zabbix.create_incidents(poll=False))

        # Должно остаться 3 инцидента с сервисом service1
        self.assertEqual(
            len(set(Incident.objects.filter(Q(statuslogs__service__service_description='service1') & Q(etime=None)))),
            3
        )
        # один инцидент с хостом testhost11
        self.assertEqual(
            len(set(Incident.objects.filter(Q(statuslogs__host__host_name='testhost11') & Q(etime=None)))),
            1
        )
        # инцидент с testhost18 возникнуть не должен, т.к. priority = 1, т.е. только ворнинг
        self.assertEqual(
            len(set(Incident.objects.filter(Q(statuslogs__host__host_name='testhost18') & Q(etime=None)))),
            0
        )

    def test_04_change_incidents(self):
        """Инцидент должен закрыться при исчезновении триггера"""
        self.zabbix.triggers = [
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'30712', u'flags': u'4', u'comments': u'', u'hostid': u'10', u'host': u'testhost11', u'expression': u'box48'},
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'32395', u'flags': u'4', u'comments': u'', u'hostid': u'11', u'host': u'testhost12', u'expression': u'box40'},
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'32687', u'flags': u'4', u'comments': u'', u'hostid': u'12', u'host': u'testhost13', u'expression': u'box10'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'33684', u'flags': u'4', u'comments': u'', u'hostid': u'13', u'host': u'testhost14', u'expression': u'box90'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'35235', u'flags': u'4', u'comments': u'', u'hostid': u'14', u'host': u'testhost15', u'expression': u'box37'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'35336', u'flags': u'4', u'comments': u'', u'hostid': u'15', u'host': u'testhost16', u'expression': u'box39'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'38161', u'flags': u'4', u'comments': u'', u'hostid': u'16', u'host': u'testhost17', u'expression': u'box29'},
            {u'description': u'service2', u'priority': u'1', u'triggerid': u'38205', u'flags': u'4', u'comments': u'', u'hostid': u'17', u'host': u'testhost18', u'expression': u'box29'},
        ]
        self.assertTrue(self.zabbix.create_incidents(poll=False))

        self.zabbix.triggers = [
            # Эта строка закомментирована специально
            # {u'description': u'service1', u'priority': u'4', u'triggerid': u'30712', u'flags': u'4', u'comments': u'', u'hostid': u'10', u'host': u'testhost11', u'expression': u'box48'},
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'32395', u'flags': u'4', u'comments': u'', u'hostid': u'11', u'host': u'testhost12', u'expression': u'box40'},
            {u'description': u'service1', u'priority': u'4', u'triggerid': u'32687', u'flags': u'4', u'comments': u'', u'hostid': u'12', u'host': u'testhost13', u'expression': u'box10'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'33684', u'flags': u'4', u'comments': u'', u'hostid': u'13', u'host': u'testhost14', u'expression': u'box90'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'35235', u'flags': u'4', u'comments': u'', u'hostid': u'14', u'host': u'testhost15', u'expression': u'box37'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'35336', u'flags': u'4', u'comments': u'', u'hostid': u'15', u'host': u'testhost16', u'expression': u'box39'},
            {u'description': u'service2', u'priority': u'4', u'triggerid': u'38161', u'flags': u'4', u'comments': u'', u'hostid': u'16', u'host': u'testhost17', u'expression': u'box29'},
            {u'description': u'service2', u'priority': u'1', u'triggerid': u'38205', u'flags': u'4', u'comments': u'', u'hostid': u'17', u'host': u'testhost18', u'expression': u'box29'},
        ]

        self.assertTrue(self.zabbix.create_incidents(poll=False))
        auto_close_incidents()

        # Должно остаться 2 инцидента с сервисом service1
        self.assertEqual(
            len(set(Incident.objects.filter(Q(statuslogs__service__service_description='service1') & Q(etime=None)))),
            2
        )
        # инцидент с хостом testhost11 должен закрыться
        self.assertEqual(
            len(set(Incident.objects.filter(Q(statuslogs__host__host_name='testhost11') & Q(etime=None)))),
            0
        )

    def test_05_separate_incidents_after_30_min(self):
        """Инцидент по тому же хосту, созданный через >30 мин не должен содержать сервисы из старого инцидента"""
        # создаём первый инцидент
        self.zabbix.triggers = [
            {u'description': u'service21', u'priority': u'4', u'triggerid': u'38161', u'flags': u'4', u'comments': u'', u'hostid': u'21', u'host': u'testhost21', u'expression': u'box29'},
            {u'description': u'service22', u'priority': u'4', u'triggerid': u'38205', u'flags': u'4', u'comments': u'', u'hostid': u'21', u'host': u'testhost21', u'expression': u'box29'},
            # {u'description': u'service23', u'priority': u'4', u'triggerid': u'38205', u'flags': u'4', u'comments': u'', u'hostid': u'21', u'host': u'testhost21', u'expression': u'box29'},
        ]
        self.assertTrue(self.zabbix.create_incidents(poll=False))
        incident1 = Incident.objects.get(statuslogs__service__service_description='service21')
        # переводим время создания первого инцидента на час назад
        incident1.ctime = get_current_timezone().localize(datetime.now() - timedelta(minutes=60))
        incident1.save()

        # создаём второй инцидент
        self.zabbix.triggers = [
            {u'description': u'service21', u'priority': u'4', u'triggerid': u'38161', u'flags': u'4', u'comments': u'', u'hostid': u'21', u'host': u'testhost21', u'expression': u'box29'},
            {u'description': u'service22', u'priority': u'4', u'triggerid': u'38205', u'flags': u'4', u'comments': u'', u'hostid': u'21', u'host': u'testhost21', u'expression': u'box29'},
            {u'description': u'service23', u'priority': u'4', u'triggerid': u'38205', u'flags': u'4', u'comments': u'', u'hostid': u'21', u'host': u'testhost21', u'expression': u'box29'},
        ]
        self.assertTrue(self.zabbix.create_incidents(poll=False))
        incident2 = Incident.objects.get(statuslogs__service__service_description='service23')
        # print([stlg.service.service_description for stlg in incident1.statuslogs.all()])
        # print([stlg.service.service_description for stlg in incident2.statuslogs.all()])

        # второй инцидент должен содержать только сервис service23
        self.assertEqual(
            set([stlg.service.service_description for stlg in incident2.statuslogs.all()]),
            {u'service23'}
        )

    def test_06_time_check(self):
        os.environ['NOW'] = '2016-11-13 23:00:00' # sunday
        self.zabbix.triggers = [
            # workhours
            {u'description': u'service61', u'priority': u'2', u'triggerid': u'161', u'flags': u'4', u'comments': u'', u'hostid': u'61', u'host': u'testhost61', u'expression': u'box29'},
            # daytime
            {u'description': u'service62', u'priority': u'3', u'triggerid': u'162', u'flags': u'4', u'comments': u'', u'hostid': u'62', u'host': u'testhost62', u'expression': u'box29'},
            # 24x7
            {u'description': u'service63', u'priority': u'4', u'triggerid': u'163', u'flags': u'4', u'comments': u'', u'hostid': u'63', u'host': u'testhost63', u'expression': u'box29'},
        ]
        self.zabbix.create_incidents(poll=False)
        auto_close_incidents()

        # Инцидент с сервисом не должен возникнуть
        self.assertEquals(
            Incident.objects.filter(statuslogs__service__service_description='service61').count(),
            0
        )
        self.assertEquals(
            Incident.objects.filter(statuslogs__service__service_description='service62').count(),
            0
        )
        # должен возникнуть инцидент
        self.assertEquals(
            Incident.objects.filter(statuslogs__service__service_description='service63').count(),
            1
        )

        # наступило утро. Должны создаться инциденты
        os.environ['NOW'] = '2016-11-14 14:00:00' # monday
        self.zabbix.create_incidents(poll=False)
        auto_close_incidents()
        self.assertEquals(
            Incident.objects.filter(statuslogs__service__service_description='service61').count(),
            1
        )
        self.assertEquals(
            Incident.objects.filter(statuslogs__service__service_description='service62').count(),
            1
        )
        self.assertEquals(
            Incident.objects.filter(statuslogs__service__service_description='service63').count(),
            1
        )

    def test_07_separate_incidents(self):
        """Создание отдельных инцидентов для сервисов с флагом separate_incidents"""
        # ниже есть более общая проверка. Эта проверяет только заббикс-бэкэнд

        # создаём инцидент
        self.zabbix.triggers = [
            {u'description': u'service71', u'priority': u'4', u'triggerid': u'7161', u'flags': u'4', u'comments': u'', u'hostid': u'71', u'host': u'testhost71', u'expression': u'box29'},
        ]
        self.zabbix.create_incidents(poll=False)
        self.assertEqual(
            Incident.objects.filter(statuslogs__host__host_name='testhost71').count(),
            1
        )

        # делаем сервис "отдельным"
        s = Service.objects.get(service_description=u'service71')
        s.separate_incidents = True
        s.save()

        # закрываем инцидент
        self.zabbix.triggers = []
        self.zabbix.connected = True
        self.zabbix.create_incidents(poll=False)
        incident = Incident.objects.filter(statuslogs__host__host_name='testhost71', etime=None).first()
        incident.etime = now()
        incident.save()


        # создаём ещё инциденты по этому же сервису и двум другим.
        self.zabbix.triggers = [
            {u'description': u'service71', u'priority': u'4', u'triggerid': u'7161', u'flags': u'4', u'comments': u'', u'hostid': u'71', u'host': u'testhost71', u'expression': u'box29'},
            {u'description': u'service72', u'priority': u'4', u'triggerid': u'7162', u'flags': u'4', u'comments': u'', u'hostid': u'71', u'host': u'testhost71', u'expression': u'box29'},
            {u'description': u'service73', u'priority': u'4', u'triggerid': u'7163', u'flags': u'4', u'comments': u'', u'hostid': u'71', u'host': u'testhost71', u'expression': u'box29'},
        ]
        self.zabbix.create_incidents(poll=False)

        # должно возникнуть два инцидента с этим хостом
        incs = set()
        for i in Incident.objects.filter(statuslogs__host__host_name='testhost71', etime=None):
            incs.add(i.id)
        self.assertEqual(len(incs), 2)

        # инцидент с сервисом service73 должен иметь два сервиса
        incident = Incident.objects.filter(statuslogs__service__service_description='service73', etime=None).first()
        srvs = set()
        for stlg in incident.statuslogs.all():
            srvs.add(stlg.service_id)
        self.assertEqual(len(srvs), 2)

class A04_MiscTests(TestCase):
    def setUp(self):
        self.nagios = Nagios(fqdn='test_nagios_misc_tests', mtime=now())
        self.nagios.save()

    def test_01_check_separate_incidents(self):
        """Создание отдельных инцидентов для сервисов с флагом separate_incidents"""
        host = Host(
            nagios=self.nagios,
            host_name='test_host_separate_incidents',
            hostgroup='test_hostgroup_separate_incidents'
        )
        host.save()
        service1 = Service(
            host=host,
            service_description='test_service_separate_incidents',
            separate_incidents=True
        )
        service1.save()
        service2 = Service(
            host=host,
            service_description='test_service_not_separate_incidents_1',
            separate_incidents=False
        )
        service2.save()
        service3 = Service(
            host=host,
            service_description='test_service_not_separate_incidents_2',
            separate_incidents=False
        )
        service3.save()
        # Создаём первый инцидент
        incident = find_incident(service2, create=True)
        stlg = StatusLog(host=host, service=service2, incident=incident)
        stlg.save()
        # Должен быть создан один инцидент с этим хостом
        self.assertEqual(len(set(Incident.objects.filter(Q(statuslogs__host=host) & Q(etime=None)))), 1)

        # Создаём снова с тем же сервисом
        incident = find_incident(service2, create=True)
        stlg = StatusLog(host=host, service=service2, incident=incident)
        stlg.save()
        # инцидент должен по-прежнему быть один
        self.assertEqual(len(set(Incident.objects.filter(Q(statuslogs__host=host) & Q(etime=None)))), 1)

        # Создаём с другим сервисом
        incident = find_incident(service3, create=True)
        stlg = StatusLog(host=host, service=service3, incident=incident)
        stlg.save()
        # инцидент должен снова быть один
        self.assertEqual(len(set(Incident.objects.filter(Q(statuslogs__host=host) & Q(etime=None)))), 1)

        # Создаём со специальным сервисом
        incident = find_incident(service1, create=True)
        stlg = StatusLog(host=host, service=service2, incident=incident)
        stlg.save()
        # Должно стать два инцидента с этим хостом
        self.assertEqual(len(set(Incident.objects.filter(Q(statuslogs__host=host) & Q(etime=None)))), 2)


class A05_AsteriskTests(TestCase):
    def setUp(self):
        self.nagios = Nagios(fqdn='asterisk_tests', mtime=now())
        self.nagios.save()
        self.asterisk = Asterisk()
        self.incidents = list(map(lambda x: Incident(), range(3)))
        map(lambda x: x.save(), self.incidents)
        self.th = RPCThread(set(self.incidents), 'test_thread')
        self.schedule_data = {'duties': [
                {u'comment': u'891', u'duty_priority': u'1', u'workphone': u'891', u'name': u'faust', u'project_priority': u'5', u'vacation': u'-1464351837', u'priority': 1, 'phone': u'891', u'user_priority': u'5'},
                {u'comment': u'+7(926)816-26-88', u'duty_priority': None, u'workphone': u'', u'name': u'rodion', u'project_priority': u'5', u'vacation': u'-1464351837', u'priority': 5, 'phone': u'+79268162688', u'user_priority': u'5'},
                {u'comment': u'+7(926)556-72-64', u'duty_priority': None, u'workphone': u'', u'name': u'def', u'project_priority': u'5', u'vacation': u'-1464351837', u'priority': 5, 'phone': u'+79265567264', u'user_priority': u'5'},
                {u'comment': u'+7(915)288-42-21', u'duty_priority': None, u'workphone': None, u'name': u'zalyalowa', u'project_priority': u'5', u'vacation': u'-1464351837', u'priority': 5, 'phone': u'+79152884221', u'user_priority': u'5'},
                {u'comment': u'+7(916)585-20-41', u'duty_priority': None, u'workphone': u'', u'name': u'puleglot', u'project_priority': u'5', u'vacation': u'-1464351837', u'priority': 5, 'phone': u'+79165852041', u'user_priority': u'5'},
                {u'comment': u'+7(909)936-55-88', u'duty_priority': None, u'workphone': u'', u'name': u'paranoic', u'project_priority': u'7', u'vacation': u'-1464351837', u'priority': 7, 'phone': u'+79099365588', u'user_priority': u'5'},
                {u'comment': u'803', u'duty_priority': None, u'workphone': u'803', u'name': u'softded', u'project_priority': u'7', u'vacation': u'-21482637', u'priority': 7, 'phone': u'803', u'user_priority': u'5'},
                {u'comment': u'+7(916)444-95-73', u'duty_priority': None, u'workphone': u'', u'name': u'smol', u'project_priority': u'10', u'vacation': u'-17940237', u'priority': 10, 'phone': u'+79164449573', u'user_priority': u'10'},
                {u'comment': u'+7(916)930-15-02', u'duty_priority': None, u'workphone': u'', u'name': u'ppdv', u'project_priority': u'5', u'vacation': u'203763', u'priority': 99, 'phone': u'+79169301502', u'user_priority': u'5'},
                {u'comment': u'', u'duty_priority': None, u'workphone': u'', u'name': u'0smol-test', u'project_priority': u'99', u'vacation': u'-1178637', u'priority': 99, u'user_priority': u'99'},
                {u'comment': u'+7(967)136-81-99', u'duty_priority': None, u'workphone': u'', u'name': u'vonabarak', u'project_priority': u'100', u'vacation': u'-1464351837', u'priority': 100, 'phone': u'+79671368199', u'user_priority': u'5'},
                {u'comment': u'+7(925)060-14-28', u'duty_priority': None, u'workphone': u'', u'name': u'kyryyenko', u'project_priority': u'100', u'vacation': u'-1464351837', u'priority': 100, 'phone': u'+79250601428', u'user_priority': u'100'},
                {u'comment': u'+996550436325', u'duty_priority': None, u'workphone': u'', u'name': u'morgen', u'project_priority': u'100', u'vacation': u'-1464351837', u'priority': 100, 'phone': u'+996550436325', u'user_priority': u'100'},
                {u'comment': u'+7(903)507-60-13', u'duty_priority': None, u'workphone': u'', u'name': u'kovaleva', u'project_priority': u'100', u'vacation': u'-1464351837', u'priority': 100, 'phone': u'+79035076013', u'user_priority': u'100'},
                {'name': ''},
            ]
        }
        NotifyLog(
            incident=self.incidents[0],
            assigned_to='puleglot',
            answer='unreachable',
            ctime=now() - timedelta(minutes=5)
        ).save()
        NotifyLog(
            incident=self.incidents[0],
            assigned_to='rodion',
            answer='no_answer',
            ctime=now() - timedelta(minutes=5)
        ).save()
        NotifyLog(
            incident=self.incidents[0],
            assigned_to='def',
            answer='cant_help',
            ctime=now() - timedelta(minutes=5)
        ).save()
        NotifyLog(
            incident=self.incidents[0],
            assigned_to='vonabarak',
            answer='not_my_problem',
            ctime=now() - timedelta(minutes=5)
        ).save()

    def test_10_not_my_problem(self):
        self.assertTrue(self.th.not_my_problem('vonabarak'))
        should_be_in_list = [
            d['name'] for d in self.schedule_data['duties'] if
            'name' in d and d['name'] and d['name'] != 'vonabarak'
        ]
        # print(should_be_in_list)
        for x in should_be_in_list:
            self.assertFalse(self.th.not_my_problem(x))

    def test_11_cant_help(self):
        self.assertTrue(self.th.cant_help('def'))
        should_be_in_list = [
            d['name'] for d in self.schedule_data['duties'] if
            'name' in d and d['name'] and d['name'] != 'def'
        ]
        # print(should_be_in_list)
        for x in should_be_in_list:
            self.assertFalse(self.th.cant_help(x))

    def test_12_no_answer(self):
        self.assertTrue(self.th.no_answer('rodion'))
        should_be_in_list = [
            d['name'] for d in self.schedule_data['duties'] if
            'name' in d and d['name'] and d['name'] != 'rodion'
        ]
        # print(should_be_in_list)
        for x in should_be_in_list:
            self.assertFalse(self.th.no_answer(x))

    def test_13_unreachable(self):
        self.assertTrue(self.th.unreachable('puleglot'))
        should_be_in_list = [
            d['name'] for d in self.schedule_data['duties'] if
            'name' in d and d['name'] and d['name'] != 'puleglot'
        ]
        # print(should_be_in_list)
        for x in should_be_in_list:
            self.assertFalse(self.th.unreachable(x))

    def test_14_lock_unlock(self):
        self.assertFalse(self.th.lock())     # Инциденты не были залочены. Должно вернуть False
        self.assertTrue(self.th.lock())      # Теперь должно вернуть True, т.к. залочили строкой выше
        self.assertFalse(self.th.incidents)  # Список инцидентов должен стать пустым, т.к. лоченые исключаются

        # Возвращаем список инцидентов в первоначальное состояние
        self.th.incidents = set(self.incidents)
        # переставляем время лока на 30 мин назад.
        for i in self.incidents:
            an = AsteriskNotification.objects.get(incident=i)
            for f in an._meta.local_fields:
                if f.name == 'mtime':
                    f.auto_now = False
            an.mtime = now() - timedelta(minutes=30)
            an.save()
            for f in an._meta.local_fields:
                if f.name == 'mtime':
                    f.auto_now = True
        self.assertFalse(self.th.lock())  # Снова должно вернуть False
        self.th.unlock()
        self.assertFalse(self.th.lock())  # и опять
        self.th.unlock()
        self.assertEquals(len(self.th.incidents), 3)

    def test_21_duties_filtering(self):
        self.assertEquals(
            set(self.th.filter_duties(self.schedule_data['duties'])),
            set([
                (d['phone'], d['name'],
                 True if 'duty_priority' in d and
                         d['duty_priority'] is not None and
                         int(d['duty_priority']) > 0 else False,
                 d['priority'])

                for d in self.schedule_data['duties'] if

                'phone' in d and
                'name' in d and
                'priority' in d and 'duty_priority' in d and
                d['phone'] and d['name'] not in ['puleglot', 'rodion', 'def', 'vonabarak'] and
                int(d['priority']) < 100
            ])
        )
