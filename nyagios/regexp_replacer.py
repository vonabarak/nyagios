# -*- coding: utf8 -*-
import re

re_dict = {
    r'\.domain': u' домэйн ',
    r'whmcs': u' вэ ха эм си эс ',
    r'mne\.ru': u' мне ру',
    r'\.mne\.ru': u' точка мне ру',
    r'\.agava\.net': u' точка агава нет',
    r'rt3.': u' эр тэ три ',
    r'rt2.': u' эр тэ два ',
    r'cisco-manager': u'циско менеджер',
    r'autoreport': u'авто репорт',
    r'jivosite': u'живо сайт',
    r'mysql_repl': u'майску эль репл',
    r'db-slave': u'диби слэйв',
    r'newrt': u'нью эртэ',
    r'es\d\.agava\.net': u'е эс агава нет',
    r'90th Percentile Bandwidth Utilization': u'Полочка на интерфейсе',
    r'AGAVA-Col-Tech unassign tickets': u'Неназначенные заявки в очереди коло',
    r'AGAVA-Col-Tech unanswered tickets': u'Неотвеченные заявки в очереди коло',
    r'HS-Admin-Unix unassign tickets': u'Неназначенные заявки в очереди Юникс',
    r'HS-Admin-Unix unanswered tickets': u'Неотвеченные заявки в очереди Юникс',
    r'HS-Admin-Win unassign tickets': u'Неназначенные заявки в очереди Виндовс',
    r'HS-Admin-Win unanswered tickets': u'Неотвеченные заявки в очереди Виндовс',
    r'support_2nd_line_24x7': u'Вторая линия саппорта',
    r'ACHTUNG': u'АХТУНГ Шеф всё пропало',
    r'kostinan': u'Кос тян',
    r'abelov': u'А. Белов',
    r'softded': u'Софт Дэд',
    r'vonabarak': u'Вон аба рак',
    r'ppdv': u'Пэ Пэ Дэ Вэ',
    r'zalyalowa': u'Александра'

}

compiled_re_dict = {re.compile(key): value for key, value in re_dict.items()}


def replace(string):
    for expression, replacement in compiled_re_dict.items():
        string = expression.sub(replacement, string)
    return string
