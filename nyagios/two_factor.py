# encoding: utf-8
#
# Модуль для двухфакторной авторизации через внутренний сервис Агавы
#
# Created by smol@agava.com, 11.09.2014
import requests
import json
import logging
from random import random


def two_factor_auth(username, code):
    logger = logging.getLogger(__name__)
    if code == '':
        logger.info("Username: {0}, empty code.".format(username))
        return False, 'err_empty_code'

    url = 'https://etoken.domain/api/json'
    headers = {'content-type': 'application/json'}

    # Example echo method
    payload = {
        "method": "auth",
        "params": {
            "username": username,
            "type": "staff",
            "code": code,
            "authinfo": {'nyagios': '***'}
        },
        "id": "{0}.{1}".format(username, random()),
    }
    logger.info("Request: {0}".format(payload))
    try:
        payload['params']['authinfo'] = {'nyagios': 'wempfvn1gyBimIAP'}
        response = requests.post(
            url,
            data=json.dumps(payload),
            headers=headers,
            verify=False).json()
    except requests.exceptions.ConnectionError as err:
        logger.error("Etoken error: {0}".format(err))
        return True, ''

    logger.info("Reply: {0}".format(response))

    if response['result']:
        return True, ''
    else:
        return False, response['error']['message']
